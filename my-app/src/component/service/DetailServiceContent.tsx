import {
    Input,
    Select,
    Typography,
    DatePicker,
    Table
} from 'antd'
import React, {
    useState,
    useEffect
} from 'react';
import {
    EditOutlined,
    CaretDownOutlined,
    CalendarOutlined,
    SearchOutlined,
    RollbackOutlined
} from '@ant-design/icons';
import {
    Link,
    useLocation
} from 'react-router-dom';
import { Service } from '../../types/Service';
import { servicesCollection } from '../../features/Service/serviceSlice';
import {
    DocumentData,
    onSnapshot,
    QuerySnapshot
} from "firebase/firestore";
import { Progression } from '../../types/Progression';
import moment from 'moment';
import type { DatePickerProps, RangePickerProps } from 'antd/es/date-picker';
import { progressionCollection } from '../../features/Progression/progressionSlice';

function DetailServiceContent() {
    const [services, setServices] = useState<Service[]>([]);
    const [id, setId] = useState('');
    const location = useLocation();
    const path = location.pathname;
    const searchTerm = "id=";
    const [filteredProgressions, setFilteredProgressions] = useState<Progression[]>([]);
    const [selectedStatus, setSelectedStatus] = useState<string>('Tất cả');
    const handleStatus = (value: string) => {
        setSelectedStatus(value);
        if (value === 'Tất cả') {
            setFilteredProgressions(progressions); // Hiển thị tất cả dữ liệu
        } else {
            const filteredData = progressions.filter(progression => progression.status === value);
            setFilteredProgressions(filteredData);
        }
    };
    const [filteredDateProgressions, setFilteredDateProgressions] = useState<Progression[]>([]);

    const [progressions, setProgressions] = useState<Progression[]>([]);
    const onChangee = (
        value: DatePickerProps['value'] | RangePickerProps['value'],
        dateString: [string, string] | string
    ) => {
        console.log('Selected Time: ', value);
        console.log('Formatted Selected Time: ', dateString);

        if (!value) {
            setFilteredDateProgressions(progressions); // Hiển thị tất cả các mục khi không có ngày được chọn
            return;
        }

        if (Array.isArray(dateString)) {
            const startDate = moment(dateString[0], 'DD/MM/YYYY');
            const endDate = moment(dateString[1], 'DD/MM/YYYY');

            const filteredData = progressions.filter(progression => {
                if (progression.dayStart && progression.dayEnd) {
                    const dayStart = progression.dayStart.split(' - ')[1];

                    const formattedDayStart = moment(dayStart, 'DD/MM/YYYY');
                    const formattedDayEnd = moment(dayStart, 'DD/MM/YYYY');

                    const isWithinRange =
                        formattedDayStart.isBetween(startDate, endDate, null, '[]') &&
                        formattedDayEnd.isBetween(startDate, endDate, null, '[]');

                    return isWithinRange;
                }

                return false;
            });

            setFilteredDateProgressions(filteredData);
        } else {
            setFilteredDateProgressions([]);
        }
    };
    const { RangePicker } = DatePicker;
    const onOk = (value: DatePickerProps['value'] | RangePickerProps['value']) => {
        console.log('onOk: ', value);
    };
    const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchKeyword(event.target.value);
    }; const [searchKeyword, setSearchKeyword] = useState<string>('');
    useEffect(() => {
        setFilteredDateProgressions(progressions);

    }, [progressions]);
    useEffect(() => {
        const unsubscribe = onSnapshot(progressionCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const progressionData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setProgressions(progressionData);
        });

        return () => {
            unsubscribe();
        };
    }, []);
    useEffect(() => {
        const startIndex = path.indexOf(searchTerm);
        if (startIndex !== -1) {
            const desiredValue = path.substring(startIndex + searchTerm.length);
            setId(desiredValue);
        }
        console.log(id);
    })
    useEffect(() => {
        const unsubscribe = onSnapshot(servicesCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const serviceData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setServices(serviceData);
        });

        return () => {
            // Cleanup function to unsubscribe from the snapshot listener
            unsubscribe();
        };
    }, []);
    const columns = [
        {
            title: 'Số thứ tự',
            dataIndex: 'stt',
            key: 'stt',
            render: (stt: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {stt}
                    </Typography>
                );
            },
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            key: 'status',
            render: (status: string) => {
                return (
                    <>
                        {status === 'Đang chờ'
                            ?
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'center'
                                }}>
                                <div
                                    style={{
                                        width: '6px',
                                        height: '6px',
                                        backgroundColor: '#4277FF',
                                        borderRadius: '100%',
                                        marginRight: '2px'
                                    }}></div>
                                <Typography
                                    style={{
                                        fontFamily: 'Nunito-Regular',
                                        fontSize: '14px'
                                    }}>
                                    Đang chờ
                                </Typography>
                            </div>
                            : ""
                        }
                        {
                            status === 'Đã sử dụng'
                                ?
                                <div
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center'
                                    }}>
                                    <div
                                        style={{
                                            width: '6px',
                                            height: '6px',
                                            backgroundColor: '#7E7D88',
                                            borderRadius: '100%',
                                            marginRight: '2px'
                                        }}></div>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-Regular',
                                            fontSize: '14px'
                                        }}>
                                        Đã sử dụng
                                    </Typography>
                                </div>
                                : ""
                        }
                        {
                            status === 'Bỏ qua'
                                ?
                                <div
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center'
                                    }}>
                                    <div
                                        style={{
                                            width: '6px',
                                            height: '6px',
                                            backgroundColor: '#E73F3F',
                                            borderRadius: '100%',
                                            marginRight: '2px'
                                        }}></div>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-Regular',
                                            fontSize: '14px'
                                        }}>
                                        Bỏ qua
                                    </Typography>
                                </div>
                                : ""
                        }
                    </>
                );
            },
        },
    ]
    return (
        <div
            style={{
                margin: '0 20px'
            }}
        >
            <Typography
                style={{
                    color: '#FF7506',
                    fontFamily: 'Nunito-Bold',
                    fontSize: '24px'
                }}
            >
                Quản lý dịch vụ
            </Typography>
            <div
                style={{
                    width: '1112px',
                    height: '606px',
                    borderRadius: '16px',
                    margin: '6px 0',
                    display: 'flex',
                    justifyContent: 'space-between',
                }}>
                {services.map((service) =>
                    service.id === id ?
                        <div
                            style={{
                                width: '370px',
                                height: '606px',
                                background: '#FFFFFF',
                                boxShadow: '2px 2px 8px rgba(232, 239, 244, 0.8)',
                                borderRadius: '12px',
                                padding: '20px'
                            }}>
                            <div>
                                <Typography
                                    style={{
                                        color: '#FF9138',
                                        fontFamily: 'Nunito-Bold',
                                        fontSize: '20px'
                                    }}>
                                    Thông tin dịch vụ
                                </Typography>
                                <div
                                    style={{
                                        display: 'flex',
                                        marginTop: '6px',
                                        marginBottom: '6px',
                                    }}>
                                    <Typography
                                        style={{
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-SemiBold',
                                            width: '120px'
                                        }}>
                                        Mã dịch vụ:
                                    </Typography>
                                    <Typography
                                        style={{
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-Regular',
                                            color: '#535261',
                                            maxWidth: '210px'
                                        }}>
                                        {service.idService}
                                    </Typography>
                                </div>
                                <div
                                    style={{
                                        display: 'flex',
                                        marginTop: '6px',
                                        marginBottom: '6px',
                                    }}>
                                    <Typography
                                        style={{
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-SemiBold',
                                            width: '120px'
                                        }}>
                                        Tên dịch vụ:
                                    </Typography>
                                    <Typography
                                        style={{
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-Regular',
                                            color: '#535261',
                                            maxWidth: '210px'
                                        }}>
                                        {service.name}
                                    </Typography>
                                </div>
                                <div
                                    style={{
                                        display: 'flex',
                                        marginTop: '6px',
                                        marginBottom: '6px',
                                    }}>
                                    <Typography
                                        style={{
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-SemiBold',
                                            width: '120px',
                                            maxWidth: '210px'
                                        }}>
                                        Mô tả:
                                    </Typography>
                                    <Typography
                                        style={{
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-Regular',
                                            color: '#535261'
                                        }}>
                                        {service.describe}
                                    </Typography>
                                </div>
                            </div>
                            <div>
                                <Typography
                                    style={{
                                        color: '#FF9138',
                                        fontFamily: 'Nunito-Bold',
                                        fontSize: '20px'
                                    }}>
                                    Quy tắc cấp số
                                </Typography>
                                <div
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        margin: '8px 0'
                                    }}>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-SemiBold',
                                            fontSize: '16px',
                                            width: '120px'
                                        }}
                                    >
                                        Tăng tự động:
                                    </Typography>
                                    <Input
                                        value={'0001'}
                                        style={{
                                            width: '61px',
                                            height: '44px',
                                            fontFamily: 'Nunito-Regular',
                                            fontSize: '16px',
                                        }}>

                                    </Input>
                                    <div>
                                        <Typography
                                            style={{
                                                fontFamily: 'Nunito-SemiBold',
                                                fontSize: '16px',
                                                marginInline: '8px'
                                            }}
                                        >
                                            đến
                                        </Typography>
                                    </div>
                                    <Input
                                        value={'9999'}
                                        style={{
                                            width: '61px',
                                            height: '44px',
                                            fontFamily: 'Nunito-Regular',
                                            fontSize: '16px',
                                        }}>

                                    </Input>
                                </div>
                                <div
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        margin: '8px 0'
                                    }}>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-SemiBold',
                                            fontSize: '16px',
                                            width: '120px'
                                        }}
                                    >
                                        Prefix:
                                    </Typography>
                                    <Input
                                        value={'0001'}
                                        style={{
                                            width: '61px',
                                            height: '44px',
                                            fontFamily: 'Nunito-Regular',
                                            fontSize: '16px',
                                        }}>

                                    </Input>
                                </div>
                                <div>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-SemiBold',
                                            fontSize: '16px',
                                            width: '120px'
                                        }}
                                    >
                                        Reset mỗi ngày
                                    </Typography>
                                </div>
                                <div>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-Regular',
                                            fontSize: '16px',
                                            width: '120px',
                                            color: '#282739',
                                            marginTop: '10px'
                                        }}>
                                        Ví dụ: 201-2001
                                    </Typography>
                                </div>
                            </div>
                        </div>
                        : '')}
                <div
                    style={{
                        width: '718px',
                        height: '606px',
                        background: '#FFFFFF',
                        boxShadow: '2px 2px 8px rgba(232, 239, 244, 0.8)',
                        borderRadius: '12px',
                        padding: '20px',

                    }}>
                    <div
                        style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                        }}>
                        <div
                            style={{
                                display: 'flex'
                            }}>
                            <div
                                style={{
                                    marginRight: '16px'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-SemiBold',
                                    }}>
                                    Trạng thái
                                </Typography>
                                <Select
                                    defaultValue="Tất cả"
                                    onChange={handleStatus}
                                    listHeight={180}
                                    suffixIcon={<CaretDownOutlined
                                        style={{
                                            color: '#FF7506'
                                        }}
                                    />}
                                    style={{
                                        width: '160px',
                                        height: '44px',
                                        borderRadius: '8px',
                                        display: 'flex',
                                        justifyContent: 'space-between'
                                    }}
                                >
                                    <Select.Option
                                        className='timeSelect'
                                        key={1}
                                        Title="Tất cả"
                                        value="Tất cả"
                                        style={{
                                            height: '44px',
                                            alignItems: 'center',
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-Regular'
                                        }}
                                    >
                                        Tất cả
                                    </Select.Option>
                                    <Select.Option
                                        className='timeSelect'
                                        key={2}
                                        value="Đang chờ"
                                        Title="Đang chờ"
                                        style={{
                                            height: '44px',
                                            alignItems: 'center',
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-Regular'
                                        }}
                                    >
                                        Đang chờ
                                    </Select.Option>
                                    <Select.Option
                                        className='timeSelect'
                                        key={3}
                                        value="Đã sử dụng"
                                        Title="Đã sử dụng"
                                        style={{
                                            height: '44px',
                                            alignItems: 'center',
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-Regular'
                                        }}
                                    >
                                        Đã sử dụng
                                    </Select.Option>
                                    <Select.Option
                                        className='timeSelect'
                                        key={4}
                                        value="Bỏ qua"
                                        Title="Bỏ qua"
                                        style={{
                                            height: '44px',
                                            alignItems: 'center',
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-Regular'
                                        }}
                                    >
                                        Bỏ qua
                                    </Select.Option>
                                </Select>
                            </div>
                            <div>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-SemiBold',
                                    }}>
                                    Chọn thời gian
                                </Typography>
                                <RangePicker
                                    suffixIcon={<CalendarOutlined
                                        style={{
                                            color: '#FF7506'
                                        }}
                                    />}
                                    format="DD/MM/YYYY"
                                    onChange={onChangee}
                                    onOk={onOk}
                                />
                            </div>
                        </div>
                        <div>
                            <Typography
                                style={{
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-SemiBold',
                                }}>
                                Từ khoá
                            </Typography>
                            <Input
                                placeholder="Nhập từ khóa"
                                style={{
                                    width: '206px',
                                    height: '42px'
                                }}
                                onChange={handleSearch}
                                suffix={
                                    <SearchOutlined
                                        style={{
                                            color: '#FF7506',
                                            fontSize: '16px'
                                        }} />
                                }
                            />
                        </div>
                    </div>
                    <div
                        style={{
                            height: '490px'
                        }}
                    >
                        <Table
                            pagination={{ pageSize: 8 }}
                            dataSource={
                                selectedStatus === 'Tất cả'
                                    ? filteredDateProgressions && filteredDateProgressions.filter(
                                        progression =>
                                            progression && progression.stt && progression.stt.toString().includes(searchKeyword)
                                    )
                                    : filteredProgressions.filter(
                                        progression =>
                                            progression &&
                                            (progression.status === selectedStatus)

                                    )
                            }
                            columns={columns}
                            rowClassName={(record, index) =>
                                index % 2 === 0 ? "bg-white" : "bg-orange-50"
                            }
                            style={{
                                height: '490px',
                                marginTop: '4px'
                            }}
                        />
                    </div>
                </div>
            </div>
            <Link
                to={`/service/list-service/update-service/id=${id}`}
                style={{
                    display: 'flex',
                    position: 'absolute',
                    top: '15%',
                    left: '94%',
                    backgroundColor: '#FFF2E7',
                    flexDirection: 'column',
                    padding: '12px',
                    width: '92px',
                    borderRadius: '8px',
                    boxShadow: '0px 0px 6px #E7E9F2',
                    cursor: 'pointer'
                }}>
                <EditOutlined
                    style={{
                        color: '#ffffff',
                        width: '28px',
                        height: '28px',
                        backgroundColor: '#FF9138',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: '8px',
                        marginBottom: '4px',
                        alignSelf: 'center',
                    }} />
                <Typography
                    style={{
                        fontFamily: 'Nunito-SemiBold',
                        fontSize: '14px',
                        color: '#FF9138',
                        textAlign: 'center',
                    }}>
                    Cập nhật danh sách
                </Typography>
            </Link>
            <Link
                to={'/service/list-service'}
                style={{
                    display: 'flex',
                    position: 'absolute',
                    top: '29%',
                    left: '94%',
                    backgroundColor: '#FFF2E7',
                    flexDirection: 'column',
                    padding: '12px',
                    width: '92px',
                    borderRadius: '8px',
                    boxShadow: '0px 0px 6px #E7E9F2',
                    cursor: 'pointer'
                }}>
                <RollbackOutlined
                    style={{
                        color: '#ffffff',
                        width: '28px',
                        height: '28px',
                        backgroundColor: '#FF9138',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: '8px',
                        marginBottom: '4px',
                        alignSelf: 'center',
                    }} />
                <Typography
                    style={{
                        fontFamily: 'Nunito-SemiBold',
                        fontSize: '14px',
                        color: '#FF9138',
                        textAlign: 'center',
                    }}>
                    Quay lại
                </Typography>
            </Link>
        </div>
    )
}

export default DetailServiceContent