import {
    Button,
    Input,
    Typography,
    Checkbox
} from 'antd'
import React, {
    useState,
    useEffect
} from 'react';
import type { CheckboxChangeEvent } from 'antd/es/checkbox';
import {
    Link,
    useLocation,
    useNavigate
} from 'react-router-dom';
import { updateService } from '../../features/Service/serviceSlice';
import { useDispatch } from 'react-redux';
function UpdateServiceContent() {
    const onChange = (e: CheckboxChangeEvent) => {
        console.log(`checked = ${e.target.checked}`);
    };
    const navigate = useNavigate();
    const [idService, setIdService] = useState("");
    const [name, setName] = useState("");
    const [describe, setDecribe] = useState("");
    const [active, setActive] = useState("Hoạt động");
    const dispatch = useDispatch();
    const handleUpdate = async () => {
        try {
            await dispatch(
                updateService({
                    id: id,
                    docData: {
                        idService,
                        name,
                        describe,
                        active,
                    }
                }) as any
            );
            console.log('successfully added a new user');
        } catch (error) {
            console.log('error:', error);
        }
    };
    const [id, setId] = useState('');
    const location = useLocation();
    const path = location.pathname;
    const searchTerm = "id=";
    useEffect(() => {
        const startIndex = path.indexOf(searchTerm);
        if (startIndex !== -1) {
            const desiredValue = path.substring(startIndex + searchTerm.length);
            setId(desiredValue);
        }
        console.log(id);
    })
    const { TextArea } = Input;
    return (
        <div
            style={{
                margin: '0 20px'
            }}
        >
            <Typography
                style={{
                    color: '#FF7506',
                    fontFamily: 'Nunito-Bold',
                    fontSize: '24px'
                }}
            >
                Quản lý dịch vụ
            </Typography>
            <div
                style={{
                    width: '1178px',
                    height: '534px',
                    backgroundColor: '#ffffff',
                    boxShadow: '2px 2px 8px rgba(232, 239, 244, 0.8)',
                    borderRadius: '16px',
                    padding: '20px',
                    margin: '7px 0'
                }}>
                <div>
                    <Typography
                        style={{
                            color: '#FF9138',
                            fontFamily: 'Nunito-Bold',
                            fontSize: '20px'
                        }}>
                        Thông tin dịch vụ
                    </Typography>
                    <div
                        style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            marginTop: '6px',
                            marginBottom: '6px',
                        }}>
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'space-between'
                            }}>
                            <div
                                style={{
                                    display: 'flex',
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-SemiBold',
                                    }}>
                                    Mã dịch vụ:
                                </Typography>
                                <div
                                    style={{
                                        width: '25px',
                                        height: '25px'
                                    }}
                                >
                                    <Typography
                                        style={{
                                            color: 'red',
                                            fontSize: '16px',
                                            alignItems: 'center',
                                            display: 'flex',
                                            height: '32px',
                                            marginLeft: '2px'
                                        }}>
                                        *
                                    </Typography>
                                </div>
                            </div>
                            <Input
                                onChange={(e) => setIdService(e.target.value)}
                                placeholder="Nhập mã dịch vụ"
                                style={{
                                    width: '553px',
                                    height: '44px',
                                    fontSize: '16px'
                                }}
                            />
                            <div
                                style={{
                                    display: 'flex',
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-SemiBold',
                                    }}>
                                    Tên dịch vụ:
                                </Typography>
                                <div
                                    style={{
                                        width: '25px',
                                        height: '25px'
                                    }}
                                >
                                    <Typography
                                        style={{
                                            color: 'red',
                                            fontSize: '16px',
                                            alignItems: 'center',
                                            display: 'flex',
                                            height: '32px',
                                            marginLeft: '2px'
                                        }}>
                                        *
                                    </Typography>
                                </div>
                            </div>
                            <Input
                                onChange={(e) => setName(e.target.value)}
                                placeholder="Nhập tên dịch vụ"
                                style={{
                                    width: '553px',
                                    height: '44px',
                                    fontSize: '16px'
                                }}
                            />
                        </div>

                        <div>
                            <div
                                style={{
                                    display: 'flex',
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-SemiBold',
                                        paddingBottom: '6px'
                                    }}>
                                    Mô tả:
                                </Typography>
                            </div>
                            <TextArea
                                onChange={(e) => setDecribe(e.target.value)}
                                placeholder="Mô tả dịch vụ"
                                style={{
                                    width: '553px',
                                    height: '132px',
                                    fontSize: '16px',
                                    resize: 'none'
                                }}
                            />
                        </div>
                    </div>
                </div>
                <div>
                    <Typography
                        style={{
                            color: '#FF9138',
                            fontFamily: 'Nunito-Bold',
                            fontSize: '20px',
                        }}>
                        Quy tắc cấp số
                    </Typography>
                    <div
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                            margin: '8px 0'
                        }}>
                        <Checkbox
                            onChange={onChange}
                            style={{
                                width: '160px'
                            }}
                        >
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-SemiBold',
                                    fontSize: '16px',
                                }}
                            >
                                Tăng tự động từ:
                            </Typography>
                        </Checkbox>
                        <Input
                            value={'0001'}
                            style={{
                                width: '61px',
                                height: '44px',
                                fontFamily: 'Nunito-Regular',
                                fontSize: '16px',
                            }}>

                        </Input>
                        <div>
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-SemiBold',
                                    fontSize: '16px',
                                    marginInline: '8px'
                                }}
                            >
                                đến
                            </Typography>
                        </div>
                        <Input
                            value={'9999'}
                            style={{
                                width: '61px',
                                height: '44px',
                                fontFamily: 'Nunito-Regular',
                                fontSize: '16px',
                            }}>

                        </Input>
                    </div>
                    <div
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                            margin: '8px 0'
                        }}>
                        <Checkbox
                            onChange={onChange}
                            style={{
                                width: '160px'
                            }}
                        >
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-SemiBold',
                                    fontSize: '16px',
                                }}
                            >
                                Prefix:
                            </Typography>
                        </Checkbox>
                        <Input
                            value={'0001'}
                            style={{
                                width: '61px',
                                height: '44px',
                                fontFamily: 'Nunito-Regular',
                                fontSize: '16px',
                            }}>

                        </Input>
                    </div>
                    <div
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                            margin: '8px 0'
                        }}>
                        <Checkbox
                            onChange={onChange}
                            style={{
                                width: '160px'
                            }}
                        >
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-SemiBold',
                                    fontSize: '16px',
                                }}
                            >
                                Surfix:
                            </Typography>
                        </Checkbox>
                        <Input
                            value={'0001'}
                            style={{
                                width: '61px',
                                height: '44px',
                                fontFamily: 'Nunito-Regular',
                                fontSize: '16px',
                            }}>

                        </Input>
                    </div>
                    <div
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                            margin: '8px 0'
                        }}>
                        <Checkbox
                            onChange={onChange}
                            style={{
                                width: '160px'
                            }}
                        >
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-SemiBold',
                                    fontSize: '16px',
                                }}
                            >
                                Reset mỗi ngày
                            </Typography>
                        </Checkbox>
                    </div>
                </div>
                <div
                    style={{
                        display: 'flex',
                        paddingTop: '6px',
                        paddingBottom: '6px',
                    }}>
                    <Typography
                        style={{
                            color: 'red',
                            fontSize: '14px',
                            alignItems: 'center',
                            display: 'flex',
                            height: '28px',
                            marginRight: '2px'
                        }}>
                        *
                    </Typography>
                    <Typography
                        style={{
                            fontSize: '14px',
                            fontFamily: 'Nunito-Regular',
                            color: '#7E7D88'
                        }}>
                        Là trường thông tin bắt buộc
                    </Typography>
                </div>
            </div>
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                <div
                    style={{
                        width: '320px',
                        display: 'flex',
                        justifyContent: 'space-between'
                    }}
                >

                    <Link
                        to={'/service/list-service'}>
                        <Button
                            style={{
                                width: '147px',
                                height: '48px',
                                borderRadius: '8px',
                                backgroundColor: '#FFF2E7',
                                borderWidth: '1.5px',
                                borderColor: '#FF9138',

                            }}>
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-Bold',
                                    fontSize: '16px',
                                    color: '#FF9138',
                                }}>
                                Huỷ bỏ
                            </Typography>
                        </Button>
                    </Link>
                    <Button
                        onClick={() => handleUpdate()}
                        style={{
                            width: '147px',
                            height: '48px',
                            borderRadius: '8px',
                            backgroundColor: '#FF9138'
                        }}>
                        <Typography
                            style={{
                                fontFamily: 'Nunito-Bold',
                                fontSize: '16px',
                                color: '#ffffff'
                            }}>
                            Cập nhật
                        </Typography>
                    </Button>
                </div>
            </div>
        </div >
    )
}

export default UpdateServiceContent