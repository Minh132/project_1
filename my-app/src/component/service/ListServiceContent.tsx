import {
    Input,
    Select,
    Table,
    Typography,
    DatePicker
} from 'antd'
import React, {
    useState,
    useEffect
} from 'react'
import {
    CaretDownOutlined,
    SearchOutlined,
    PlusOutlined,
    CalendarOutlined
} from '@ant-design/icons';
import { Link } from 'react-router-dom';
import type {
    DatePickerProps,
    RangePickerProps
} from 'antd/es/date-picker';
import { Service } from '../../types/Service';
import {
    DocumentData,
    onSnapshot,
    QuerySnapshot
} from "firebase/firestore";
import { servicesCollection } from '../../features/Service/serviceSlice';
function ListServiceContent() {
    const [services, setServices] = useState<Service[]>([]);
    const [searchKeyword, setSearchKeyword] = useState<string>('');
    const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchKeyword(event.target.value);
    };
    const [filteredServices, setFilteredServices] = useState<Service[]>([]);
    const [selectedActive, setSelectedActive] = useState<string>('Tất cả');
    const { RangePicker } = DatePicker;
    const onChangee = (
        value: DatePickerProps['value'] | RangePickerProps['value'],
        dateString: [string, string] | string,
    ) => {
        console.log('Selected Time: ', value);
        console.log('Formatted Selected Time: ', dateString);
    };
    const handleActive = (value: string) => {
        setSelectedActive(value);
        if (value === 'Tất cả') {
            setFilteredServices(services); // Hiển thị tất cả dữ liệu
        } else {
            const filteredData = services.filter(service => service.active === value);
            setFilteredServices(filteredData);
        }
    };
    const onOk = (value: DatePickerProps['value'] | RangePickerProps['value']) => {
        console.log('onOk: ', value);
    };
    useEffect(() => {
        const unsubscribe = onSnapshot(servicesCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const serviceData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setServices(serviceData);
        });

        return () => {
            // Cleanup function to unsubscribe from the snapshot listener
            unsubscribe();
        };
    }, []);
    const onChange: DatePickerProps['onChange'] = (date, dateString) => {
        console.log(date, dateString);
    };
    const handleChange = (value: { value: string; label: React.ReactNode }) => {
        console.log(value); // { value: "lucy", key: "lucy", label: "Lucy (101)" }
    }
    const columns = [
        {
            title: 'Mã dịch vụ',
            dataIndex: 'idService',
            key: 'idService',
            render: (idService: string) => {
                return (
                    <div
                        style={{
                            minWidth: '70px'
                        }}>
                        <Typography
                            style={{
                                fontFamily: 'Nunito-Regular',
                                fontSize: '14px'
                            }}>
                            {idService}
                        </Typography>
                    </div>
                );
            },
        },
        {
            title: 'Tên dịch vụ',
            dataIndex: 'name',
            key: 'name',
            render: (name: string) => {
                return (
                    <div
                        style={{
                            minWidth: '310px',
                            maxWidth: '310px'
                        }}>
                        <Typography
                            style={{
                                fontFamily: 'Nunito-Regular',
                                fontSize: '14px'
                            }}>
                            {name}
                        </Typography>
                    </div>
                );
            },
        },
        {
            title: 'Mô tả',
            dataIndex: 'describe',
            key: 'describe',
            render: (describe: string) => {
                return (
                    <div
                        style={{
                            minWidth: '310px',
                            maxWidth: '310px'
                        }}>
                        <Typography
                            style={{
                                fontFamily: 'Nunito-Regular',
                                fontSize: '14px'
                            }}>
                            {describe}
                        </Typography>
                    </div>
                );
            },
        },
        {
            title: 'Trạng thái hoạt động',
            dataIndex: 'active',
            key: 'active',
            render: (active: string) => {
                return (
                    <>
                        {active === "Hoạt động"
                            ?
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'center'
                                }}>
                                <div
                                    style={{
                                        width: '6px',
                                        height: '6px',
                                        backgroundColor: '#34CD26',
                                        borderRadius: '100%',
                                        marginRight: '2px'
                                    }}></div>
                                <Typography
                                    style={{
                                        fontFamily: 'Nunito-Regular',
                                        fontSize: '14px'
                                    }}>
                                    Hoạt động
                                </Typography>
                            </div>
                            : ""}
                        {active === "Ngưng hoạt động" ?
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'center'
                                }}>
                                <div
                                    style={{
                                        width: '6px',
                                        height: '6px',
                                        backgroundColor: '#EC3740',
                                        borderRadius: '100%',
                                        marginRight: '2px'
                                    }}></div>
                                <Typography
                                    style={{
                                        fontFamily: 'Nunito-Regular',
                                        fontSize: '14px'
                                    }}>
                                    Ngưng hoạt động
                                </Typography>
                            </div>
                            : ""}
                    </>
                );
            },
        },
        {
            title: ' ',
            dataIndex: 'detail',
            key: 'detail',
            render: (_: any, record: Service) => {
                return (
                    <Link
                        to={`/service/list-service/detail-service/id=${record.id}`}>
                        <Typography.Link
                            style={{
                                textDecoration: 'underline',
                                fontFamily: 'Nunito-Regular',
                                fontSize: '14px'
                            }}>
                            Chi tiết
                        </Typography.Link>
                    </Link>
                );
            },
        },
        {
            title: ' ',
            dataIndex: 'update',
            key: 'update',
            render: (_: any, record: Service) => {
                return (
                    <Link
                        to={`/service/list-service/update-service/id=${record.id}`}>
                        <Typography.Link
                            style={{
                                textDecoration: 'underline',
                                fontFamily: 'Nunito-Regular',
                                fontSize: '14px'
                            }}>
                            Cập nhật
                        </Typography.Link>
                    </Link>
                );
            },
        },
    ];
    return (
        <div
            style={{
                margin: '0 20px',
                width: '1112px'
            }}>
            <Typography
                style={{
                    fontSize: '24px',
                    fontFamily: 'Nunito-Bold',
                    color: '#FF7506',
                }}>
                Danh sách thiết bị
            </Typography>
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                }}>
                <div
                    style={{
                        display: 'flex'
                    }}>
                    <div
                        style={{
                            marginRight: '20px'
                        }}>
                        <Typography
                            style={{
                                fontSize: '16px',
                                fontFamily: 'Nunito-SemiBold',
                            }}>
                            Trạng thái hoạt động
                        </Typography>
                        <Select
                            defaultValue="Tất cả"
                            onChange={handleActive}
                            listHeight={180}
                            suffixIcon={<CaretDownOutlined
                                style={{
                                    color: '#FF7506'
                                }}
                            />}
                            style={{
                                width: '300px',
                                height: '44px',
                                borderRadius: '8px',
                                display: 'flex',
                                justifyContent: 'space-between'
                            }}
                        >
                            <Select.Option
                                className='timeSelect'
                                key={1}
                                value="Tất cả"
                                Title="Tất cả"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Tất cả
                            </Select.Option>
                            <Select.Option
                                className='timeSelect'
                                key={2}
                                value="Hoạt động"
                                Title="Hoạt động"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Hoạt động
                            </Select.Option>
                            <Select.Option
                                className='timeSelect'
                                key={3}
                                value="Ngưng hoạt động"
                                Title="Ngưng hoạt động"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Ngưng hoạt động
                            </Select.Option>
                        </Select>
                    </div>
                    <div>
                        <Typography
                            style={{
                                fontSize: '16px',
                                fontFamily: 'Nunito-SemiBold',
                            }}>
                            Chọn thời gian
                        </Typography>
                        <RangePicker
                            suffixIcon={<CalendarOutlined
                                style={{
                                    color: '#FF7506'
                                }}
                            />}
                            format="DD/MM/YYYY"
                            onChange={onChangee}
                            onOk={onOk}
                        />
                    </div>
                </div>
                <div>
                    <Typography
                        style={{
                            fontSize: '16px',
                            fontFamily: 'Nunito-SemiBold',
                        }}>
                        Từ khoá
                    </Typography>
                    <Input
                        onChange={handleSearch}
                        placeholder="Nhập từ khóa"
                        style={{
                            width: '300px',
                            height: '42px'
                        }}
                        suffix={
                            <SearchOutlined
                                style={{
                                    color: '#FF7506',
                                    fontSize: '16px'
                                }} />
                        }
                    />
                </div>
            </div>
            <div
                style={{
                    height: '490px'
                }}
            >
                <Table
                    pagination={{ pageSize: 9 }}
                    dataSource={
                        selectedActive === 'Tất cả'
                            ? services && services.filter(
                                service =>
                                    service && service.name && service.name && service.name.toLowerCase().includes(searchKeyword.toLowerCase())
                            )
                            : filteredServices.filter(
                                service =>
                                    service &&
                                    (service.active === selectedActive)

                            )
                    }
                    columns={columns}
                    rowClassName={(record, index) =>
                        index % 2 === 0 ? "bg-white" : "bg-orange-50"
                    }
                    style={{
                        height: '490px',
                        marginTop: '4px'
                    }}
                />
            </div>
            <Link
                to={'/service/list-service/add-service'}
                style={{
                    display: 'flex',
                    position: 'absolute',
                    top: '24.4%',
                    left: '94%',
                    backgroundColor: '#FFF2E7',
                    flexDirection: 'column',
                    padding: '12px',
                    width: '92px',
                    borderRadius: '8px',
                    boxShadow: '0px 0px 6px #E7E9F2',
                    cursor: 'pointer'
                }}>
                <PlusOutlined
                    style={{
                        color: '#ffffff',
                        width: '28px',
                        height: '28px',
                        backgroundColor: '#FF9138',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: '8px',
                        marginBottom: '4px',
                        alignSelf: 'center',
                    }} />
                <Typography
                    style={{
                        fontFamily: 'Nunito-SemiBold',
                        fontSize: '14px',
                        color: '#FF9138',
                        textAlign: 'center',
                    }}>
                    Thêm dịch vụ
                </Typography>
            </Link>
        </div>
    )
}

export default ListServiceContent