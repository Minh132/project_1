import {
    Table,
    Typography,
    DatePicker
} from 'antd'
import {
    DownloadOutlined,
    CalendarOutlined
} from '@ant-design/icons';
import { Link } from 'react-router-dom';
import type {
    DatePickerProps,
    RangePickerProps
} from 'antd/es/date-picker';
import React, {
    useState,
    useEffect
} from 'react'
import {
    DocumentData,
    onSnapshot,
    QuerySnapshot
} from "firebase/firestore";
import { Progression } from '../../types/Progression';
import moment from 'moment';
import { progressionCollection } from '../../features/Progression/progressionSlice';

function ListReportContent() {
    const { RangePicker } = DatePicker;
    const [filteredProgressions, setFilteredProgressions] = useState<Progression[]>([]);
    const onChangee = (
        value: DatePickerProps['value'] | RangePickerProps['value'],
        dateString: [string, string] | string
    ) => {
        console.log('Selected Time: ', value);
        console.log('Formatted Selected Time: ', dateString);

        if (!value) {
            setFilteredProgressions(progressions); // Hiển thị tất cả các mục khi không có ngày được chọn
            return;
        }

        if (Array.isArray(dateString)) {
            const startDate = moment(dateString[0], 'DD/MM/YYYY');
            const endDate = moment(dateString[1], 'DD/MM/YYYY');

            const filteredData = progressions.filter(progression => {
                if (progression.dayStart && progression.dayEnd) {
                    const dayStart = progression.dayStart.split(' - ')[1];

                    const formattedDayStart = moment(dayStart, 'DD/MM/YYYY');
                    const formattedDayEnd = moment(dayStart, 'DD/MM/YYYY');

                    const isWithinRange =
                        formattedDayStart.isBetween(startDate, endDate, null, '[]') &&
                        formattedDayEnd.isBetween(startDate, endDate, null, '[]');

                    return isWithinRange;
                }

                return false;
            });

            setFilteredProgressions(filteredData);
        } else {
            setFilteredProgressions([]);
        }
    };

    const onOk = (value: DatePickerProps['value'] | RangePickerProps['value']) => {
        console.log('onOk: ', value);
    };
    const [progressions, setProgressions] = useState<Progression[]>([]);
    useEffect(() => {
        const unsubscribe = onSnapshot(progressionCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const progressionData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setProgressions(progressionData);
        });

        return () => {
            unsubscribe();
        };
    }, []);
    useEffect(() => {
        setFilteredProgressions(progressions);
    }, [progressions]);
    const columns = [
        {
            title: 'Số thứ tự',
            dataIndex: 'stt',
            key: 'stt',
            render: (stt: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {stt}
                    </Typography>
                );
            },
        },
        {
            title: 'Tên dịch vụ',
            dataIndex: 'service',
            key: 'service',
            render: (service: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {service}
                    </Typography>
                );
            },
        },
        {
            title: 'Thời gian cấp',
            dataIndex: 'dayStart',
            key: 'dayStart',
            render: (dayStart: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {dayStart}
                    </Typography>
                );
            },
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            key: 'status',
            render: (status: string) => {
                return (
                    <>
                        {status === 'Đang chờ'
                            ?
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'center'
                                }}>
                                <div
                                    style={{
                                        width: '6px',
                                        height: '6px',
                                        backgroundColor: '#4277FF',
                                        borderRadius: '100%',
                                        marginRight: '2px'
                                    }}></div>
                                <Typography
                                    style={{
                                        fontFamily: 'Nunito-Regular',
                                        fontSize: '14px'
                                    }}>
                                    Đang chờ
                                </Typography>
                            </div>
                            : ""
                        }
                        {
                            status === 'Đã sử dụng'
                                ?
                                <div
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center'
                                    }}>
                                    <div
                                        style={{
                                            width: '6px',
                                            height: '6px',
                                            backgroundColor: '#7E7D88',
                                            borderRadius: '100%',
                                            marginRight: '2px'
                                        }}></div>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-Regular',
                                            fontSize: '14px'
                                        }}>
                                        Đã sử dụng
                                    </Typography>
                                </div>
                                : ""
                        }
                        {
                            status === 'Bỏ qua'
                                ?
                                <div
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center'
                                    }}>
                                    <div
                                        style={{
                                            width: '6px',
                                            height: '6px',
                                            backgroundColor: '#E73F3F',
                                            borderRadius: '100%',
                                            marginRight: '2px'
                                        }}></div>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-Regular',
                                            fontSize: '14px'
                                        }}>
                                        Bỏ qua
                                    </Typography>
                                </div>
                                : ""
                        }
                    </>
                );
            },
        },
        {
            title: 'Nguồn cấp',
            dataIndex: 'provide',
            key: 'provide',
            render: (provide: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {provide}
                    </Typography>
                );
            },
        },
    ];
    return (
        <div
            style={{
                margin: '0 20px',
                width: '1112px',
            }}>
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                }}>
                <div
                    style={{
                        display: 'flex',
                        width: '100%',
                        justifyContent: 'space-between',
                    }}>
                    <div>
                        <Typography
                            style={{
                                fontSize: '16px',
                                fontFamily: 'Nunito-SemiBold',
                            }}>
                            Chọn thời gian
                        </Typography>
                        <RangePicker
                            suffixIcon={<CalendarOutlined
                                style={{
                                    color: '#FF7506'
                                }}
                            />}
                            format="DD/MM/YYYY"
                            onChange={onChangee}
                            onOk={onOk}
                        />
                    </div>
                </div>
            </div>
            <div
                style={{
                    height: '490px'
                }}
            >
                <Table
                    pagination={{ pageSize: 10 }}
                    dataSource={filteredProgressions}
                    columns={columns}
                    rowClassName={(record, index) =>
                        index % 2 === 0 ? "bg-white" : "bg-orange-50"
                    }
                    style={{
                        height: '490px',
                        marginTop: '4px'
                    }}
                />
            </div>
            <Link
                to={'/progression/list-progression/new-progression'}
                style={{
                    display: 'flex',
                    position: 'absolute',
                    top: '18.4%',
                    left: '94%',
                    backgroundColor: '#FFF2E7',
                    flexDirection: 'column',
                    padding: '12px',
                    width: '92px',
                    borderRadius: '8px',
                    boxShadow: '0px 0px 6px #E7E9F2',
                    cursor: 'pointer'
                }}>
                <DownloadOutlined
                    style={{
                        color: '#ffffff',
                        width: '28px',
                        height: '28px',
                        backgroundColor: '#FF9138',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: '8px',
                        marginBottom: '4px',
                        alignSelf: 'center',
                    }} />
                <Typography
                    style={{
                        fontFamily: 'Nunito-SemiBold',
                        fontSize: '14px',
                        color: '#FF9138',
                        textAlign: 'center',
                    }}>
                    Tải về
                </Typography>
            </Link>
        </div>
    )
}

export default ListReportContent