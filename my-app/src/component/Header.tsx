import {
    BellFilled
} from '@ant-design/icons';
import {
    Image,
    Typography
} from 'antd';
import BreadCrumb from './BreadCrumb';
import React, {
    useState,
    useEffect,
    useContext
} from 'react'
import {
    Link,
    useLocation
} from 'react-router-dom';
import {
    DocumentData,
    onSnapshot,
    QuerySnapshot
} from "firebase/firestore";
import { User } from '../types/User';
import { IdContext } from '../IdContext';
import { usersCollection } from '../features/User/userSlice';
function Header() {
    const [users, setUsers] = useState<User[]>([]);
    const location = useLocation();
    const path = location.pathname;
    const searchTerm = "id=";
    const { id } = useContext(IdContext);
    useEffect(() => {
        const unsubscribe = onSnapshot(usersCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const userData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setUsers(userData);
        });

        return () => {
            unsubscribe();
        };
    }, []);
    useEffect(() => {
        const startIndex = path.indexOf(searchTerm);
        if (startIndex !== -1) {
            const desiredValue = path.substring(startIndex + searchTerm.length);
            // setId(desiredValue);
        }
        console.log(id);
    })
    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'space-between',
                zIndex: 1,
            }}>
            <div
                style={{
                    paddingInline: '20px'
                }}>
                <BreadCrumb />
            </div>
            <div
                style={{
                    display: 'flex',
                    width: '350px',
                    justifyContent: 'space-between',
                    margin: '10px'
                }}>
                <div
                    style={{
                        display: 'flex',
                        alignItems: 'center'
                    }}>
                    <BellFilled
                        style={{
                            color: '#FFAC6A',
                            fontSize: '16px'
                        }} />
                </div>
                <div
                    style={{
                        display: 'flex',
                        minWidth: '300px'
                    }}>
                    <div>
                        <Link
                            to={`/account-info/id=${id}`}>
                            <Image
                                preview={false}
                                src='https://images.unsplash.com/photo-1575936123452-b67c3203c357?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8aW1hZ2V8ZW58MHx8MHx8&w=1000&q=80'
                                style={{
                                    borderRadius: '100%',
                                    width: '40px',
                                    height: '40px'
                                }} />
                        </Link>
                    </div>
                    {users.map((user) =>
                        user.id === id ?
                            <div
                                style={{
                                    marginLeft: '6px'
                                }}>
                                <Typography
                                    style={{
                                        fontFamily: 'Nunito-ExtraLight',
                                        fontSize: '12px'
                                    }}>Xin chào</Typography>
                                <Typography
                                    style={{
                                        fontFamily: 'Nunito-Bold',
                                        fontSize: '16px'
                                    }}>{user.name}</Typography>
                            </div>
                            : '')}
                </div>
            </div>
        </div >
    )
}

export default Header