import {
    Image,
    Input,
    Typography
} from 'antd'
import React, {
    useState,
    useEffect,
    useContext
} from 'react'
import {
    CameraOutlined
} from '@ant-design/icons';
import { User } from '../types/User';
import {
    DocumentData,
    onSnapshot,
    QuerySnapshot
} from "firebase/firestore";
import { useLocation } from 'react-router-dom';
import { IdContext } from '../IdContext';
import { usersCollection } from '../features/User/userSlice';
function AccountInfoContent() {
    const [users, setUsers] = useState<User[]>([]);
    const location = useLocation();
    const path = location.pathname;
    const searchTerm = "id=";
    const { id } = useContext(IdContext);
    useEffect(() => {
        const unsubscribe = onSnapshot(usersCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const userData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setUsers(userData);
        });

        return () => {
            unsubscribe();
        };
    }, []);
    useEffect(() => {
        const startIndex = path.indexOf(searchTerm);
        if (startIndex !== -1) {
            const desiredValue = path.substring(startIndex + searchTerm.length);
        }
        console.log(id);
    })
    return (
        <div
            style={{
                margin: '20px'
            }}>
            <div
                style={{
                    marginTop: '44px',
                    backgroundColor: '#ffffff',
                    minHeight: '397px',
                    borderRadius: '12px',
                    display: 'flex',
                    alignItems: 'flex-start',
                    width: '1112px',
                    justifyContent: 'space-evenly',
                    padding: '40px 0'
                }}>
                {users.map((user) =>
                    user.id === id ?
                        <>
                            <div
                                style={{
                                    display: 'flex',
                                    flexDirection: 'column'
                                }}>
                                <div
                                    style={{
                                        position: 'relative'
                                    }}>
                                    <Image
                                        preview={false}
                                        src='https://images.unsplash.com/photo-1575936123452-b67c3203c357?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8aW1hZ2V8ZW58MHx8MHx8&w=1000&q=80'
                                        style={{
                                            width: '248px',
                                            height: '248px',
                                            borderRadius: '100%',
                                        }}
                                    />
                                    <div
                                        style={{
                                            borderRadius: '100%',
                                            backgroundColor: '#FF7506',
                                            width: '40px',
                                            height: '40px',
                                            position: 'absolute',
                                            left: '70%',
                                            top: '84%'
                                        }}>
                                        <CameraOutlined
                                            style={{
                                                color: '#ffffff',
                                                fontSize: '25px',
                                                border: '2px',
                                                textAlign: 'center',
                                                position: 'absolute',
                                                top: '18%',
                                                left: '19%'
                                            }} />
                                    </div>
                                </div>
                                <Typography.Title
                                    level={3}
                                    style={{
                                        fontFamily: 'Nunito-Bold',
                                        textAlign: 'center',
                                        maxWidth: '248px'
                                    }}>
                                    {user.name}
                                </Typography.Title>
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    justifyContent: 'space-between',
                                    height: '260px',
                                    minWidth: '384px'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                    }}>
                                    Tên người dùng
                                </Typography>
                                <Input
                                    disabled
                                    value={user.name}
                                    style={{
                                        userSelect: 'none',
                                        fontSize: '16px',
                                        marginTop: '10px'
                                    }} />
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        marginTop: '30px'
                                    }}>
                                    Số điện thoại
                                </Typography>
                                <Input
                                    disabled
                                    value={user.phone}
                                    style={{
                                        userSelect: 'none',
                                        fontSize: '16px',
                                        marginTop: '10px'
                                    }} />
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        marginTop: '30px'
                                    }}>
                                    Email:
                                </Typography>
                                <Input
                                    disabled
                                    value={user.email}
                                    style={{
                                        userSelect: 'none',
                                        fontSize: '16px',
                                        marginTop: '10px'
                                    }} />
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    justifyContent: 'space-between',
                                    height: '260px',
                                    minWidth: '384px'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                    }}>
                                    Tên đăng nhập
                                </Typography>
                                <Input
                                    disabled
                                    value={user.userAccount}
                                    style={{
                                        userSelect: 'none',
                                        fontSize: '16px',
                                        marginTop: '10px'
                                    }} />
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        marginTop: '30px'
                                    }}>
                                    Mật khẩu
                                </Typography>
                                <Input
                                    disabled
                                    value={user.password}
                                    style={{
                                        userSelect: 'none',
                                        fontSize: '16px',
                                        marginTop: '10px'
                                    }} />
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        marginTop: '30px'
                                    }}>
                                    Vai trò:
                                </Typography>
                                <Input
                                    disabled
                                    value={user.role}
                                    style={{
                                        userSelect: 'none',
                                        fontSize: '16px',
                                        marginTop: '10px'
                                    }} />
                            </div>
                        </> : ''
                )}
            </div>

        </div>
    )
}

export default AccountInfoContent