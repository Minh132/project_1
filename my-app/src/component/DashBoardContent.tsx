import React, {
    useState
} from 'react';
import ReactDOM from 'react-dom';
import {
    Area
} from '@ant-design/plots';
import {
    Card,
    Statistic,
    Typography,
    Select,
    Progress,
    Menu,
    DatePicker,
    Calendar
} from 'antd';
import type {
    DatePickerProps
} from 'antd';
import {
    CalendarOutlined,
    CarryOutOutlined,
    ContactsOutlined,
    BookOutlined,
    ArrowUpOutlined,
    ArrowDownOutlined,
    DesktopOutlined,
    MessageOutlined,
    CopyOutlined
} from '@ant-design/icons';
function DashBoardContent() {
    const [time, setTime] = useState()
    const day = [
        {
            "timePeriod": "1",
            "value": 2800
        },
        {
            "timePeriod": "2",
            "value": 4300
        },
        {
            "timePeriod": "3",
            "value": 3300
        },
        {
            "timePeriod": "4",
            "value": 3600
        },
        {
            "timePeriod": "5",
            "value": 3400
        },
        {
            "timePeriod": "6",
            "value": 3400
        },
        {
            "timePeriod": "7",
            "value": 3500
        },
        {
            "timePeriod": "8",
            "value": 3300
        },
        {
            "timePeriod": "9",
            "value": 3200
        },
        {
            "timePeriod": "10",
            "value": 4000
        },
        {
            "timePeriod": "11",
            "value": 3900
        },
        {
            "timePeriod": "12",
            "value": 3800
        },
        {
            "timePeriod": "13",
            "value": 3700
        },
        {
            "timePeriod": "14",
            "value": 3300
        },
        {
            "timePeriod": "15",
            "value": 3100
        },
        {
            "timePeriod": "16",
            "value": 3200
        },
        {
            "timePeriod": "17",
            "value": 3400
        },
        {
            "timePeriod": "18",
            "value": 3500
        },
        {
            "timePeriod": "19",
            "value": 3000
        },
        {
            "timePeriod": "20",
            "value": 3100
        },
        {
            "timePeriod": "21",
            "value": 3300
        },
        {
            "timePeriod": "22",
            "value": 3200
        },
        {
            "timePeriod": "23",
            "value": 3600
        },
        {
            "timePeriod": "24",
            "value": 3300
        },
        {
            "timePeriod": "25",
            "value": 3700
        },
        {
            "timePeriod": "26",
            "value": 3900
        },
        {
            "timePeriod": "27",
            "value": 3800
        },
        {
            "timePeriod": "28",
            "value": 4221
        },
        {
            "timePeriod": "29",
            "value": 3800
        },
        {
            "timePeriod": "30",
            "value": 4500
        },
        {
            "timePeriod": "31",
            "value": 3700
        },
    ]
    const week = [
        {
            "timePeriod": "1",
            "value": 2800
        },
        {
            "timePeriod": "2",
            "value": 4300
        },
        {
            "timePeriod": "3",
            "value": 3300
        },
        {
            "timePeriod": "4",
            "value": 3600
        },
    ]
    const month = [
        {
            "timePeriod": "1",
            "value": 2800
        },
        {
            "timePeriod": "2",
            "value": 4300
        },
        {
            "timePeriod": "3",
            "value": 3300
        },
        {
            "timePeriod": "4",
            "value": 3600
        },
        {
            "timePeriod": "5",
            "value": 3400
        },
        {
            "timePeriod": "6",
            "value": 4221
        },
        {
            "timePeriod": "7",
            "value": 3800
        },
        {
            "timePeriod": "8",
            "value": 4500
        },
        {
            "timePeriod": "9",
            "value": 4000
        },
        {
            "timePeriod": "10",
            "value": 3900
        },
        {
            "timePeriod": "11",
            "value": 3800
        },
        {
            "timePeriod": "12",
            "value": 3700
        },
    ]
    const config = {
        data: day,
        xField: 'timePeriod',
        yField: 'value',
        xAxis: {
            range: [0, 1],
        },
        smooth: true,
        startOnZero: true,
    };
    const handleChange = (value: { value: string; label: React.ReactNode }) => {
        console.log(value); // { value: "lucy", key: "lucy", label: "Lucy (101)" }
    }
    const onChange: DatePickerProps['onChange'] = (date, dateString) => {
        console.log(date, dateString);
    };
    return (
        <div
            style={{
                display: 'flex',
            }}>
            <div
                style={{
                    margin: '0 20px',
                }}>
                <div
                    style={{
                        width: '791px',
                    }}>
                    <Typography
                        style={{
                            fontSize: '24px',
                            fontFamily: 'Nunito-Bold',
                            color: '#FF7506',
                            marginBottom: '8px'
                        }}>
                        Biểu đồ cấp số
                    </Typography>
                    <div
                        style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            marginBottom: '8px'
                        }}>
                        <div
                            style={{
                                width: '186px',
                                height: '120px',
                                backgroundColor: '#FFFFFF',
                                borderRadius: '10px',
                                padding: '10px',
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'space-between',
                            }}>
                            <div
                                style={{
                                    display: 'flex'
                                }}>
                                <div
                                    style={{
                                        width: '48px',
                                        height: '48px',
                                        backgroundColor: 'rgba(102,149,251,0.15)',
                                        display: 'flex',
                                        borderRadius: '100%',
                                        position: 'relative'
                                    }}>
                                    <CalendarOutlined
                                        style={{
                                            color: '#6493F9',
                                            fontSize: '24px',
                                            position: 'absolute',
                                            left: '12px',
                                            top: '12px'
                                        }} />
                                </div>
                                <div
                                    style={{
                                        width: '72px',
                                        display: 'flex',
                                        alignItems: 'center',
                                        marginLeft: '10px'
                                    }}>
                                    <Typography
                                        style={{
                                            fontSize: '14px',
                                            fontFamily: 'Nunito-Bold',
                                            lineHeight: '18px'
                                        }}>
                                        Số thứ tự đã cấp
                                    </Typography>
                                </div>

                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'space-between'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '30px',
                                        fontFamily: 'Nunito-Bold',
                                        lineHeight: '45px'
                                    }}>
                                    4.221
                                </Typography>
                                <Card
                                    style={{
                                        width: '48px',
                                        height: '15px',
                                        padding: 0,
                                        border: 0,
                                        textAlign: 'center',
                                        backgroundColor: 'rgba(255, 149, 1, 0.15)',

                                    }}
                                    bodyStyle={{
                                        width: '48px',
                                        height: '15px',
                                        padding: '0px',
                                    }}>
                                    <Statistic
                                        value={32.41}
                                        precision={2}
                                        valueStyle={{
                                            color: '#FF9138',
                                            fontSize: '8px',
                                            width: '48px',
                                            height: '15px',
                                            display: 'flex',
                                            alignItems: 'center',
                                            justifyContent: 'center'

                                        }}
                                        prefix={<ArrowUpOutlined />}
                                        suffix="%"
                                    />
                                </Card>
                            </div>
                        </div>
                        <div
                            style={{
                                width: '186px',
                                height: '120px',
                                backgroundColor: '#FFFFFF',
                                borderRadius: '10px',
                                padding: '10px',
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'space-between',
                            }}>
                            <div
                                style={{
                                    display: 'flex'
                                }}>
                                <div
                                    style={{
                                        width: '48px',
                                        height: '48px',
                                        backgroundColor: 'rgba(53,199,90,0.15)',
                                        display: 'flex',
                                        borderRadius: '100%',
                                        position: 'relative'
                                    }}>
                                    <CarryOutOutlined
                                        style={{
                                            color: '#35C75A',
                                            fontSize: '24px',
                                            position: 'absolute',
                                            left: '12px',
                                            top: '12px'
                                        }} />
                                </div>
                                <div
                                    style={{
                                        width: '72px',
                                        display: 'flex',
                                        alignItems: 'center',
                                        marginLeft: '10px'
                                    }}>
                                    <Typography
                                        style={{
                                            fontSize: '14px',
                                            fontFamily: 'Nunito-Bold',
                                            lineHeight: '18px'
                                        }}>
                                        Số thứ tự đã sử dụng
                                    </Typography>
                                </div>
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'space-between'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '30px',
                                        fontFamily: 'Nunito-Bold',
                                        lineHeight: '45px'
                                    }}>
                                    3.721
                                </Typography>
                                <Card
                                    style={{
                                        width: '48px',
                                        height: '15px',
                                        padding: 0,
                                        border: 0,
                                        textAlign: 'center',
                                        backgroundColor: 'rgba(231, 63, 63, 0.15)',

                                    }}
                                    bodyStyle={{
                                        width: '48px',
                                        height: '15px',
                                        padding: '0px',
                                    }}>
                                    <Statistic
                                        value={32.41}
                                        precision={2}
                                        valueStyle={{
                                            color: '#E73F3F',
                                            fontSize: '8px',
                                            width: '48px',
                                            height: '15px',
                                            display: 'flex',
                                            alignItems: 'center',
                                            justifyContent: 'center'

                                        }}
                                        prefix={<ArrowDownOutlined />}
                                        suffix="%"
                                    />
                                </Card>
                            </div>
                        </div>
                        <div
                            style={{
                                width: '186px',
                                height: '120px',
                                backgroundColor: '#FFFFFF',
                                borderRadius: '10px',
                                padding: '10px',
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'space-between',
                            }}>
                            <div
                                style={{
                                    display: 'flex'
                                }}>
                                <div
                                    style={{
                                        width: '48px',
                                        height: '48px',
                                        backgroundColor: 'rgba(255,172,106,0.15)',
                                        display: 'flex',
                                        borderRadius: '100%',
                                        position: 'relative'
                                    }}>
                                    <ContactsOutlined
                                        style={{
                                            color: '#FFAC6A',
                                            fontSize: '24px',
                                            position: 'absolute',
                                            left: '12px',
                                            top: '12px'
                                        }} />
                                </div>
                                <div
                                    style={{
                                        width: '72px',
                                        display: 'flex',
                                        alignItems: 'center',
                                        marginLeft: '10px'
                                    }}>
                                    <Typography
                                        style={{
                                            fontSize: '14px',
                                            fontFamily: 'Nunito-Bold',
                                            lineHeight: '18px'
                                        }}>
                                        Số thứ tự đang chờ
                                    </Typography>
                                </div>

                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'space-between'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '30px',
                                        fontFamily: 'Nunito-Bold',
                                        lineHeight: '45px'
                                    }}>
                                    468
                                </Typography>
                                <Card
                                    style={{
                                        width: '48px',
                                        height: '15px',
                                        padding: 0,
                                        border: 0,
                                        textAlign: 'center',
                                        backgroundColor: 'rgba(255, 149, 1, 0.15)',

                                    }}
                                    bodyStyle={{
                                        width: '48px',
                                        height: '15px',
                                        padding: '0px',
                                    }}>
                                    <Statistic
                                        value={56.41}
                                        precision={2}
                                        valueStyle={{
                                            color: '#FF9138',
                                            fontSize: '8px',
                                            width: '48px',
                                            height: '15px',
                                            display: 'flex',
                                            alignItems: 'center',
                                            justifyContent: 'center'

                                        }}
                                        prefix={<ArrowUpOutlined />}
                                        suffix="%"
                                    />
                                </Card>
                            </div>
                        </div>
                        <div
                            style={{
                                width: '186px',
                                height: '120px',
                                backgroundColor: '#FFFFFF',
                                borderRadius: '10px',
                                padding: '10px',
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'space-between',
                            }}>
                            <div
                                style={{
                                    display: 'flex'
                                }}>
                                <div
                                    style={{
                                        width: '48px',
                                        height: '48px',
                                        backgroundColor: 'rgba(248,109,109,0.15)',
                                        display: 'flex',
                                        borderRadius: '100%',
                                        position: 'relative'
                                    }}>
                                    <BookOutlined
                                        style={{
                                            color: '#F86D6D',
                                            fontSize: '24px',
                                            position: 'absolute',
                                            left: '12px',
                                            top: '12px'
                                        }} />
                                </div>
                                <div
                                    style={{
                                        width: '72px',
                                        display: 'flex',
                                        alignItems: 'center',
                                        marginLeft: '10px'
                                    }}>
                                    <Typography
                                        style={{
                                            fontSize: '14px',
                                            fontFamily: 'Nunito-Bold',
                                            lineHeight: '18px'
                                        }}>
                                        Số thứ tự đã bỏ qua
                                    </Typography>
                                </div>

                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'space-between'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '30px',
                                        fontFamily: 'Nunito-Bold',
                                        lineHeight: '45px'
                                    }}>
                                    32
                                </Typography>
                                <Card
                                    style={{
                                        width: '48px',
                                        height: '15px',
                                        padding: 0,
                                        border: 0,
                                        textAlign: 'center',
                                        backgroundColor: 'rgba(231, 63, 63, 0.15)',

                                    }}
                                    bodyStyle={{
                                        width: '48px',
                                        height: '15px',
                                        padding: '0px',
                                    }}>
                                    <Statistic
                                        value={22.41}
                                        precision={2}
                                        valueStyle={{
                                            color: '#E73F3F',
                                            fontSize: '8px',
                                            width: '48px',
                                            height: '15px',
                                            display: 'flex',
                                            alignItems: 'center',
                                            justifyContent: 'center'

                                        }}
                                        prefix={<ArrowUpOutlined />}
                                        suffix="%"
                                    />
                                </Card>
                            </div>
                        </div>
                    </div>
                    <div
                        style={{
                            minHeight: '440px',
                            maxHeight: '440px',
                            borderRadius: '12px',
                            backgroundColor: '#FFFFFF',
                            padding: '20px'
                        }}>
                        <div
                            style={{
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                            }}>
                            <div>
                                <div>
                                    <Typography
                                        style={{
                                            fontSize: '20px',
                                            fontFamily: 'Nunito-Bold'
                                        }}>
                                        Bảng thống kê theo ngày
                                    </Typography>
                                </div>
                                <div>
                                    <Typography
                                        style={{
                                            fontSize: '14px',
                                            fontFamily: 'Nunito-Regular',
                                            color: '#A9A9B0'
                                        }}>
                                        Tháng 11/2021
                                    </Typography>
                                </div>
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'center'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-SemiBold',
                                        marginRight: '10px'
                                    }}>
                                    Xem theo
                                </Typography>
                                <Select
                                    labelInValue
                                    defaultValue={{ value: 'jack', label: 'Ngày' }}
                                    onChange={handleChange}
                                    style={{
                                        width: '106px'
                                    }}
                                >
                                    <Select.Option
                                        className='timeSelect'
                                        key={1}
                                        Title="Ngày"
                                        style={{
                                            height: '44px',
                                            alignItems: 'center',
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-Regular'
                                        }}
                                    >
                                        Ngày
                                    </Select.Option>
                                    <Select.Option
                                        className='timeSelect'
                                        key={2}
                                        Title="Tuần"
                                        style={{
                                            height: '44px',
                                            alignItems: 'center',
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-Regular'
                                        }}
                                    >
                                        Tuần
                                    </Select.Option>
                                    <Select.Option
                                        className='timeSelect'
                                        key={3}
                                        Title="Tháng"
                                        style={{
                                            height: '44px',
                                            alignItems: 'center',
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-Regular'
                                        }}
                                    >
                                        Tháng
                                    </Select.Option>
                                </Select>
                            </div>
                        </div>
                        <div
                            style={{
                                height: '300px',
                                marginTop: '20px'
                            }}>
                            <Area {...config} />
                        </div>
                    </div>
                </div>
            </div>
            <div
                style={{
                    width: '401px',
                    height: '720px',
                    position: 'absolute',
                    top: 0,
                    left: '73.9%',
                    backgroundColor: '#FFFFFF',
                    padding: '16px'
                }}>
                <div
                    style={{
                        height: '74px',
                    }} />
                <Typography
                    style={{
                        fontSize: '24px',
                        fontFamily: 'Nunito-Bold',
                        color: '#FF7506',
                        // marginBottom: '16px'
                    }}>
                    Tổng quan
                </Typography>
                <div
                    style={{
                        display: 'flex',
                        width: '353px',
                        height: '83px',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        padding: '16px',
                        boxShadow: '2px 2px 15px rgba(70, 64, 67, 0.1)',
                        borderRadius: '12px',
                        // marginBottom: '10px'
                    }}>
                    <div
                        style={{
                            display: 'flex'
                        }}>
                        <div
                            style={{
                                position: 'relative',
                                width: '60px',
                                height: '60px'
                            }}>
                            <Progress
                                percent={90}
                                type="circle"
                                size={60}
                                strokeColor={{ '0%': '#FF7506' }}
                                format={(percent) =>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-Bold',
                                            fontSize: '14px',
                                            lineHeight: 0
                                        }}>
                                        {percent}%
                                    </Typography>} />
                            <Progress
                                size={50}
                                percent={10}
                                strokeWidth={6}
                                strokeColor="#383b36"
                                type="circle"
                                format={() => ``}
                                style={{ position: 'absolute', top: '9%', left: '8%' }}
                            />
                        </div>
                        <div
                            style={{
                                paddingLeft: '14px'
                            }}>
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-ExtraBold',
                                    fontSize: '24px'
                                }}>
                                4.221
                            </Typography>
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-SemiBold',
                                    fontSize: '14px',
                                    color: '#FF7506'
                                }}>
                                <DesktopOutlined /> Thiết bị
                            </Typography>
                        </div>
                    </div>
                    <div>
                        <div
                            style={{
                                display: 'flex',
                                alignItems: 'center',
                                width: '164px',
                                height: '22px',
                                justifyContent: 'space-between'
                            }}>
                            <li
                                className='marker-yellow'
                                style={{
                                    fontFamily: 'Nunito-Regular',
                                    fontSize: '12px',
                                    color: '#7E7D88',
                                    paddingRight: '10px',
                                }}>
                                Đang hoạt động
                            </li>
                            <div
                                style={{
                                    width: '40px'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '14px',
                                        fontFamily: 'Nunito-Bold',
                                        color: '#FF7506'
                                    }}>
                                    3.799
                                </Typography>
                            </div>
                        </div>
                        <div
                            style={{
                                display: 'flex',
                                alignItems: 'center',
                                width: '164px',
                                height: '22px',
                                justifyContent: 'space-between'
                            }}>
                            <li
                                className='marker-gray'
                                style={{
                                    fontFamily: 'Nunito-Regular',
                                    fontSize: '12px',
                                    color: '#7E7D88',
                                    paddingRight: '10px'
                                }}>
                                Ngưng hoạt động
                            </li>
                            <div
                                style={{
                                    width: '40px'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '14px',
                                        fontFamily: 'Nunito-Bold',
                                        color: '#FF7506'
                                    }}>
                                    422
                                </Typography>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    style={{
                        display: 'flex',
                        width: '353px',
                        height: '83px',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        padding: '16px',
                        boxShadow: '2px 2px 15px rgba(70, 64, 67, 0.1)',
                        borderRadius: '12px',
                        // marginBottom: '10px'
                    }}>
                    <div
                        style={{
                            display: 'flex'
                        }}>
                        <div
                            style={{
                                position: 'relative',
                                width: '60px',
                                height: '60px'
                            }}>
                            <Progress
                                percent={76}
                                type="circle"
                                size={60}
                                strokeColor={{ '0%': '#4277FF' }}
                                format={(percent) =>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-Bold',
                                            fontSize: '14px',
                                            lineHeight: 0
                                        }}>
                                        {percent}%
                                    </Typography>} />
                            <Progress
                                size={50}
                                percent={24}
                                strokeWidth={6}
                                strokeColor="#383b36"
                                type="circle"
                                format={() => ``}
                                style={{ position: 'absolute', top: '9%', left: '8%' }}
                            />
                        </div>
                        <div
                            style={{
                                paddingLeft: '14px'
                            }}>
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-ExtraBold',
                                    fontSize: '24px'
                                }}>
                                276
                            </Typography>
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-SemiBold',
                                    fontSize: '14px',
                                    color: '#4277FF'
                                }}>
                                <MessageOutlined /> Dịch vụ
                            </Typography>
                        </div>
                    </div>
                    <div>
                        <div
                            style={{
                                display: 'flex',
                                alignItems: 'center',
                                width: '164px',
                                height: '22px',
                                justifyContent: 'space-between'
                            }}>
                            <li
                                className='marker-blue'
                                style={{
                                    fontFamily: 'Nunito-Regular',
                                    fontSize: '12px',
                                    color: '#7E7D88',
                                    paddingRight: '10px',
                                }}>
                                Đang hoạt động
                            </li>
                            <div
                                style={{
                                    width: '40px'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '14px',
                                        fontFamily: 'Nunito-Bold',
                                        color: '#4277FF'
                                    }}>
                                    210
                                </Typography>
                            </div>
                        </div>
                        <div
                            style={{
                                display: 'flex',
                                alignItems: 'center',
                                width: '164px',
                                height: '22px',
                                justifyContent: 'space-between'
                            }}>
                            <li
                                className='marker-gray'
                                style={{
                                    fontFamily: 'Nunito-Regular',
                                    fontSize: '12px',
                                    color: '#7E7D88',
                                    paddingRight: '10px'
                                }}>
                                Ngưng hoạt động
                            </li>
                            <div
                                style={{
                                    width: '40px'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '14px',
                                        fontFamily: 'Nunito-Bold',
                                        color: '#4277FF'
                                    }}>
                                    66
                                </Typography>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    style={{
                        display: 'flex',
                        width: '353px',
                        height: '83px',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        padding: '16px',
                        boxShadow: '2px 2px 15px rgba(70, 64, 67, 0.1)',
                        borderRadius: '12px',
                        // marginBottom: '10px'
                    }}>
                    <div
                        style={{
                            display: 'flex'
                        }}>
                        <div
                            style={{
                                position: 'relative',
                                width: '60px',
                                height: '60px'
                            }}>
                            <Progress
                                percent={86}
                                type="circle"
                                size={60}
                                strokeColor={{ '0%': '#35C75A' }}
                                format={(percent) =>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-Bold',
                                            fontSize: '14px',
                                            lineHeight: 0
                                        }}>
                                        {percent}%
                                    </Typography>} />
                            <Progress
                                size={50}
                                percent={10}
                                strokeWidth={6}
                                strokeColor="#383b36"
                                type="circle"
                                format={() => ``}
                                style={{ position: 'absolute', top: '9%', left: '8%' }}
                            />
                            <Progress
                                size={40}
                                percent={4}
                                strokeWidth={6}
                                strokeColor="#F178B6"
                                type="circle"
                                format={() => ``}
                                style={{ position: 'absolute', top: '17.5%', left: '16%' }}
                            />
                        </div>
                        <div
                            style={{
                                paddingLeft: '14px'
                            }}>
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-ExtraBold',
                                    fontSize: '24px'
                                }}>
                                4.221
                            </Typography>
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-SemiBold',
                                    fontSize: '14px',
                                    color: '#35C75A'
                                }}>
                                <CopyOutlined /> Cấp số
                            </Typography>
                        </div>
                    </div>
                    <div>
                        <div
                            style={{
                                display: 'flex',
                                alignItems: 'center',
                                width: '164px',
                                height: '22px',
                                justifyContent: 'space-between'
                            }}>
                            <li
                                className='marker-green'
                                style={{
                                    fontFamily: 'Nunito-Regular',
                                    fontSize: '12px',
                                    color: '#7E7D88',
                                    paddingRight: '10px',
                                }}>
                                Đang chờ
                            </li>
                            <div
                                style={{
                                    width: '40px'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '14px',
                                        fontFamily: 'Nunito-Bold',
                                        color: '#35C75A'
                                    }}>
                                    3.721
                                </Typography>
                            </div>
                        </div>
                        <div
                            style={{
                                display: 'flex',
                                alignItems: 'center',
                                width: '164px',
                                height: '22px',
                                justifyContent: 'space-between'
                            }}>
                            <li
                                className='marker-gray'
                                style={{
                                    fontFamily: 'Nunito-Regular',
                                    fontSize: '12px',
                                    color: '#7E7D88',
                                    paddingRight: '10px'
                                }}>
                                Đã sử dụng
                            </li>
                            <div
                                style={{
                                    width: '40px'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '14px',
                                        fontFamily: 'Nunito-Bold',
                                        color: '#35C75A'
                                    }}>
                                    486
                                </Typography>
                            </div>
                        </div>
                        <div
                            style={{
                                display: 'flex',
                                alignItems: 'center',
                                width: '164px',
                                height: '22px',
                                justifyContent: 'space-between'
                            }}>
                            <li
                                className='marker-pink'
                                style={{
                                    fontFamily: 'Nunito-Regular',
                                    fontSize: '12px',
                                    color: '#7E7D88',
                                    paddingRight: '10px'
                                }}>
                                Bỏ qua
                            </li>
                            <div
                                style={{
                                    width: '40px'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '14px',
                                        fontFamily: 'Nunito-Bold',
                                        color: '#35C75A'
                                    }}>
                                    32
                                </Typography>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <Calendar
                        fullscreen={false}
                        style={{
                            filter: 'drop-shadow(2px 2px 15px rgba(70, 64, 67, 0.1))',
                            borderRadius: '12px',
                            width: '353px',
                            height: '310px',
                        }} />
                </div>
            </div>
        </div>

    )
}

export default DashBoardContent