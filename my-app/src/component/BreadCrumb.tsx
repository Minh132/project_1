import { Breadcrumb } from 'antd';
import {
  Link,
  useLocation
} from 'react-router-dom';
import Typography from 'antd/es/typography/Typography';
import React, {
  useState,
  useEffect,
  useContext
} from 'react';
import { IdContext } from '../IdContext';
const AppBreadcrumb = () => {
  const [idProgression, setIdProgression] = useState('');
  const [idDevice, setIdDevice] = useState('');
  const [idService, setIdService] = useState('');
  const [idAccountSetting, setIdAccountSetting] = useState('');
  const location = useLocation();
  const path = location.pathname;
  const searchTerm = "id=";
  const pathSnippets = location.pathname.split('/').filter(i => i);
  const { id } = useContext(IdContext);
  useEffect(() => {
    const startIndex = path.indexOf(searchTerm);
    if (startIndex !== -1) {
      const desiredValue = path.substring(startIndex + searchTerm.length);
      setIdAccountSetting(desiredValue);
      setIdService(desiredValue);
      setIdDevice(desiredValue);
      setIdProgression(desiredValue);
    }
    console.log(idAccountSetting);
  })
  const breadcrumbItems = [
    <>
      {location.pathname === `/account-info/id=${id}` ?
        <Breadcrumb.Item key="account-info">
          <Link to="/account-info">
            <Typography
              style={{
                fontSize: '20px',
                fontFamily: 'Nunito-Bold',
                lineHeight: '24px',
                color: '#FF7506'
              }}>
              Thông tin cá nhân
            </Typography>
          </Link>
        </Breadcrumb.Item>
        : ''}
      {location.pathname === '/dashboard' ?
        <Breadcrumb.Item key="dashboard">
          <Link to="/dashboard">
            <Typography
              style={{
                fontSize: '20px',
                fontFamily: 'Nunito-Bold',
                lineHeight: '24px',
                color: '#FF7506'
              }}>
              Dashboard
            </Typography>
          </Link>
        </Breadcrumb.Item>
        : ''}
      {location.pathname === '/device/list-device' ?
        <>
          <Breadcrumb.Item key="device">
            <Typography
              style={{
                fontSize: '20px',
                fontFamily: 'Nunito-Bold',
                lineHeight: '24px',
                color: '#7E7D88'
              }}>
              Thiết bị
            </Typography>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/device/list-device">
            <Link to="/device/list-device">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Danh sách thiết bị
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === '/device/list-device/add-device' ?
        <>
          <Breadcrumb.Item key="device">
            <Typography
              style={{
                fontSize: '20px',
                fontFamily: 'Nunito-Bold',
                lineHeight: '24px',
                color: '#7E7D88'
              }}>
              Thiết bị
            </Typography>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/device/list-device">
            <Link to="/device/list-device">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Danh sách thiết bị
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/device/list-device/add-device">
            <Link to="/device/list-device/add-device">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Thêm thiết bị
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === `/device/list-device/detail-device/id=${idDevice}` ?
        <>
          <Breadcrumb.Item key="device">
            <Typography
              style={{
                fontSize: '20px',
                fontFamily: 'Nunito-Bold',
                lineHeight: '24px',
                color: '#7E7D88'
              }}>
              Thiết bị
            </Typography>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/device/list-device">
            <Link to="/device/list-device">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Danh sách thiết bị
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/device/list-device/detail-device">
            <Link to="/device/list-device/detail-device">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Chi tiết thiết bị
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === `/device/list-device/update-device/id=${idDevice}` ?
        <>
          <Breadcrumb.Item key="device">
            <Typography
              style={{
                fontSize: '20px',
                fontFamily: 'Nunito-Bold',
                lineHeight: '24px',
                color: '#7E7D88'
              }}>
              Thiết bị
            </Typography>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/device/list-device">
            <Link to="/device/list-device">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Danh sách thiết bị
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/device/list-device/update-device">
            <Link to="/device/list-device/update-device">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Cập nhật thiết bị
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === '/service/list-service' ?
        <>
          <Breadcrumb.Item key="service">
            <Link to="/service">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Dịch vụ
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/service/list-service">
            <Link to="/service/list-service">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Danh sách dịch vụ
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === '/service/list-service/add-service' ?
        <>
          <Breadcrumb.Item key="service">
            <Link to="/service">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Dịch vụ
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/service/list-service">
            <Link to="/service/list-service">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Danh sách dịch vụ
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/service/list-service/add-service">
            <Link to="/service/list-service/add-service">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Thêm dịch vụ
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === `/service/list-service/detail-service/id=${idService}` ?
        <>
          <Breadcrumb.Item key="service">
            <Link to="/service">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Dịch vụ
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/service/list-service">
            <Link to="/service/list-service">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Danh sách dịch vụ
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/service/list-service/detail-service">
            <Link to="/service/list-service/detail-service">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Chi tiết
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === `/service/list-service/update-service/id=${idService}` ?
        <>
          <Breadcrumb.Item key="service">
            <Link to="/service">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Dịch vụ
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/service/list-service">
            <Link to="/service/list-service">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Danh sách dịch vụ
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/service/list-service/update-service">
            <Link to="/service/list-service/update-service">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Cập nhật
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === '/progression/list-progression' ?
        <>
          <Breadcrumb.Item key="progression">
            <Link to="/progression">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Cấp số
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/progression/list-progression">
            <Link to="/progression/list-progression">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Danh sách cấp số
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === '/progression/list-progression/new-progression' ?
        <>
          <Breadcrumb.Item key="progression">
            <Link to="/progression">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Cấp số
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/progression/list-progression">
            <Link to="/progression/list-progression">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Danh sách cấp số
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/progression/list-progression/new-progression">
            <Link to="/progression/list-progression/new-progression">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Cấp số mới
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === `/progression/list-progression/detail-progression/id=${idProgression}` ?
        <>
          <Breadcrumb.Item key="progression">
            <Link to="/progression">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Cấp số
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/progression/list-progression">
            <Link to="/progression/list-progression">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Danh sách cấp số
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/progression/list-progression/detail-progression">
            <Link to="/progression/list-progression/detail-progression">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Chi tiết
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === '/report/list-report' ?
        <>
          <Breadcrumb.Item key="report">
            <Link to="/report">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Báo cáo
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/report/list-report">
            <Link to="/report/list-report">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Lập báo cáo
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === '/setting/account-setting' ?
        <>
          <Breadcrumb.Item key="setting">
            <Link to="/setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Cài đặt hệ thống
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/setting/account-setting">
            <Link to="/setting/account-setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Quản lý tài khoản
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === '/setting/account-setting/add-account-setting' ?
        <>
          <Breadcrumb.Item key="setting">
            <Link to="/setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Cài đặt hệ thống
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/setting/account-setting">
            <Link to="/setting/account-setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Quản lý tài khoản
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/setting/account-setting/add-account-setting">
            <Link to="/setting/account-setting/add-account-setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Thêm tài khoản
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === `/setting/account-setting/update-account-setting/id=${idAccountSetting}` ?
        <>
          <Breadcrumb.Item key="setting">
            <Link to="/setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Cài đặt hệ thống
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/setting/account-setting">
            <Link to="/setting/account-setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Quản lý tài khoản
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/setting/account-setting/update-account-setting">
            <Link to="/setting/account-setting/update-account-setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Cập nhật tài khoản
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === '/setting/role-setting' ?
        <>
          <Breadcrumb.Item key="setting">
            <Link to="/setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Cài đặt hệ thống
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/setting/role-setting">
            <Link to="/setting/role-setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Quản lý vai trò
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === '/setting/role-setting/add-role-setting' ?
        <>
          <Breadcrumb.Item key="setting">
            <Link to="/setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Cài đặt hệ thống
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/setting/role-setting">
            <Link to="/setting/role-setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Quản lý vai trò
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/setting/role-setting/add-role-setting">
            <Link to="/setting/role-setting/add-role-setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Thêm vai trò
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === '/setting/role-setting/update-role-setting' ?
        <>
          <Breadcrumb.Item key="setting">
            <Link to="/setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Cài đặt hệ thống
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/setting/role-setting">
            <Link to="/setting/role-setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Quản lý vai trò
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/setting/role-setting/update-role-setting">
            <Link to="/setting/role-setting/update-role-setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Cập nhật vai trò
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
      {location.pathname === '/setting/user-setting' ?
        <>
          <Breadcrumb.Item key="setting">
            <Link to="/setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#7E7D88'
                }}>
                Cài đặt hệ thống
              </Typography>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item key="/setting/user-setting">
            <Link to="/setting/user-setting">
              <Typography
                style={{
                  fontSize: '20px',
                  fontFamily: 'Nunito-Bold',
                  lineHeight: '24px',
                  color: '#FF7506'
                }}>
                Nhật ký hoạt động
              </Typography>
            </Link>
          </Breadcrumb.Item>
        </>
        : ''}
    </>
  ]
  // ].concat(extraBreadcrumbItems);

  return (
    <Breadcrumb separator=">" style={{ margin: '16px 0' }}>
      {breadcrumbItems}
    </Breadcrumb>
  );
};

export default AppBreadcrumb;