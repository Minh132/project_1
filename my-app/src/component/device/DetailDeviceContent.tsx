import { Typography } from 'antd'
import React, {
    useState,
    useEffect
} from 'react';
import {
    EditOutlined
} from '@ant-design/icons';
import {
    Link,
    useLocation
} from 'react-router-dom';
import {
    DocumentData,
    onSnapshot,
    QuerySnapshot
} from "firebase/firestore";
import { Device } from '../../types/Device';
import { devicesCollection } from '../../features/Device/deviceSlice';
function DetailDeviceContent() {
    const [devices, setDevices] = useState<Device[]>([]);
    const [id, setId] = useState('');
    const location = useLocation();
    const path = location.pathname;
    const searchTerm = "id=";
    useEffect(() => {
        const startIndex = path.indexOf(searchTerm);
        if (startIndex !== -1) {
            const desiredValue = path.substring(startIndex + searchTerm.length);
            setId(desiredValue);
        }
        console.log(id);
    })
    useEffect(() => {
        const unsubscribe = onSnapshot(devicesCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const deviceData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setDevices(deviceData);
        });
        return () => {
            unsubscribe();
        };
    }, []);
    return (
        <div
            style={{
                margin: '0 20px'
            }}
        >
            <Typography
                style={{
                    color: '#FF7506',
                    fontFamily: 'Nunito-Bold',
                    fontSize: '24px'
                }}
            >
                Quản lý thiết bị
            </Typography>

            <div
                style={{
                    width: '1112px',
                    height: '604px',
                    backgroundColor: '#FFFFFF',
                    boxShadow: '2px 2px 8px rgba(232, 239, 244, 0.8)',
                    borderRadius: '16px',
                    padding: '20px',
                    margin: '7px 0'
                }}>
                {devices.map((device) =>
                    device.id === id ?
                        <>
                            <Typography
                                style={{
                                    color: '#FF9138',
                                    fontFamily: 'Nunito-Bold',
                                    fontSize: '20px'
                                }}>
                                Thông tin thiết bị
                            </Typography>
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    marginTop: '6px',
                                    marginBottom: '6px',
                                }}>
                                <div>
                                    <div
                                        style={{
                                            display: 'flex',
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-SemiBold',
                                                paddingBottom: '6px',
                                                width: '160px'
                                            }}>
                                            Mã thiết bị:
                                        </Typography>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-Regular',
                                                paddingBottom: '6px',
                                                width: '360px',

                                                color: '#535261'
                                            }}>
                                            {device.idDevice}
                                        </Typography>
                                    </div>
                                </div>

                                <div>
                                    <div
                                        style={{
                                            display: 'flex',
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-SemiBold',
                                                paddingBottom: '6px',
                                                width: '160px'
                                            }}>
                                            Loại thiết bị:
                                        </Typography>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-Regular',
                                                paddingBottom: '6px',
                                                width: '360px',

                                                color: '#535261'
                                            }}>
                                            {device.type}
                                        </Typography>
                                    </div>
                                </div>
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    marginTop: '6px',
                                    marginBottom: '6px',
                                }}>
                                <div>
                                    <div
                                        style={{
                                            display: 'flex',
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-SemiBold',
                                                paddingBottom: '6px',
                                                width: '160px'
                                            }}>
                                            Tên thiết bị:
                                        </Typography>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-Regular',
                                                paddingBottom: '6px',
                                                width: '360px',

                                                color: '#535261'
                                            }}>
                                            {device.name}
                                        </Typography>
                                    </div>
                                </div>

                                <div>
                                    <div
                                        style={{
                                            display: 'flex',
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-SemiBold',
                                                paddingBottom: '6px',
                                                width: '160px'
                                            }}>
                                            Tên đăng nhập:
                                        </Typography>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-Regular',
                                                paddingBottom: '6px',
                                                width: '360px',

                                                color: '#535261'
                                            }}>
                                            {device.nameAccount}
                                        </Typography>
                                    </div>
                                </div>
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    marginTop: '6px',
                                    marginBottom: '6px',
                                }}>
                                <div>
                                    <div
                                        style={{
                                            display: 'flex',
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-SemiBold',
                                                paddingBottom: '6px',
                                                width: '160px'
                                            }}>
                                            Địa chỉ IP:
                                        </Typography>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-Regular',
                                                paddingBottom: '6px',
                                                width: '360px',

                                                color: '#535261'
                                            }}>
                                            {device.ip}
                                        </Typography>
                                    </div>
                                </div>

                                <div>
                                    <div
                                        style={{
                                            display: 'flex',
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-SemiBold',
                                                paddingBottom: '6px',
                                                width: '160px'
                                            }}>
                                            Mật khẩu:
                                        </Typography>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-Regular',
                                                paddingBottom: '6px',
                                                width: '360px',

                                                color: '#535261'
                                            }}>
                                            {device.password}
                                        </Typography>
                                    </div>
                                </div>
                            </div>
                            <div
                                style={{
                                    marginTop: '6px',
                                    marginBottom: '6px',
                                }}>
                                <div>
                                    <Typography
                                        style={{
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-SemiBold',
                                            paddingBottom: '6px',
                                            width: '160px'
                                        }}>
                                        Dịch vụ sử dụng:
                                    </Typography>
                                    <Typography
                                        style={{
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-Regular',
                                            paddingBottom: '6px',
                                            width: '100%',
                                            color: '#535261'
                                        }}>
                                        {device.service}
                                    </Typography>
                                </div>
                            </div>
                        </>
                        : '')}
            </div>


            <Link
                to={`/device/list-device/update-device/id=${id}`}
                style={{
                    display: 'flex',
                    position: 'absolute',
                    top: '15%',
                    left: '94%',
                    backgroundColor: '#FFF2E7',
                    flexDirection: 'column',
                    padding: '12px',
                    width: '92px',
                    borderRadius: '8px',
                    boxShadow: '0px 0px 6px #E7E9F2',
                    cursor: 'pointer'
                }}>
                <EditOutlined
                    style={{
                        color: '#ffffff',
                        width: '28px',
                        height: '28px',
                        backgroundColor: '#FF9138',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: '8px',
                        marginBottom: '4px',
                        alignSelf: 'center',
                    }} />
                <Typography
                    style={{
                        fontFamily: 'Nunito-SemiBold',
                        fontSize: '14px',
                        color: '#FF9138',
                        textAlign: 'center',
                    }}>
                    Cập nhật thiết bị
                </Typography>
            </Link>
        </div>
    )
}

export default DetailDeviceContent