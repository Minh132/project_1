import {
    Input,
    Select,
    Table,
    Typography
} from 'antd'
import {
    CaretDownOutlined,
    SearchOutlined,
    PlusOutlined
} from '@ant-design/icons';
import { Link } from 'react-router-dom';
import React, {
    useState,
    useEffect
} from 'react'
import {
    DocumentData,
    onSnapshot,
    QuerySnapshot
} from "firebase/firestore";
import { Device } from '../../types/Device';
import { devicesCollection } from '../../features/Device/deviceSlice';
function ListDeviceContent() {
    const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchKeyword(event.target.value);
    }; const [searchKeyword, setSearchKeyword] = useState<string>('');
    const [filteredDevices, setFilteredDevices] = useState<Device[]>([]);
    const [selectedActive, setSelectedActive] = useState<string>('Tất cả');
    const [selectedConnect, setSelectedConnect] = useState<string>('Tất cả');
    const handleActive = (value: string) => {
        setSelectedActive(value);
        if (value === 'Tất cả') {
            setFilteredDevices(devices); // Hiển thị tất cả dữ liệu
        } else {
            const filteredData = devices.filter(device => device.active === value);
            setFilteredDevices(filteredData);
        }
    };
    const handleConnect = (value: string) => {
        setSelectedConnect(value);
        if (value === 'Tất cả') {
            setFilteredDevices(devices); // Hiển thị tất cả dữ liệu
        } else {
            const filteredData = devices.filter(device => device.connect === value);
            setFilteredDevices(filteredData);
        }
    };
    const [devices, setDevices] = useState<Device[]>([]);
    useEffect(() => {
        const unsubscribe = onSnapshot(devicesCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const deviceData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setDevices(deviceData);
        });

        return () => {
            unsubscribe();
        };
    }, []);
    const [showMore, setShowMore] = useState(false)
    const handleShowMore = () => {
        setShowMore(true)
    }
    const columns = [
        {
            title: 'Mã thiết bị',
            dataIndex: 'idDevice',
            key: 'idDevice',
            render: (idDevice: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {idDevice}
                    </Typography>
                );
            },
        },
        {
            title: 'Tên thiết bị',
            dataIndex: 'name',
            key: 'name',
            render: (name: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {name}
                    </Typography>
                );
            },
        },
        {
            title: 'Địa chỉ IP',
            dataIndex: 'ip',
            key: 'ip',
            render: (ip: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {ip}
                    </Typography>
                );
            },
        },
        {
            title: 'Trạng thái hoạt động',
            dataIndex: 'active',
            key: 'active',
            render: (active: string) => {
                return (
                    <>
                        {active === 'Hoạt động'
                            ?
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'center'
                                }}>
                                <div
                                    style={{
                                        width: '6px',
                                        height: '6px',
                                        backgroundColor: '#34CD26',
                                        borderRadius: '100%',
                                        marginRight: '2px'
                                    }}></div>
                                <Typography
                                    style={{
                                        fontFamily: 'Nunito-Regular',
                                        fontSize: '14px'
                                    }}>
                                    Hoạt động
                                </Typography>
                            </div>
                            : ''}
                        {active === 'Ngưng hoạt động' ?
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'center'
                                }}>
                                <div
                                    style={{
                                        width: '6px',
                                        height: '6px',
                                        backgroundColor: '#EC3740',
                                        borderRadius: '100%',
                                        marginRight: '2px'
                                    }}></div>
                                <Typography
                                    style={{
                                        fontFamily: 'Nunito-Regular',
                                        fontSize: '14px'
                                    }}>
                                    Ngưng hoạt động
                                </Typography>
                            </div> : ''}
                    </>
                );
            },
        },
        {
            title: 'Trạng thái kết nối',
            dataIndex: 'connect',
            key: 'connect',
            render: (connect: string) => {
                return (
                    <>
                        {connect === "Kết nối"
                            ?
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'center'
                                }}>
                                <div
                                    style={{
                                        width: '6px',
                                        height: '6px',
                                        backgroundColor: '#34CD26',
                                        borderRadius: '100%',
                                        marginRight: '2px'
                                    }}></div>
                                <Typography
                                    style={{
                                        fontFamily: 'Nunito-Regular',
                                        fontSize: '14px'
                                    }}>
                                    Kết nối
                                </Typography>
                            </div>
                            : ""}
                        {connect === "Mất kết nối" ?
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'center'
                                }}>
                                <div
                                    style={{
                                        width: '6px',
                                        height: '6px',
                                        backgroundColor: '#EC3740',
                                        borderRadius: '100%',
                                        marginRight: '2px'
                                    }}></div>
                                <Typography
                                    style={{
                                        fontFamily: 'Nunito-Regular',
                                        fontSize: '14px'
                                    }}>
                                    Mất kết nối
                                </Typography>
                            </div>
                            : ''}
                    </>
                );

            },
        },
        {
            title: 'Dịch vụ sử dụng',
            dataIndex: 'service',
            key: 'service',
            render: (service: string) => {
                return (
                    <>
                        {showMore
                            ?
                            <div
                                style={{
                                    width: '268px',
                                }}>
                                <Typography
                                    style={{
                                        fontFamily: 'Nunito-Regular',
                                        fontSize: '14px'
                                    }}>
                                    {service}
                                </Typography>
                            </div>
                            :
                            <div
                                style={{
                                    width: '268px',
                                    overflow: 'hidden',
                                    height: '21px',
                                    WebkitLineClamp: 1,
                                    textOverflow: 'ellipsis',
                                    WebkitBoxOrient: 'vertical',
                                }}>
                                <Typography
                                    style={{
                                        fontFamily: 'Nunito-Regular',
                                        fontSize: '14px'
                                    }}>
                                    {service}
                                </Typography>
                            </div>}

                        <Typography.Link
                            key={service}
                            onClick={handleShowMore}
                            style={{
                                textDecoration: 'underline',
                            }}>
                            Xem thêm
                        </Typography.Link>
                    </>
                );
            },
        },
        {
            title: ' ',
            dataIndex: 'detail',
            key: 'detail',
            render: (_: any, record: Device) => {
                return (
                    <Link
                        to={`/device/list-device/detail-device/id=${record.id}`}>
                        <Typography.Link
                            style={{
                                textDecoration: 'underline',
                                fontFamily: 'Nunito-Regular',
                                fontSize: '14px'
                            }}>
                            Chi tiết
                        </Typography.Link>
                    </Link>
                );
            },
        },
        {
            title: ' ',
            dataIndex: 'update',
            key: 'update',
            render: (_: any, record: Device) => {
                return (
                    <Link
                        to={`/device/list-device/update-device/id=${record.id}`}>
                        <Typography.Link
                            style={{
                                textDecoration: 'underline',
                                fontFamily: 'Nunito-Regular',
                                fontSize: '14px'
                            }}>
                            Cập nhật
                        </Typography.Link>
                    </Link>
                );
            },
        },
    ];
    return (
        <div
            style={{
                margin: '0 20px',
                width: '1112px'
            }}>
            <Typography
                style={{
                    fontSize: '24px',
                    fontFamily: 'Nunito-Bold',
                    color: '#FF7506',
                }}>
                Danh sách thiết bị
            </Typography>
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                }}>
                <div
                    style={{
                        display: 'flex'
                    }}>
                    <div
                        style={{
                            marginRight: '20px'
                        }}>
                        <Typography
                            style={{
                                fontSize: '16px',
                                fontFamily: 'Nunito-SemiBold',
                            }}>
                            Trạng thái hoạt động
                        </Typography>
                        <Select
                            defaultValue="Tất cả"
                            onChange={handleActive}
                            listHeight={180}
                            suffixIcon={<CaretDownOutlined
                                style={{
                                    color: '#FF7506'
                                }}
                            />}
                            style={{
                                width: '300px',
                                height: '44px',
                                borderRadius: '8px',
                                display: 'flex',
                                justifyContent: 'space-between'
                            }}
                        >
                            <Select.Option
                                className='timeSelect'
                                key={1}
                                value="Tất cả"
                                Title="Tất cả"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Tất cả
                            </Select.Option>
                            <Select.Option
                                className='timeSelect'
                                key={2}
                                value="Hoạt động"
                                Title="Hoạt động"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Hoạt động
                            </Select.Option>
                            <Select.Option
                                className='timeSelect'
                                key={3}
                                value="Ngưng hoạt động"
                                Title="Ngưng hoạt động"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Ngưng hoạt động
                            </Select.Option>
                        </Select>
                    </div>
                    <div>
                        <Typography
                            style={{
                                fontSize: '16px',
                                fontFamily: 'Nunito-SemiBold',
                            }}>
                            Trạng thái kết nối
                        </Typography>
                        <Select
                            defaultValue="Tất cả"
                            onChange={handleConnect}
                            listHeight={180}
                            suffixIcon={<CaretDownOutlined
                                style={{
                                    color: '#FF7506'
                                }}
                            />}
                            style={{
                                width: '300px',
                                height: '44px',
                                borderRadius: '8px',
                                display: 'flex',
                                justifyContent: 'space-between'
                            }}
                        >
                            <Select.Option
                                className='timeSelect'
                                value="Tất cả"
                                key={1}
                                Title="Tất cả"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Tất cả
                            </Select.Option>
                            <Select.Option
                                className='timeSelect'
                                key={2}
                                value="Kết nối"
                                Title="Kết nối"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Kết nối
                            </Select.Option>
                            <Select.Option
                                className='timeSelect'
                                key={3}
                                value="Mất kết nối"
                                Title="Mất kết nối"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Mất kết nối
                            </Select.Option>
                        </Select>
                    </div>
                </div>
                <div>
                    <Typography
                        style={{
                            fontSize: '16px',
                            fontFamily: 'Nunito-SemiBold',
                        }}>
                        Từ khoá
                    </Typography>
                    <Input
                        onChange={handleSearch}
                        placeholder="Nhập từ khóa"
                        style={{
                            width: '300px',
                            height: '42px'
                        }}
                        suffix={
                            <SearchOutlined
                                style={{
                                    color: '#FF7506',
                                    fontSize: '16px'
                                }} />
                        }
                    />
                </div>
            </div>
            <div
                style={{
                    height: '490px'
                }}
            >
                <Table
                    pagination={{ pageSize: 9 }}
                    dataSource={
                        selectedActive === 'Tất cả' && selectedConnect === 'Tất cả'
                            ? devices && devices.filter(
                                device =>
                                    device && device.name && device.name && device.name.toLowerCase().includes(searchKeyword.toLowerCase())
                            ) : filteredDevices.filter(
                                device =>
                                    device &&
                                    (device.active === selectedActive ||
                                        device.connect === selectedConnect
                                    ))

                    }

                    columns={columns}
                    rowClassName={(record, index) =>
                        index % 2 === 0 ? "bg-white" : "bg-orange-50"
                    }
                    style={{
                        height: '490px',
                        marginTop: '4px'
                    }}
                />
            </div>
            <Link
                to={'/device/list-device/add-device'}
                style={{
                    display: 'flex',
                    position: 'absolute',
                    top: '24.4%',
                    left: '94%',
                    backgroundColor: '#FFF2E7',
                    flexDirection: 'column',
                    padding: '12px',
                    width: '92px',
                    borderRadius: '8px',
                    boxShadow: '0px 0px 6px #E7E9F2',
                    cursor: 'pointer'
                }}>
                <PlusOutlined
                    style={{
                        color: '#ffffff',
                        width: '28px',
                        height: '28px',
                        backgroundColor: '#FF9138',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: '8px',
                        marginBottom: '4px',
                        alignSelf: 'center',
                    }} />
                <Typography
                    style={{
                        fontFamily: 'Nunito-SemiBold',
                        fontSize: '14px',
                        color: '#FF9138',
                        textAlign: 'center',
                    }}>
                    Thêm thiết bị
                </Typography>
            </Link>
        </div>
    )
}

export default ListDeviceContent