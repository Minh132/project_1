import {
    Button,
    Input,
    Select,
    Typography
} from 'antd'
import React, {
    useState,
    useEffect
} from 'react';
import {
    CaretDownOutlined,
} from '@ant-design/icons';
import {
    Link,
    useLocation
} from 'react-router-dom';
import { User } from '../../types/User';
import { Service } from '../../types/Service';
import {
    DocumentData,
    onSnapshot,
    QuerySnapshot
} from "firebase/firestore";
import { useDispatch } from 'react-redux';
import { updateDevice } from '../../features/Device/deviceSlice';
import { usersCollection } from '../../features/User/userSlice';
import { servicesCollection } from '../../features/Service/serviceSlice';
function UpdateDeviceContent() {
    const [idDevice, setIdDevice] = useState("");
    const [name, setName] = useState("");
    const [ip, setIp] = useState("");
    const [connect, setConnect] = useState("Kết nối");
    const [services, setServices] = useState<Service[]>([]);
    const [service, setService] = useState<string[]>([]);
    const [type, setType] = useState("Kiosk");
    const [nameAccount, setNameAccount] = useState("");
    const [password, setPassword] = useState("");
    const [users, setUsers] = useState<User[]>([]);
    const [active, setActive] = useState('Hoạt động');
    const dispatch = useDispatch();
    const handleSelectTypeChange = (value: string) => {
        setType(value);
    };
    const handleService = (value: any) => {
        if (value.length > 1) {
            setService(value.join(', '));
        }
        setService(value);
    };
    const handleUpdate = async () => {
        try {
            await dispatch(
                updateDevice({
                    id: id,
                    docData: {
                        idDevice: idDevice,
                        name: name,
                        ip: ip,
                        active: active,
                        connect: connect,
                        type: type,
                        nameAccount: nameAccount,
                        password: password,
                        service: service.join(", ")
                    }
                }) as any
            );
            console.log('successfully added a new user');
        } catch (error) {
            console.log('error:', error);
        }
    };
    useEffect(() => {
        const unsubscribe = onSnapshot(usersCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const userData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setUsers(userData);
        });

        return () => {
            unsubscribe();
        };
    }, []);
    useEffect(() => {

        const a = users.map(user => {
            if (user.email === nameAccount && user.active === 'Hoạt động') { setActive("Hoạt động"); setConnect('Kết nối'); }
            else
                setActive('Ngưng hoạt động')
            setConnect('Mất kết nối')
        })

    }, [handleService])
    const [id, setId] = useState('');
    const location = useLocation();
    const path = location.pathname;
    const searchTerm = "id=";
    useEffect(() => {
        const startIndex = path.indexOf(searchTerm);
        if (startIndex !== -1) {
            const desiredValue = path.substring(startIndex + searchTerm.length);
            setId(desiredValue);
        }
        console.log(id);
    })
    useEffect(() => {
        const unsubscribe = onSnapshot(servicesCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const serviceData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setServices(serviceData);
        });

        return () => {
            unsubscribe();
        };
    }, []);
    return (
        <div
            style={{
                margin: '0 20px'
            }}
        >
            <Typography
                style={{
                    color: '#FF7506',
                    fontFamily: 'Nunito-Bold',
                    fontSize: '24px'
                }}
            >
                Quản lý thiết bị
            </Typography>
            <div
                style={{
                    width: '1152px',
                    height: '550px',
                    backgroundColor: '#ffffff',
                    boxShadow: '2px 2px 8px rgba(232, 239, 244, 0.8)',
                    borderRadius: '16px',
                    padding: '20px',
                    margin: '7px 0'
                }}>
                <Typography
                    style={{
                        color: '#FF9138',
                        fontFamily: 'Nunito-Bold',
                        fontSize: '20px'
                    }}>
                    Thông tin thiết bị
                </Typography>
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        marginTop: '6px',
                        marginBottom: '6px',
                    }}>
                    <div>
                        <div
                            style={{
                                display: 'flex',
                            }}>
                            <Typography
                                style={{
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-SemiBold',
                                    paddingBottom: '6px'
                                }}>
                                Mã thiết bị:
                            </Typography>
                            <div
                                style={{
                                    width: '25px',
                                    height: '25px'
                                }}
                            >
                                <Typography
                                    style={{
                                        color: 'red',
                                        fontSize: '16px',
                                        alignItems: 'center',
                                        display: 'flex',
                                        height: '32px',
                                        marginLeft: '2px'
                                    }}>
                                    *
                                </Typography>
                            </div>
                        </div>
                        <Input
                            onChange={(e) => setIdDevice(e.target.value)}
                            placeholder="Nhập mã thiết bị"
                            style={{
                                width: '540px',
                                height: '44px',
                                fontSize: '16px',
                                fontFamily: 'Nunito-SemiBold',
                                color: '#535261'
                            }}
                        />
                    </div>

                    <div>
                        <div
                            style={{
                                display: 'flex',
                            }}>
                            <Typography
                                style={{
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-SemiBold',
                                    paddingBottom: '6px'
                                }}>
                                Loại thiết bị:
                            </Typography>
                            <div
                                style={{
                                    width: '25px',
                                    height: '25px'
                                }}
                            >
                                <Typography
                                    style={{
                                        color: 'red',
                                        fontSize: '16px',
                                        alignItems: 'center',
                                        display: 'flex',
                                        height: '32px',
                                        marginLeft: '2px'
                                    }}>
                                    *
                                </Typography>
                            </div>

                        </div>
                        <Select
                            value={type}
                            listHeight={180}
                            placeholder={'Chọn loại thiết bi'}
                            onChange={handleSelectTypeChange}
                            suffixIcon={<CaretDownOutlined
                                style={{
                                    color: '#FF7506'
                                }}
                            />}
                            style={{
                                width: '540px',
                                height: '44px',
                                borderRadius: '8px',
                                display: 'flex',
                                justifyContent: 'space-between'
                            }}
                        >
                            <Select.Option
                                value="Kiosk"
                                className='timeSelect'
                                key={1}
                                Title="Kiosk"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Kiosk
                            </Select.Option>
                            <Select.Option
                                value="Display counter"
                                className='timeSelect'
                                key={2}
                                Title="Display counter"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Display counter
                            </Select.Option>
                        </Select>
                    </div>
                </div>
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        marginTop: '6px',
                        marginBottom: '6px',
                    }}>
                    <div>
                        <div
                            style={{
                                display: 'flex',
                            }}>
                            <Typography
                                style={{
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-SemiBold',
                                    paddingBottom: '6px'
                                }}>
                                Tên thiết bị:
                            </Typography>
                            <div
                                style={{
                                    width: '25px',
                                    height: '25px'
                                }}
                            >
                                <Typography
                                    style={{
                                        color: 'red',
                                        fontSize: '16px',
                                        alignItems: 'center',
                                        display: 'flex',
                                        height: '32px',
                                        marginLeft: '2px'
                                    }}>
                                    *
                                </Typography>
                            </div>

                        </div>
                        <Input
                            onChange={(e) => setName(e.target.value)}
                            placeholder="Nhập tên thiết bị"
                            style={{
                                width: '540px',
                                height: '44px',
                                fontSize: '16px'
                            }}
                        />
                    </div>

                    <div>
                        <div
                            style={{
                                display: 'flex',
                            }}>
                            <Typography
                                style={{
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-SemiBold',
                                    paddingBottom: '6px'
                                }}>
                                Tên đăng nhập:
                            </Typography>
                            <div
                                style={{
                                    width: '25px',
                                    height: '25px'
                                }}
                            >
                                <Typography
                                    style={{
                                        color: 'red',
                                        fontSize: '16px',
                                        alignItems: 'center',
                                        display: 'flex',
                                        height: '32px',
                                        marginLeft: '2px'
                                    }}>
                                    *
                                </Typography>
                            </div>

                        </div>
                        <Input
                            onChange={(e) => setNameAccount(e.target.value)}
                            placeholder="Nhập tài khoản"
                            style={{
                                width: '540px',
                                height: '44px',
                                fontSize: '16px'
                            }}
                        />
                    </div>
                </div>
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        marginTop: '6px',
                        marginBottom: '6px',
                    }}>
                    <div>
                        <div
                            style={{
                                display: 'flex',
                            }}>
                            <Typography
                                style={{
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-SemiBold',
                                    paddingBottom: '6px'
                                }}>
                                Địa chỉ IP:
                            </Typography>
                            <div
                                style={{
                                    width: '25px',
                                    height: '25px'
                                }}
                            >
                                <Typography
                                    style={{
                                        color: 'red',
                                        fontSize: '16px',
                                        alignItems: 'center',
                                        display: 'flex',
                                        height: '32px',
                                        marginLeft: '2px'
                                    }}>
                                    *
                                </Typography>
                            </div>

                        </div>
                        <Input
                            onChange={(e) => setIp(e.target.value)}
                            placeholder="Nhập địa chỉ IP"
                            style={{
                                width: '540px',
                                height: '44px',
                                fontSize: '16px'
                            }}
                        />
                    </div>

                    <div>
                        <div
                            style={{
                                display: 'flex',
                            }}>
                            <Typography
                                style={{
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-SemiBold',
                                    paddingBottom: '6px'
                                }}>
                                Mật khẩu:
                            </Typography>
                            <div
                                style={{
                                    width: '25px',
                                    height: '25px'
                                }}
                            >
                                <Typography
                                    style={{
                                        color: 'red',
                                        fontSize: '16px',
                                        alignItems: 'center',
                                        display: 'flex',
                                        height: '32px',
                                        marginLeft: '2px'
                                    }}>
                                    *
                                </Typography>
                            </div>

                        </div>
                        <Input
                            onChange={(e) => setPassword(e.target.value)}
                            placeholder="Nhập mật khẩu"
                            style={{
                                width: '540px',
                                height: '44px',
                                fontSize: '16px'
                            }}
                        />
                    </div>
                </div>
                <div
                    style={{
                        marginTop: '6px',
                        marginBottom: '6px',
                    }}>
                    <div
                        style={{
                            display: 'flex',
                        }}>
                        <Typography
                            style={{
                                fontSize: '16px',
                                fontFamily: 'Nunito-SemiBold',
                                paddingBottom: '6px'
                            }}>
                            Dịch vụ sử dụng:
                        </Typography>
                        <div
                            style={{
                                width: '25px',
                                height: '25px'
                            }}
                        >
                            <Typography
                                style={{
                                    color: 'red',
                                    fontSize: '16px',
                                    alignItems: 'center',
                                    display: 'flex',
                                    height: '32px',
                                    marginLeft: '2px'
                                }}>
                                *
                            </Typography>
                        </div>

                    </div>
                    <Select
                        listHeight={130}
                        showSearch={false}
                        mode="multiple"
                        placeholder="Nhập dịch vụ sử dụng"
                        value={service}
                        onChange={handleService}
                        className='service-use'
                        style={{
                            width: '100%',
                            fontSize: '16px',
                            fontFamily: 'Nunito-Regular'
                        }}
                    >
                        {services &&
                            services.map((service) => (
                                <Select.Option
                                    value={service.name}
                                    className='timeSelect'
                                    Title={service.name}
                                    key={service.id}
                                    style={{
                                        height: '44px',
                                        alignItems: 'center',
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-Regular'
                                    }}
                                >
                                    {service.name}
                                </Select.Option>
                            ))}
                    </Select>
                </div>
                <div
                    style={{
                        display: 'flex',
                        paddingTop: '6px',
                        paddingBottom: '6px',
                    }}>
                    <Typography
                        style={{
                            color: 'red',
                            fontSize: '14px',
                            alignItems: 'center',
                            display: 'flex',
                            height: '28px',
                            marginRight: '2px'
                        }}>
                        *
                    </Typography>
                    <Typography
                        style={{
                            fontSize: '14px',
                            fontFamily: 'Nunito-Regular',
                            color: '#7E7D88'
                        }}>
                        Là trường thông tin bắt buộc
                    </Typography>
                </div>
            </div>
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                <div
                    style={{
                        width: '320px',
                        display: 'flex',
                        justifyContent: 'space-between'
                    }}
                >

                    <Link
                        to={'/device/list-device'}>
                        <Button
                            style={{
                                width: '147px',
                                height: '48px',
                                borderRadius: '8px',
                                backgroundColor: '#FFF2E7',
                                borderWidth: '1.5px',
                                borderColor: '#FF9138',

                            }}>
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-Bold',
                                    fontSize: '16px',
                                    color: '#FF9138',
                                }}>
                                Huỷ bỏ
                            </Typography>
                        </Button>
                    </Link>
                    <Button
                        onClick={() => handleUpdate()}
                        style={{
                            width: '147px',
                            height: '48px',
                            borderRadius: '8px',
                            backgroundColor: '#FF9138'
                        }}>
                        <Typography
                            style={{
                                fontFamily: 'Nunito-Bold',
                                fontSize: '16px',
                                color: '#ffffff'
                            }}>
                            Cập nhật
                        </Typography>
                    </Button>

                </div>

            </div>
        </div >
    )
}

export default UpdateDeviceContent