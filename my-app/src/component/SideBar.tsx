import React from 'react'
import Sider from "antd/es/layout/Sider";
import {
    Button,
    Image,
    Menu,
    Typography
} from 'antd';
import {
    AppstoreOutlined,
    DesktopOutlined,
    MessageOutlined,
    SettingOutlined,
    CopyOutlined,
    FileTextOutlined,
    LogoutOutlined
} from '@ant-design/icons';
import { Link, useNavigate } from 'react-router-dom';
import MenuItem from 'antd/es/menu/MenuItem';
function SideBar() {
    const navigate = useNavigate();
    const { SubMenu } = Menu;
    return (
        <Sider
            width={200}
            trigger={null}
            style={{
                minHeight: '100vh',
                backgroundColor: 'white'
            }}
        >
            <div
                style={{
                    justifyContent: 'center',
                    display: 'flex',
                    padding: '32px'
                }}>
                <Image
                    width={100}
                    preview={false}
                    src='https://cdn.discordapp.com/attachments/1029747761094070354/1099033961432367224/image.png'
                    style={{
                        margin: '0 auto'
                    }} />
            </div>
            <Menu
                style={{
                    padding: '32px 0'
                }}>
                <Menu.Item
                    style={{
                        borderRadius: 0,
                        margin: 0,
                        width: '100%',
                        display: 'flex',
                        alignItems: 'center'
                    }}>
                    <Link
                        to={`/dashboard`}
                    >
                        <AppstoreOutlined /> Dashboard
                    </Link>
                </Menu.Item>
                <Menu.Item
                    style={{
                        borderRadius: 0,
                        margin: 0,
                        width: '100%',
                        display: 'flex',
                        alignItems: 'center'
                    }}>
                    <Link
                        to={`/device`}
                    >
                        <DesktopOutlined /> Thiết bị
                    </Link>
                </Menu.Item>
                <Menu.Item
                    style={{
                        borderRadius: 0,
                        margin: 0,
                        width: '100%',
                        display: 'flex',
                        alignItems: 'center'
                    }}>
                    <Link
                        to={`/service`}
                    >
                        <MessageOutlined /> Dịch vụ
                    </Link>
                </Menu.Item>
                <Menu.Item
                    style={{
                        borderRadius: 0,
                        margin: 0,
                        width: '100%',
                        display: 'flex',
                        alignItems: 'center'
                    }}>
                    <Link
                        to={`/progression`}
                    >
                        <CopyOutlined /> Cấp số
                    </Link>
                </Menu.Item>
                <Menu.Item
                    style={{
                        borderRadius: 0,
                        margin: 0,
                        width: '100%',
                        display: 'flex',
                        alignItems: 'center'
                    }}>
                    <Link
                        to={`/report`}
                    >
                        <FileTextOutlined /> Báo cáo
                    </Link>
                </Menu.Item>
                <SubMenu
                    icon={
                        <SettingOutlined style={{ color: '#7E7D88' }} />
                    }
                    title="Cài đặt hệ thống"
                    style={{
                        borderRadius: 0,
                        margin: 0, width: '100%',
                        display: 'flex',
                        alignItems: 'center'
                    }}>
                    <Menu.Item
                        style={{
                            borderRadius: 0,
                            margin: 0, width: '100%',
                            display: 'flex',
                            alignItems: 'center'
                        }}>
                        <Link
                            to={`/setting/role-setting`}
                        >
                            Quản lý vai trò
                        </Link>
                    </Menu.Item>
                    <Menu.Item
                        style={{
                            borderRadius: 0,
                            margin: 0, width: '100%',
                            display: 'flex',
                            alignItems: 'center'
                        }}>
                        <Link
                            to={`/setting/account-setting`}
                        >
                            Quản lý tài khoản
                        </Link>
                    </Menu.Item>
                    <Menu.Item
                        style={{
                            borderRadius: 0,
                            margin: 0, width: '100%',
                            display: 'flex',
                            alignItems: 'center'
                        }}>
                        <Link
                            to={`/setting/user-setting`}
                        >
                            Nhật ký người dùng
                        </Link>
                    </Menu.Item>
                </SubMenu>
            </Menu>
            <div
                style={{
                    display: 'flex',
                    height: '272px',
                    alignItems: 'flex-end'
                }}>
                <Button
                    onClick={() => { navigate("/") }}
                    style={{
                        backgroundColor: '#FFF2E7',
                        border: '0',
                        width: '100%',
                        margin: '14px',
                        height: '40px'
                    }}>
                    <Typography
                        style={{
                            color: '#FF7506',
                            textAlign: 'start'
                        }}>
                        <LogoutOutlined /> Đăng xuất
                    </Typography>
                </Button>
            </div>
        </Sider>
    )
}

export default SideBar