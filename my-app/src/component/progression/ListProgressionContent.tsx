import {
    Input,
    Select,
    Table,
    Typography,
    DatePicker
} from 'antd'
import {
    CaretDownOutlined,
    PlusOutlined,
    SearchOutlined,
    CaretRightOutlined,
    CalendarOutlined
} from '@ant-design/icons';
import { Link } from 'react-router-dom';
import type {
    DatePickerProps,
    RangePickerProps
} from 'antd/es/date-picker';
import React, {
    useState,
    useEffect
} from 'react'
import {
    DocumentData,
    onSnapshot,
    QuerySnapshot
} from "firebase/firestore";
import { Progression } from '../../types/Progression';
import { Service } from '../../types/Service';
import moment from 'moment';
import { progressionCollection } from '../../features/Progression/progressionSlice';
import { servicesCollection } from '../../features/Service/serviceSlice';
function ListProgressionContent() {
    const [progressions, setProgressions] = useState<Progression[]>([]);
    useEffect(() => {
        const unsubscribe = onSnapshot(progressionCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const progressionData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setProgressions(progressionData);
        });

        return () => {
            unsubscribe();
        };
    }, []);
    const { RangePicker } = DatePicker;

    const onChangee = (
        value: DatePickerProps['value'] | RangePickerProps['value'],
        dateString: [string, string] | string
    ) => {
        console.log('Selected Time: ', value);
        console.log('Formatted Selected Time: ', dateString);

        if (!value) {
            setFilteredDateProgressions(progressions); // Hiển thị tất cả các mục khi không có ngày được chọn
            return;
        }

        if (Array.isArray(dateString)) {
            const startDate = moment(dateString[0], 'DD/MM/YYYY');
            const endDate = moment(dateString[1], 'DD/MM/YYYY');

            const filteredData = progressions.filter(progression => {
                if (progression.dayStart && progression.dayEnd) {
                    const dayStart = progression.dayStart.split(' - ')[1];

                    const formattedDayStart = moment(dayStart, 'DD/MM/YYYY');
                    const formattedDayEnd = moment(dayStart, 'DD/MM/YYYY');

                    const isWithinRange =
                        formattedDayStart.isBetween(startDate, endDate, null, '[]') &&
                        formattedDayEnd.isBetween(startDate, endDate, null, '[]');

                    return isWithinRange;
                }

                return false;
            });

            setFilteredDateProgressions(filteredData);
        } else {
            setFilteredDateProgressions([]);
        }
    };
    const onOk = (value: DatePickerProps['value'] | RangePickerProps['value']) => {
        console.log('onOk: ', value);
    };
    const onChange: DatePickerProps['onChange'] = (date, dateString) => {
        console.log(date, dateString);
    };
    const [showMore, setShowMore] = useState(false)
    const handleShowMore = () => {
        setShowMore(true)
    }
    const [services, setServices] = useState<Service[]>([]);

    const [filteredProgressions, setFilteredProgressions] = useState<Progression[]>([]);
    const [filteredDateProgressions, setFilteredDateProgressions] = useState<Progression[]>([]);
    const [selectedService, setSelectedService] = useState<string>('Tất cả');
    const [selectedStatus, setSelectedStatus] = useState<string>('Tất cả');
    const [selectedProvide, setSelectedProvide] = useState<string>('Tất cả');
    const handleService = (value: string) => {
        setSelectedService(value);
        if (value === 'Tất cả') {
            setFilteredProgressions(progressions); // Hiển thị tất cả dữ liệu
        } else {
            const filteredData = progressions.filter(progression => progression.service === value);
            setFilteredProgressions(filteredData);
        }
    };
    const handleStatus = (value: string) => {
        setSelectedStatus(value);
        if (value === 'Tất cả') {
            setFilteredProgressions(progressions); // Hiển thị tất cả dữ liệu
        } else {
            const filteredData = progressions.filter(progression => progression.status === value);
            setFilteredProgressions(filteredData);
        }
    };
    const handleProvide = (value: string) => {
        setSelectedProvide(value);
        if (value === 'Tất cả') {
            setFilteredProgressions(progressions); // Hiển thị tất cả dữ liệu
        } else {
            const filteredData = progressions.filter(progression => progression.provide === value);
            setFilteredProgressions(filteredData);
        }
    };
    useEffect(() => {
        const unsubscribe = onSnapshot(servicesCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const serviceData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setServices(serviceData);
        });

        return () => {
            unsubscribe();
        };
    }, []);
    const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchKeyword(event.target.value);
    }; const [searchKeyword, setSearchKeyword] = useState<string>('');
    useEffect(() => {
        setFilteredDateProgressions(progressions);

    }, [progressions]);

    const columns = [
        {
            title: 'STT',
            dataIndex: 'stt',
            key: 'stt',
            render: (stt: number) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {stt}
                    </Typography>
                );
            },
        },
        {
            title: 'Tên khách hàng',
            dataIndex: 'name',
            key: 'name',
            render: (name: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {name}
                    </Typography>
                );
            },
        },
        {
            title: 'Tên dịch vụ',
            dataIndex: 'service',
            key: 'service',
            render: (service: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {service}
                    </Typography>
                );
            },
        },
        {
            title: 'Thời gian cấp',
            dataIndex: 'dayStart',
            key: 'dayStart',
            render: (dayStart: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {dayStart}
                    </Typography>
                );
            },
        },
        {
            title: 'Hạn sử dụng',
            dataIndex: 'dayEnd',
            key: 'dayEnd',
            render: (dayEnd: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {dayEnd}
                    </Typography>
                );
            },
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            key: 'status',
            render: (status: string) => {
                return (
                    <>
                        {status === 'Đang chờ'
                            ?
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'center'
                                }}>
                                <div
                                    style={{
                                        width: '6px',
                                        height: '6px',
                                        backgroundColor: '#4277FF',
                                        borderRadius: '100%',
                                        marginRight: '2px'
                                    }}></div>
                                <Typography
                                    style={{
                                        fontFamily: 'Nunito-Regular',
                                        fontSize: '14px'
                                    }}>
                                    Đang chờ
                                </Typography>
                            </div>
                            : ""
                        }
                        {
                            status === 'Đã sử dụng'
                                ?
                                <div
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center'
                                    }}>
                                    <div
                                        style={{
                                            width: '6px',
                                            height: '6px',
                                            backgroundColor: '#7E7D88',
                                            borderRadius: '100%',
                                            marginRight: '2px'
                                        }}></div>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-Regular',
                                            fontSize: '14px'
                                        }}>
                                        Đã sử dụng
                                    </Typography>
                                </div>
                                : ""
                        }
                        {
                            status === 'Bỏ qua'
                                ?
                                <div
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center'
                                    }}>
                                    <div
                                        style={{
                                            width: '6px',
                                            height: '6px',
                                            backgroundColor: '#E73F3F',
                                            borderRadius: '100%',
                                            marginRight: '2px'
                                        }}></div>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-Regular',
                                            fontSize: '14px'
                                        }}>
                                        Bỏ qua
                                    </Typography>
                                </div>
                                : ""
                        }
                    </>
                );
            },
        },
        {
            title: 'Nguồn cấp',
            dataIndex: 'provide',
            key: 'provide',
            render: (provide: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {provide}
                    </Typography>
                );
            },
        },
        {
            title: ' ',
            dataIndex: 'detail',
            key: 'detail',
            render: (_: any, record: Progression) => {
                return (
                    <Link
                        to={`/progression/list-progression/detail-progression/id=${record.id}`}>
                        <Typography.Link
                            style={{
                                textDecoration: 'underline',
                                fontFamily: 'Nunito-Regular',
                                fontSize: '14px'
                            }}>
                            Chi tiết
                        </Typography.Link>
                    </Link>
                );
            },
        },
    ];
    return (
        <div
            style={{
                margin: '0 20px',
                width: '1112px',
            }}>
            <Typography
                style={{
                    fontSize: '24px',
                    fontFamily: 'Nunito-Bold',
                    color: '#FF7506',
                }}>
                Quản lý cấp số
            </Typography>
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                }}>
                <div
                    style={{
                        display: 'flex',
                        width: '100%',
                        justifyContent: 'space-between',
                    }}>
                    <div>
                        <Typography
                            style={{
                                fontSize: '16px',
                                fontFamily: 'Nunito-SemiBold',
                            }}>
                            Tên dịch vụ
                        </Typography>
                        <Select
                            defaultValue="Tất cả"
                            onChange={handleService}
                            suffixIcon={<CaretDownOutlined
                                style={{
                                    color: '#FF7506'
                                }}
                            />}
                            listHeight={180}
                            style={{
                                width: '154px',
                                height: '44px',
                                borderRadius: '8px',
                                display: 'flex',
                                justifyContent: 'space-between'
                            }}
                        >
                            <Select.Option
                                className='timeSelect'
                                key={1}
                                Title="Tất cả"
                                value="Tất cả"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '14px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Tất cả
                            </Select.Option>
                            {services &&
                                services.map((service) => (
                                    <Select.Option
                                        value={service.name}
                                        className='timeSelect'
                                        Title={service.name}
                                        key={service.id}
                                        style={{
                                            height: '44px',
                                            alignItems: 'center',
                                            fontSize: '14px',
                                            fontFamily: 'Nunito-Regular'
                                        }}
                                    >
                                        {service.name}
                                    </Select.Option>
                                ))}
                        </Select>
                    </div>
                    <div>
                        <Typography
                            style={{
                                fontSize: '16px',
                                fontFamily: 'Nunito-SemiBold',
                            }}>
                            Tình trạng
                        </Typography>
                        <Select
                            defaultValue="Tất cả"
                            onChange={handleStatus}
                            listHeight={180}
                            suffixIcon={<CaretDownOutlined
                                style={{
                                    color: '#FF7506'
                                }}
                            />}
                            style={{
                                width: '154px',
                                height: '44px',
                                borderRadius: '8px',
                                display: 'flex',
                                justifyContent: 'space-between'
                            }}
                        >
                            <Select.Option
                                className='timeSelect'
                                key={1}
                                Title="Tất cả"
                                value="Tất cả"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '14px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Tất cả
                            </Select.Option>
                            <Select.Option
                                className='timeSelect'
                                key={2}
                                value="Đang chờ"
                                Title="Đang chờ"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '14px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Đang chờ
                            </Select.Option>
                            <Select.Option
                                className='timeSelect'
                                key={3}
                                value="Đã sử dụng"
                                Title="Đã sử dụng"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '14px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Đã sử dụng
                            </Select.Option>
                            <Select.Option
                                className='timeSelect'
                                key={4}
                                value="Bỏ qua"
                                Title="Bỏ qua"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '14px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Bỏ qua
                            </Select.Option>
                        </Select>
                    </div>
                    <div>
                        <Typography
                            style={{
                                fontSize: '16px',
                                fontFamily: 'Nunito-SemiBold',
                            }}>
                            Nguồn cấp
                        </Typography>
                        <Select
                            listHeight={180}
                            defaultValue="Tất cả"
                            onChange={handleProvide}
                            suffixIcon={<CaretDownOutlined
                                style={{
                                    color: '#FF7506'
                                }}
                            />}
                            style={{
                                width: '154px',
                                height: '44px',
                                borderRadius: '8px',
                                display: 'flex',
                                justifyContent: 'space-between'
                            }}
                        >
                            <Select.Option
                                className='timeSelect'
                                key={1}
                                value="Tất cả"
                                Title="Tất cả"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '14px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Tất cả
                            </Select.Option>
                            <Select.Option
                                className='timeSelect'
                                key={2}
                                value="Kiosk"
                                Title="Kiosk"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '14px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Kiosk
                            </Select.Option>
                            <Select.Option
                                className='timeSelect'
                                key={3}
                                value="Hệ thống"
                                Title="Hệ thống"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '14px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Hệ thống
                            </Select.Option>
                        </Select>
                    </div>
                    <div>
                        <Typography
                            style={{
                                fontSize: '16px',
                                fontFamily: 'Nunito-SemiBold',
                            }}>
                            Chọn thời gian
                        </Typography>
                        <RangePicker
                            suffixIcon={<CalendarOutlined
                                style={{
                                    color: '#FF7506'
                                }}
                            />}
                            format="DD/MM/YYYY"
                            onChange={onChangee}
                            onOk={onOk}
                        />
                    </div>
                    <div>
                        <Typography
                            style={{
                                fontSize: '16px',
                                fontFamily: 'Nunito-SemiBold',
                            }}>
                            Từ khoá
                        </Typography>
                        <Input
                            placeholder="Nhập từ khóa"
                            style={{
                                width: '240px',
                                height: '42px'
                            }}
                            onChange={handleSearch}
                            suffix={
                                <SearchOutlined
                                    style={{
                                        color: '#FF7506',
                                        fontSize: '16px'
                                    }} />
                            }
                        />
                    </div>
                </div>
            </div>
            <div
                style={{
                    height: '490px'
                }}
            >
                <Table
                    pagination={{ pageSize: 9 }}
                    dataSource={
                        selectedService === 'Tất cả' && selectedStatus === 'Tất cả' && selectedProvide === 'Tất cả'
                            ? filteredDateProgressions && filteredDateProgressions.filter(
                                progression =>
                                    progression && progression.name && progression.name && progression.name.toLowerCase().includes(searchKeyword.toLowerCase())
                            )
                            : filteredProgressions.filter(
                                progression =>
                                    progression &&
                                    (progression.service === selectedService ||
                                        progression.status === selectedStatus ||
                                        progression.provide === selectedProvide)

                            )
                    }
                    columns={columns}
                    rowClassName={(record, index) =>
                        index % 2 === 0 ? "bg-white" : "bg-orange-50"
                    }
                    style={{
                        height: '490px',
                        marginTop: '4px'
                    }}
                />
            </div>
            <Link
                to={'/progression/list-progression/new-progression'}
                style={{
                    display: 'flex',
                    position: 'absolute',
                    top: '24.4%',
                    left: '94%',
                    backgroundColor: '#FFF2E7',
                    flexDirection: 'column',
                    padding: '12px',
                    width: '92px',
                    borderRadius: '8px',
                    boxShadow: '0px 0px 6px #E7E9F2',
                    cursor: 'pointer'
                }}>
                <PlusOutlined
                    style={{
                        color: '#ffffff',
                        width: '28px',
                        height: '28px',
                        backgroundColor: '#FF9138',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: '8px',
                        marginBottom: '4px',
                        alignSelf: 'center',
                    }} />
                <Typography
                    style={{
                        fontFamily: 'Nunito-SemiBold',
                        fontSize: '14px',
                        color: '#FF9138',
                        textAlign: 'center',
                    }}>
                    Cấp số mới
                </Typography>
            </Link>
        </div>
    )
}

export default ListProgressionContent