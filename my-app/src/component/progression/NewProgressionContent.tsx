import {
    Button,
    Select,
    Typography,
    Modal
} from 'antd'
import React, {
    useState,
    useEffect,
    useContext
} from 'react'
import {
    CaretDownOutlined,
} from '@ant-design/icons';
import { Service } from '../../types/Service';
import {
    DocumentData,
    onSnapshot,
    QuerySnapshot
} from "firebase/firestore";
import { Device } from '../../types/Device';
import { User } from '../../types/User';
import { Link, } from 'react-router-dom';
import { IdContext } from '../../IdContext';
import { Progression } from '../../types/Progression';
import { useDispatch } from 'react-redux';
import { addProgression, progressionCollection } from '../../features/Progression/progressionSlice';
import { servicesCollection } from '../../features/Service/serviceSlice';
import { devicesCollection } from '../../features/Device/deviceSlice';
import { usersCollection } from '../../features/User/userSlice';
function NewProgressionContent() {
    const [popup, setPopup] = useState(false)
    const popupChange = () => {
        setPopup(true)
    }
    const handleCancel = () => {
        setPopup(false);
    };
    const { id } = useContext(IdContext);
    const [services, setServices] = useState<Service[]>([]);
    const [progressions, setProgressions] = useState<Progression[]>([]);
    const [devices, setDevices] = useState<Device[]>([]);
    const [users, setUsers] = useState<User[]>([]);
    const [service, setService] = useState<string>();
    const [stt, setStt] = useState(1);
    const [dayStart, setDayStart] = useState("");
    const [dayEnd, setDayEnd] = useState("");
    const [email, setEmail] = useState<string>();
    const [phone, setPhone] = useState<string>();
    const [status, setStatus] = useState("Đang chờ");
    const [provide, setProvide] = useState<string>();
    const [name, setName] = useState<string>();
    const dispatch = useDispatch();
    const handleService = (value: any) => {
        setService(value);
    };
    const addNewProgression = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        try {
            await dispatch(
                addProgression({
                    stt,
                    service,
                    name,
                    dayStart,
                    dayEnd,
                    status,
                    provide,
                    email,
                    phone
                }) as any
            );
            console.log('successfully added a new device');
        } catch (error) {
            console.log('error:', error);
        }
        popupChange()
        console.log("successfully added a new progression");
    };
    useEffect(() => {
        const unsubscribe = onSnapshot(servicesCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const serviceData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setServices(serviceData);
        });

        return () => {
            unsubscribe();
        };
    }, []);
    useEffect(() => {
        const unsubscribe = onSnapshot(devicesCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const deviceData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setDevices(deviceData);
        });

        return () => {
            unsubscribe();
        };
    }, []);
    useEffect(() => {
        const unsubscribe = onSnapshot(usersCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const userData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setUsers(userData);
        });

        return () => {
            unsubscribe();
        };
    }, []);
    useEffect(() => {
        const unsubscribe = onSnapshot(progressionCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const progressionData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setProgressions(progressionData);
        });

        return () => {
            unsubscribe();
        };
    }, []);
    useEffect(
        () => {
            const device = devices.find((device) => device.service && device.service.includes(service as string));
            if (device) {
                setProvide(device.name);
                console.log(provide);
                console.log(device.name);
                console.log(service);
                setStatus("Đã sử dụng")
            }
            else {
                setProvide('Hệ thống');
                setStatus("Đang chờ")
            }
            const user = users.find((user) => user.id === id);
            if (user) {
                setName(user.name);
                setPhone(user.phone);
                setEmail(user.email);
                const currentTime = new Date();
                const hours = currentTime.getHours().toString().padStart(2, '0');
                const minutes = currentTime.getMinutes().toString().padStart(2, '0');
                const timeStart = `${hours}:${minutes}`;
                const currentDate = new Date();
                const day = currentDate.getDate().toString().padStart(2, '0');
                const month = (currentDate.getMonth() + 1).toString().padStart(2, '0');
                const year = currentDate.getFullYear().toString();
                const dateStart = `${day}/${month}/${year}`;
                const dateStartObject = new Date(`${year}-${month}-${day}`);
                const dateEndObject = new Date(dateStartObject);
                dateEndObject.setDate(dateEndObject.getDate() + 5);
                const dateEnd = `${dateEndObject.getDate().toString().padStart(2, '0')}/${(dateEndObject.getMonth() + 1).toString().padStart(2, '0')}/${dateEndObject.getFullYear().toString()}`;
                const dayStart = timeStart + ' - ' + dateStart
                const dayEnd = timeStart + ' - ' + dateEnd
                setDayStart(dayStart);
                setDayEnd(dayEnd);
            }
            if (!stt) {
                const maxStt = Math.max(...progressions.map((progression) => Number(progression.stt)), 0);
                setStt((maxStt + 1));
                console.log(stt)
            } else {
                const maxStt = Math.max(...progressions.map((progression) => Number(progression.stt)), 0);
                setStt((maxStt + 1));
                console.log(stt)
            }
        },
        [addNewProgression]
    )
    return (
        <div
            style={{
                margin: '0 20px'
            }}
        >
            <Typography
                style={{
                    color: '#FF7506',
                    fontFamily: 'Nunito-Bold',
                    fontSize: '24px'
                }}
            >
                Quản lý cấp số
            </Typography>
            <div
                style={{
                    width: '1192px',
                    height: '604px',
                    backgroundColor: '#ffffff',
                    boxShadow: '2px 2px 8px rgba(232, 239, 244, 0.8)',
                    borderRadius: '16px',
                    padding: '20px',
                    margin: '7px 0'
                }}>
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        width: '100%'
                    }}>
                    <Typography
                        style={{
                            color: '#FF9138',
                            fontFamily: 'Nunito-Bold',
                            fontSize: '32px',
                            paddingBottom: '10px'
                        }}>
                        CẤP SỐ MỚI
                    </Typography>
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Bold',
                            fontSize: '20px',
                            paddingBottom: '10px'
                        }}>
                        Dịch vụ khách hàng lựa chọn
                    </Typography>
                    <div
                        style={{
                            paddingBottom: '10px'
                        }}>
                        <Select
                            value={service}
                            listHeight={180}
                            placeholder="Chọn dịch vụ"
                            onChange={handleService}
                            suffixIcon={<CaretDownOutlined
                                style={{
                                    color: '#FF7506'
                                }}
                            />}
                            style={{
                                width: '400px',
                                height: '44px',
                                borderRadius: '8px',
                                display: 'flex',
                                justifyContent: 'space-between'
                            }}
                        >
                            {services &&
                                services.map((service) => (
                                    <Select.Option
                                        value={service.name}
                                        className='timeSelect'
                                        Title={service.name}
                                        key={service.id}
                                        style={{
                                            height: '44px',
                                            alignItems: 'center',
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-Regular'
                                        }}
                                    >
                                        {service.name}
                                    </Select.Option>
                                ))}
                        </Select>
                    </div>
                    <div
                        style={{
                            height: '200px',
                            width: '300px',
                            display: 'flex',
                            justifyContent: 'space-around',
                            alignItems: 'center',
                        }}
                    >
                        <Link
                            to={'/progression/list-progression'}>
                            <Button
                                style={{
                                    width: '115px',
                                    height: '48px',
                                    borderRadius: '8px',
                                    backgroundColor: '#FFF2E7',
                                    borderWidth: '1.5px',
                                    borderColor: '#FF9138',

                                }}>
                                <Typography
                                    style={{
                                        fontFamily: 'Nunito-Bold',
                                        fontSize: '16px',
                                        color: '#FF9138',
                                    }}>
                                    Huỷ bỏ
                                </Typography>
                            </Button>
                        </Link>
                        <Button
                            onClick={(e: any) => addNewProgression(e)}
                            style={{
                                width: '115px',
                                height: '48px',
                                borderRadius: '8px',
                                backgroundColor: '#FF9138'
                            }}>
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-Bold',
                                    fontSize: '16px',
                                    color: '#ffffff'
                                }}>
                                In số
                            </Typography>
                        </Button>
                        <Modal
                            onCancel={handleCancel}
                            width={469}
                            closable
                            bodyStyle={{
                                height: '264px',
                                padding: '44px 24px',
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'space-between',
                                alignItems: 'center'
                            }}
                            open={popup}
                            footer={[
                                <div
                                    style={{
                                        backgroundColor: '#FF9138',
                                        padding: '20px 24px',
                                        borderBottomLeftRadius: '8px',
                                        borderBottomRightRadius: '8px',
                                        display: 'flex',
                                        flexDirection: 'column',
                                        textAlign: 'center',
                                    }}>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-Bold',
                                            color: '#ffffff',
                                            fontSize: '22px',
                                        }}>
                                        Thời gian cấp: {dayStart}
                                    </Typography>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-Bold',
                                            fontSize: '22px',
                                            color: '#ffffff'
                                        }}>
                                        Hạn sử dụng: {dayEnd}
                                    </Typography>
                                </div>
                            ]}>
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-Bold',
                                    fontSize: '32px'
                                }}>
                                Số thứ tự được cấp
                            </Typography>
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-ExtraBold',
                                    fontSize: '56px',
                                    color: '#FF7506'
                                }}>
                                {stt - 1}
                            </Typography>
                            <div
                                style={{
                                    display: 'flex',
                                }}>
                                <Typography
                                    style={{
                                        fontFamily: 'Nunito-SemiBold',
                                        fontSize: '18px',
                                        marginRight: '4px'
                                    }}>
                                    DV: {service}
                                </Typography>
                                <Typography
                                    style={{
                                        fontFamily: 'Nunito-Bold',
                                        fontSize: '18px',
                                    }}>
                                    (tại quầy số 1)
                                </Typography>
                            </div>
                        </Modal>
                    </div>
                </div>
            </div>
        </div >
    )
}

export default NewProgressionContent