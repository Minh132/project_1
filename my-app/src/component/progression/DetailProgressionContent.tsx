import { Typography } from 'antd'
import React, {
    useState,
    useEffect
} from 'react';
import {
    RollbackOutlined
} from '@ant-design/icons';
import {
    DocumentData,
    onSnapshot,
    QuerySnapshot
} from "firebase/firestore";
import {
    Link,
    useLocation
} from 'react-router-dom';
import { Progression } from '../../types/Progression';
import { progressionCollection } from '../../features/Progression/progressionSlice';
function DetailProgressionContent() {
    const [progressions, setProgressions] = useState<Progression[]>([]);
    const [id, setId] = useState('');
    const location = useLocation();
    const path = location.pathname;
    const searchTerm = "id=";
    useEffect(() => {
        const startIndex = path.indexOf(searchTerm);
        if (startIndex !== -1) {
            const desiredValue = path.substring(startIndex + searchTerm.length);
            setId(desiredValue);
        }
        console.log(id);
    })
    useEffect(() => {
        const unsubscribe = onSnapshot(progressionCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const progressionData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setProgressions(progressionData);
        });

        return () => {
            unsubscribe();
        };
    }, []);
    return (
        <div
            style={{
                margin: '0 20px'
            }}
        >
            <Typography
                style={{
                    color: '#FF7506',
                    fontFamily: 'Nunito-Bold',
                    fontSize: '24px'
                }}
            >
                Quản lý thiết bị
            </Typography>
            <div
                style={{
                    width: '1112px',
                    height: '604px',
                    backgroundColor: '#FFFFFF',
                    boxShadow: '2px 2px 8px rgba(232, 239, 244, 0.8)',
                    borderRadius: '16px',
                    padding: '20px',
                    margin: '7px 0'
                }}>
                {progressions.map((progression) =>
                    progression.id === id ?
                        <>
                            <Typography
                                style={{
                                    color: '#FF9138',
                                    fontFamily: 'Nunito-Bold',
                                    fontSize: '20px'
                                }}>
                                Thông tin cấp số
                            </Typography>
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    marginTop: '6px',
                                    marginBottom: '6px',
                                }}>
                                <div>
                                    <div
                                        style={{
                                            display: 'flex',
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-SemiBold',
                                                paddingBottom: '6px',
                                                width: '160px'
                                            }}>
                                            Họ tên:
                                        </Typography>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-Regular',
                                                paddingBottom: '6px',
                                                width: '360px',

                                                color: '#535261'
                                            }}>
                                            {progression.name}
                                        </Typography>
                                    </div>
                                </div>

                                <div>
                                    <div
                                        style={{
                                            display: 'flex',
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-SemiBold',
                                                paddingBottom: '6px',
                                                width: '160px'
                                            }}>
                                            Nguồn cấp:
                                        </Typography>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-Regular',
                                                paddingBottom: '6px',
                                                width: '360px',

                                                color: '#535261'
                                            }}>
                                            {progression.provide}
                                        </Typography>
                                    </div>
                                </div>
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    marginTop: '6px',
                                    marginBottom: '6px',
                                }}>
                                <div>
                                    <div
                                        style={{
                                            display: 'flex',
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-SemiBold',
                                                paddingBottom: '6px',
                                                width: '160px'
                                            }}>
                                            Tên dịch vụ:
                                        </Typography>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-Regular',
                                                paddingBottom: '6px',
                                                width: '360px',

                                                color: '#535261'
                                            }}>
                                            {progression.service}
                                        </Typography>
                                    </div>
                                </div>

                                <div>
                                    <div
                                        style={{
                                            display: 'flex',
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-SemiBold',
                                                paddingBottom: '6px',
                                                width: '160px'
                                            }}>
                                            Trạng thái:
                                        </Typography>
                                        <div
                                            style={{
                                                display: 'flex',
                                                alignItems: 'center'
                                            }}>
                                            <div
                                                style={{
                                                    height: '12px',
                                                }}>
                                                {progression.status === 'Đang chờ'
                                                    ?
                                                    <div
                                                        style={{
                                                            width: '6px',
                                                            height: '6px',
                                                            backgroundColor: '#4277FF',
                                                            borderRadius: '100%',
                                                            marginRight: '2px'
                                                        }}></div>
                                                    : ''}
                                                {progression.status === 'Đã sử dụng'
                                                    ?
                                                    <div
                                                        style={{
                                                            width: '6px',
                                                            height: '6px',
                                                            backgroundColor: '#7E7D88',
                                                            borderRadius: '100%',
                                                            marginRight: '2px'
                                                        }}></div>
                                                    : ''}
                                                {progression.status === 'Bỏ qua'
                                                    ?
                                                    <div
                                                        style={{
                                                            width: '6px',
                                                            height: '6px',
                                                            backgroundColor: '#E73F3F',
                                                            borderRadius: '100%',
                                                            marginRight: '2px'
                                                        }}></div>
                                                    : ''}

                                            </div>
                                            <Typography
                                                style={{
                                                    fontSize: '16px',
                                                    fontFamily: 'Nunito-Regular',
                                                    paddingBottom: '6px',
                                                    width: '352px',
                                                    color: '#535261'
                                                }}>
                                                {progression.status}
                                            </Typography>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    marginTop: '6px',
                                    marginBottom: '6px',
                                }}>
                                <div>
                                    <div
                                        style={{
                                            display: 'flex',
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-SemiBold',
                                                paddingBottom: '6px',
                                                width: '160px'
                                            }}>
                                            Số thứ tự:
                                        </Typography>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-Regular',
                                                paddingBottom: '6px',
                                                width: '360px',

                                                color: '#535261'
                                            }}>
                                            {progression.stt}
                                        </Typography>
                                    </div>
                                </div>

                                <div>
                                    <div
                                        style={{
                                            display: 'flex',
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-SemiBold',
                                                paddingBottom: '6px',
                                                width: '160px'
                                            }}>
                                            Số điện thoại:
                                        </Typography>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-Regular',
                                                paddingBottom: '6px',
                                                width: '360px',

                                                color: '#535261'
                                            }}>
                                            {progression.phone}
                                        </Typography>
                                    </div>
                                </div>
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    marginTop: '6px',
                                    marginBottom: '6px',
                                }}>
                                <div>
                                    <div
                                        style={{
                                            display: 'flex',
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-SemiBold',
                                                paddingBottom: '6px',
                                                width: '160px'
                                            }}>
                                            Thời gian cấp:
                                        </Typography>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-Regular',
                                                paddingBottom: '6px',
                                                width: '360px',

                                                color: '#535261'
                                            }}>
                                            {progression.dayStart}
                                        </Typography>
                                    </div>
                                </div>

                                <div>
                                    <div
                                        style={{
                                            display: 'flex',
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-SemiBold',
                                                paddingBottom: '6px',
                                                width: '160px'
                                            }}>
                                            Địa chỉ Email:
                                        </Typography>
                                        <Typography
                                            style={{
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-Regular',
                                                paddingBottom: '6px',
                                                width: '360px',

                                                color: '#535261'
                                            }}>
                                            {progression.email}
                                        </Typography>
                                    </div>
                                </div>
                            </div>
                            <div
                                style={{
                                    marginTop: '6px',
                                    marginBottom: '6px',
                                }}>
                                <div
                                    style={{
                                        display: 'flex'
                                    }}>
                                    <Typography
                                        style={{
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-SemiBold',
                                            paddingBottom: '6px',
                                            width: '160px'
                                        }}>
                                        Hạn sử dụng:
                                    </Typography>
                                    <Typography
                                        style={{
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-Regular',
                                            paddingBottom: '6px',
                                            color: '#535261'
                                        }}>
                                        {progression.dayEnd}
                                    </Typography>
                                </div>
                            </div>
                        </> : '')}
            </div>
            <Link
                to={'/progression/list-progression'}
                style={{
                    display: 'flex',
                    position: 'absolute',
                    top: '15%',
                    left: '94%',
                    backgroundColor: '#FFF2E7',
                    flexDirection: 'column',
                    padding: '12px',
                    width: '92px',
                    borderRadius: '8px',
                    boxShadow: '0px 0px 6px #E7E9F2',
                    cursor: 'pointer'
                }}>
                <RollbackOutlined
                    style={{
                        color: '#ffffff',
                        width: '28px',
                        height: '28px',
                        backgroundColor: '#FF9138',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: '8px',
                        marginBottom: '4px',
                        alignSelf: 'center',
                    }} />
                <Typography
                    style={{
                        fontFamily: 'Nunito-SemiBold',
                        fontSize: '14px',
                        color: '#FF9138',
                        textAlign: 'center',
                    }}>
                    Quay lại
                </Typography>
            </Link>
        </div>
    )
}

export default DetailProgressionContent