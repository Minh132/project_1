import {
    Input,
    Table,
    Typography,
    DatePicker
} from 'antd'
import {
    SearchOutlined,
    CalendarOutlined
} from '@ant-design/icons';
import type {
    DatePickerProps,
    RangePickerProps
} from 'antd/es/date-picker';
function UserSettingContent() {
    const { RangePicker } = DatePicker;

    const onChangee = (
        value: DatePickerProps['value'] | RangePickerProps['value'],
        dateString: [string, string] | string,
    ) => {
        console.log('Selected Time: ', value);
        console.log('Formatted Selected Time: ', dateString);
    };

    const onOk = (value: DatePickerProps['value'] | RangePickerProps['value']) => {
        console.log('onOk: ', value);
    };
    const dataSource = [
        {
            key: '1',
            account: 'tuyetnguyen@12',
            time: '01/12/2021 15:12:17',
            ip: '192.168.3.1',
            operation: 'Cập nhật thông tin dịch vụ DV_01',
        },
        {
            key: '2',
            account: 'tuyetnguyen@13',
            time: '01/12/2021 15:12:17',
            ip: '192.168.3.1',
            operation: 'Cập nhật thông tin dịch vụ DV_01',
        },
        {
            key: '3',
            account: 'tuyetnguyen@12',
            time: '01/12/2021 15:12:17',
            ip: '192.168.3.1',
            operation: 'Cập nhật thông tin dịch vụ DV_01',
        },
        {
            key: '4',
            account: 'tuyetnguyen@13',
            time: '01/12/2021 15:12:17',
            ip: '192.168.3.1',
            operation: 'Cập nhật thông tin dịch vụ DV_01',
        },
        {
            key: '5',
            account: 'tuyetnguyen@12',
            time: '01/12/2021 15:12:17',
            ip: '192.168.3.1',
            operation: 'Cập nhật thông tin dịch vụ DV_01',
        },
        {
            key: '6',
            account: 'tuyetnguyen@13',
            time: '01/12/2021 15:12:17',
            ip: '192.168.3.1',
            operation: 'Cập nhật thông tin dịch vụ DV_01',
        },
        {
            key: '7',
            account: 'tuyetnguyen@12',
            time: '01/12/2021 15:12:17',
            ip: '192.168.3.1',
            operation: 'Cập nhật thông tin dịch vụ DV_01',
        },
        {
            key: '8',
            account: 'tuyetnguyen@13',
            time: '01/12/2021 15:12:17',
            ip: '192.168.3.1',
            operation: 'Cập nhật thông tin dịch vụ DV_01',
        },
        {
            key: '9',
            account: 'tuyetnguyen@12',
            time: '01/12/2021 15:12:17',
            ip: '192.168.3.1',
            operation: 'Cập nhật thông tin dịch vụ DV_01',
        },
        {
            key: '10',
            account: 'tuyetnguyen@13',
            time: '01/12/2021 15:12:17',
            ip: '192.168.3.1',
            operation: 'Cập nhật thông tin dịch vụ DV_01',
        },
    ];

    const columns = [
        {
            title: 'Tên đăng nhập',
            dataIndex: 'account',
            key: 'account',
            render: (account: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {account}
                    </Typography>
                );
            },
        },
        {
            title: 'Thời gian tác động',
            dataIndex: 'time',
            key: 'time',
            render: (time: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {time}
                    </Typography>
                );
            },
        },
        {
            title: 'IP thực hiện',
            dataIndex: 'ip',
            key: 'ip',
            render: (ip: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {ip}
                    </Typography>
                );
            },
        },
        {
            title: 'Thao tác thực hiện',
            dataIndex: 'operation',
            key: 'operation',
            render: (operation: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {operation}
                    </Typography>
                );
            },
        },
    ];
    return (
        <div
            style={{
                margin: '0 20px',
                width: '1112px',
            }}>
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                }}>
                <div
                    style={{
                        display: 'flex',
                        width: '100%',
                        justifyContent: 'space-between',
                    }}>
                    <div>
                        <Typography
                            style={{
                                fontSize: '16px',
                                fontFamily: 'Nunito-SemiBold',
                            }}>
                            Chọn thời gian
                        </Typography>
                        <RangePicker
                            suffixIcon={<CalendarOutlined
                                style={{
                                    color: '#FF7506'
                                }}
                            />}
                            format="DD/MM/YYYY"
                            onChange={onChangee}
                            onOk={onOk}
                        />
                    </div>
                    <div>
                        <Typography
                            style={{
                                fontSize: '16px',
                                fontFamily: 'Nunito-SemiBold',
                            }}>
                            Từ khoá
                        </Typography>
                        <Input
                            placeholder="Nhập từ khóa"
                            style={{
                                width: '300px',
                                height: '42px'
                            }}
                            suffix={
                                <SearchOutlined
                                    style={{
                                        color: '#FF7506',
                                        fontSize: '16px'
                                    }} />
                            }
                        />
                    </div>
                </div>
            </div>
            <div
                style={{
                    height: '490px'
                }}
            >
                <Table
                    pagination={{ pageSize: 10 }}
                    dataSource={dataSource}
                    columns={columns}
                    rowClassName={(record, index) =>
                        index % 2 === 0 ? "bg-white" : "bg-orange-50"
                    }
                    style={{
                        height: '490px',
                        marginTop: '4px'
                    }}
                />
            </div>
        </div>
    )
}

export default UserSettingContent