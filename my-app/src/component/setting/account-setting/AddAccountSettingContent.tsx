import {
    Button,
    Input,
    Select,
    Typography
} from 'antd'
import React, {
    useState,
    useEffect
} from 'react';
import {
    CaretDownOutlined,
    EyeInvisibleOutlined,
    EyeTwoTone
} from '@ant-design/icons';
import { Link } from 'react-router-dom';
import {
    DocumentData,
    onSnapshot,
    QuerySnapshot
} from "firebase/firestore";
import { addUser } from '../../../features/User/userSlice';
import { Role } from '../../../types/Role';
import { useDispatch } from 'react-redux';
import { rolesCollection } from '../../../features/Role/roleSlice';
function AddAccountSettingContent() {
    const [name, setName] = useState("");
    const [userAccount, setUserAccount] = useState("");
    const [phone, setPhone] = useState("");
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("");
    const [roles, setRoles] = useState<Role[]>([]);
    const [role, setRole] = useState("");
    const [active, setActive] = useState("Tất cả");
    const dispatch = useDispatch();
    const addNewUser = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        try {
            await dispatch(
                addUser({
                    name,
                    userAccount,
                    phone,
                    password,
                    email,
                    role,
                    active,
                }) as any
            );
            console.log('successfully added a new user');
        } catch (error) {
            console.log('error:', error);
        }
        console.log("successfully added a new user");
    };
    const handleSelectRoleChange = (value: string) => {
        setRole(value);
    };
    const handleSelectActiveChange = (value: string) => {
        setActive(value);
    };
    useEffect(() => {
        const unsubscribe = onSnapshot(rolesCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const roleData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setRoles(roleData);
        });

        return () => {
            unsubscribe();
        };
    }, []);
    return (
        <div
            style={{
                margin: '0 20px'
            }}
        >
            <Typography
                style={{
                    color: '#FF7506',
                    fontFamily: 'Nunito-Bold',
                    fontSize: '24px'
                }}
            >
                Quản lý tài khoản
            </Typography>
            <div
                style={{
                    width: '1178px',
                    height: '534px',
                    backgroundColor: '#ffffff',
                    boxShadow: '2px 2px 8px rgba(232, 239, 244, 0.8)',
                    borderRadius: '16px',
                    padding: '20px',
                    margin: '7px 0'
                }}>
                <div>
                    <Typography
                        style={{
                            color: '#FF9138',
                            fontFamily: 'Nunito-Bold',
                            fontSize: '20px'
                        }}>
                        Thông tin tài khoản
                    </Typography>
                    <div
                        style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            marginTop: '6px',
                            marginBottom: '6px',
                        }}>
                        <div>
                            <div
                                style={{
                                    display: 'flex',
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-SemiBold',
                                        paddingBottom: '6px'
                                    }}>
                                    Họ tên
                                </Typography>
                                <div
                                    style={{
                                        width: '25px',
                                        height: '25px'
                                    }}
                                >
                                    <Typography
                                        style={{
                                            color: 'red',
                                            fontSize: '16px',
                                            alignItems: 'center',
                                            display: 'flex',
                                            height: '32px',
                                            marginLeft: '2px'
                                        }}>
                                        *
                                    </Typography>
                                </div>
                            </div>
                            <Input
                                onChange={(e) => setName(e.target.value)}
                                placeholder="Nhập họ tên"
                                style={{
                                    width: '560px',
                                    height: '44px',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-SemiBold',
                                    color: '#535261'
                                }}
                            />
                        </div>

                        <div>
                            <div
                                style={{
                                    display: 'flex',
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-SemiBold',
                                        paddingBottom: '6px'
                                    }}>
                                    Tên đăng nhập:
                                </Typography>
                                <div
                                    style={{
                                        width: '25px',
                                        height: '25px'
                                    }}
                                >
                                    <Typography
                                        style={{
                                            color: 'red',
                                            fontSize: '16px',
                                            alignItems: 'center',
                                            display: 'flex',
                                            height: '32px',
                                            marginLeft: '2px'
                                        }}>
                                        *
                                    </Typography>
                                </div>
                            </div>
                            <Input
                                onChange={(e) => setUserAccount(e.target.value)}
                                placeholder="Nhập tên đăng nhập"
                                style={{
                                    width: '560px',
                                    height: '44px',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-SemiBold',
                                    color: '#535261'
                                }}
                            />
                        </div>
                    </div>
                    <div
                        style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            marginTop: '6px',
                            marginBottom: '6px',
                        }}>
                        <div>
                            <div
                                style={{
                                    display: 'flex',
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-SemiBold',
                                        paddingBottom: '6px'
                                    }}>
                                    Số điện thoại
                                </Typography>
                                <div
                                    style={{
                                        width: '25px',
                                        height: '25px'
                                    }}
                                >
                                    <Typography
                                        style={{
                                            color: 'red',
                                            fontSize: '16px',
                                            alignItems: 'center',
                                            display: 'flex',
                                            height: '32px',
                                            marginLeft: '2px'
                                        }}>
                                        *
                                    </Typography>
                                </div>
                            </div>
                            <Input
                                onChange={(e) => setPhone(e.target.value)}
                                placeholder="Nhập số điện thoại"
                                style={{
                                    width: '560px',
                                    height: '44px',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-SemiBold',
                                    color: '#535261'
                                }}
                            />
                        </div>

                        <div>
                            <div
                                style={{
                                    display: 'flex',
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-SemiBold',
                                        paddingBottom: '6px'
                                    }}>
                                    Mật khẩu:
                                </Typography>
                                <div
                                    style={{
                                        width: '25px',
                                        height: '25px'
                                    }}
                                >
                                    <Typography
                                        style={{
                                            color: 'red',
                                            fontSize: '16px',
                                            alignItems: 'center',
                                            display: 'flex',
                                            height: '32px',
                                            marginLeft: '2px'
                                        }}>
                                        *
                                    </Typography>
                                </div>
                            </div>
                            <Input.Password
                                onChange={(e) => setPassword(e.target.value)}
                                iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                                placeholder="Nhập mật khẩu"
                                style={{
                                    width: '560px',
                                    height: '44px',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-SemiBold',
                                    color: '#535261'
                                }}
                            />
                        </div>
                    </div>
                    <div
                        style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            marginTop: '6px',
                            marginBottom: '6px',
                        }}>
                        <div>
                            <div
                                style={{
                                    display: 'flex',
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-SemiBold',
                                        paddingBottom: '6px'
                                    }}>
                                    Email
                                </Typography>
                                <div
                                    style={{
                                        width: '25px',
                                        height: '25px'
                                    }}
                                >
                                    <Typography
                                        style={{
                                            color: 'red',
                                            fontSize: '16px',
                                            alignItems: 'center',
                                            display: 'flex',
                                            height: '32px',
                                            marginLeft: '2px'
                                        }}>
                                        *
                                    </Typography>
                                </div>
                            </div>
                            <Input
                                onChange={(e) => setEmail(e.target.value)}
                                placeholder="Nhập email"
                                style={{
                                    width: '560px',
                                    height: '44px',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-SemiBold',
                                    color: '#535261'
                                }}
                            />
                        </div>

                        <div>
                            <div
                                style={{
                                    display: 'flex',
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-SemiBold',
                                        paddingBottom: '6px'
                                    }}>
                                    Nhập lại mật khẩu
                                </Typography>
                                <div
                                    style={{
                                        width: '25px',
                                        height: '25px'
                                    }}
                                >
                                    <Typography
                                        style={{
                                            color: 'red',
                                            fontSize: '16px',
                                            alignItems: 'center',
                                            display: 'flex',
                                            height: '32px',
                                            marginLeft: '2px'
                                        }}>
                                        *
                                    </Typography>
                                </div>
                            </div>
                            <Input.Password
                                iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                                placeholder="Nhập lại mật khẩu"
                                style={{
                                    width: '560px',
                                    height: '44px',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-SemiBold',
                                    color: '#535261'
                                }}
                            />
                        </div>
                    </div>
                    <div
                        style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            marginTop: '6px',
                            marginBottom: '6px',
                        }}>
                        <div>
                            <div
                                style={{
                                    display: 'flex',
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-SemiBold',
                                        paddingBottom: '6px'
                                    }}>
                                    Vai trò
                                </Typography>
                                <div
                                    style={{
                                        width: '25px',
                                        height: '25px'
                                    }}
                                >
                                    <Typography
                                        style={{
                                            color: 'red',
                                            fontSize: '16px',
                                            alignItems: 'center',
                                            display: 'flex',
                                            height: '32px',
                                            marginLeft: '2px'
                                        }}>
                                        *
                                    </Typography>
                                </div>
                            </div>
                            <Select
                                value={role}
                                onChange={handleSelectRoleChange}
                                placeholder={'Chọn vai trò'}
                                suffixIcon={<CaretDownOutlined
                                    style={{
                                        color: '#FF7506'
                                    }}
                                />}
                                style={{
                                    width: '560px',
                                    height: '44px',
                                    borderRadius: '8px',
                                    display: 'flex',
                                    justifyContent: 'space-between'
                                }}
                            >
                                {roles &&
                                    roles.map((role) => (
                                        <Select.Option
                                            value={role.name}
                                            className='timeSelect'
                                            key={role.id}
                                            Title={role.name}
                                            style={{
                                                height: '44px',
                                                alignItems: 'center',
                                                fontSize: '16px',
                                                fontFamily: 'Nunito-Regular'
                                            }}
                                        >
                                            {role.name}
                                        </Select.Option>
                                    ))}
                            </Select>
                        </div>

                        <div>
                            <div
                                style={{
                                    display: 'flex',
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-SemiBold',
                                        paddingBottom: '6px'
                                    }}>
                                    Tình trạng
                                </Typography>
                                <div
                                    style={{
                                        width: '25px',
                                        height: '25px'
                                    }}
                                >
                                    <Typography
                                        style={{
                                            color: 'red',
                                            fontSize: '16px',
                                            alignItems: 'center',
                                            display: 'flex',
                                            height: '32px',
                                            marginLeft: '2px'
                                        }}>
                                        *
                                    </Typography>
                                </div>
                            </div>
                            <Select
                                value={active}
                                onChange={handleSelectActiveChange}
                                placeholder={'Chọn tình trạng'}
                                suffixIcon={<CaretDownOutlined
                                    style={{
                                        color: '#FF7506'
                                    }}
                                />}
                                style={{
                                    width: '560px',
                                    height: '44px',
                                    borderRadius: '8px',
                                    display: 'flex',
                                    justifyContent: 'space-between'
                                }}
                            >
                                <Select.Option
                                    value="Tất cả"
                                    className='timeSelect'
                                    key={1}
                                    Title="Tất cả"
                                    style={{
                                        height: '44px',
                                        alignItems: 'center',
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-Regular'
                                    }}
                                >
                                    Tất cả
                                </Select.Option>
                                <Select.Option
                                    className='timeSelect'
                                    key={2}
                                    value="Ngưng hoạt động"
                                    Title="Ngưng hoạt động"
                                    style={{
                                        height: '44px',
                                        alignItems: 'center',
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-Regular'
                                    }}
                                >
                                    Ngưng hoạt động
                                </Select.Option>
                                <Select.Option
                                    className='timeSelect'
                                    key={3}
                                    value="Hoạt động"
                                    Title="Hoạt động"
                                    style={{
                                        height: '44px',
                                        alignItems: 'center',
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-Regular'
                                    }}
                                >
                                    Hoạt động
                                </Select.Option>
                            </Select>
                        </div>
                    </div>
                    <div
                        style={{
                            display: 'flex',
                            paddingTop: '6px',
                            paddingBottom: '6px',
                        }}>
                        <Typography
                            style={{
                                color: 'red',
                                fontSize: '14px',
                                alignItems: 'center',
                                display: 'flex',
                                height: '28px',
                                marginRight: '2px'
                            }}>
                            *
                        </Typography>
                        <Typography
                            style={{
                                fontSize: '14px',
                                fontFamily: 'Nunito-Regular',
                                color: '#7E7D88'
                            }}>
                            Là trường thông tin bắt buộc
                        </Typography>
                    </div>

                </div>
            </div>
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                <div
                    style={{
                        width: '320px',
                        display: 'flex',
                        justifyContent: 'space-between'
                    }}
                >
                    <Link
                        to={'/setting/account-setting'}>
                        <Button
                            style={{
                                width: '147px',
                                height: '48px',
                                borderRadius: '8px',
                                backgroundColor: '#FFF2E7',
                                borderWidth: '1.5px',
                                borderColor: '#FF9138',

                            }}>
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-Bold',
                                    fontSize: '16px',
                                    color: '#FF9138',
                                }}>
                                Huỷ bỏ
                            </Typography>
                        </Button>
                    </Link>
                    <Button
                        onClick={(e: any) => addNewUser(e)}
                        style={{
                            width: '147px',
                            height: '48px',
                            borderRadius: '8px',
                            backgroundColor: '#FF9138'
                        }}>
                        <Typography
                            style={{
                                fontFamily: 'Nunito-Bold',
                                fontSize: '16px',
                                color: '#ffffff'
                            }}>
                            Thêm
                        </Typography>
                    </Button>
                </div>

            </div>
        </div >
    )
}

export default AddAccountSettingContent