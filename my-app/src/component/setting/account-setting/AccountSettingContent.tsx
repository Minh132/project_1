import {
    Input,
    Select,
    Table,
    Typography,
} from 'antd'
import React, {
    useState,
    useEffect
} from 'react'
import {
    CaretDownOutlined,
    SearchOutlined,
    PlusOutlined,
} from '@ant-design/icons';
import { Link } from 'react-router-dom';
import {
    DocumentData,
    onSnapshot,
    QuerySnapshot
} from "firebase/firestore";
import { User } from '../../../types/User';
import { Role } from '../../../types/Role';
import { usersCollection } from '../../../features/User/userSlice';
import { rolesCollection } from '../../../features/Role/roleSlice';
function AccountSettingContent() {
    const [users, setUsers] = useState<User[]>([]);
    useEffect(() => {
        const unsubscribe = onSnapshot(usersCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const userData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setUsers(userData);
        });

        return () => {
            unsubscribe();
        };
    }, []);
    const [selectedRole, setSelectedRole] = useState<string>('Tất cả');
    const [filteredUsers, setFilteredUsers] = useState<User[]>([]);
    const [roles, setRoles] = useState<Role[]>([]);

    const handleRole = (value: string) => {
        setSelectedRole(value);
        if (value === 'Tất cả') {
            setFilteredUsers(users); // Hiển thị tất cả dữ liệu
        } else {
            const filteredData = users.filter(user => user.role === value);
            setFilteredUsers(filteredData);
        }
    };
    const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchKeyword(event.target.value);
    }; const [searchKeyword, setSearchKeyword] = useState<string>('');

    useEffect(() => {
        const unsubscribe = onSnapshot(rolesCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const roleData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setRoles(roleData);
        });

        return () => {
            unsubscribe();
        };
    }, []);
    const columns = [
        {
            title: 'Tên đăng nhập',
            dataIndex: 'userAccount',
            key: 'userAccount',
            render: (userAccount: string) => {
                return (
                    <div
                        style={{
                            minWidth: '220px',
                            maxWidth: '220px'
                        }}>
                        <Typography
                            style={{
                                fontFamily: 'Nunito-Regular',
                                fontSize: '14px'
                            }}>
                            {userAccount}
                        </Typography>
                    </div>
                );
            },
        },
        {
            title: 'Họ tên',
            dataIndex: 'name',
            key: 'name',
            render: (name: string) => {
                return (
                    <div
                        style={{
                            minWidth: '170px',
                            maxWidth: '170px'
                        }}>
                        <Typography
                            style={{
                                fontFamily: 'Nunito-Regular',
                                fontSize: '14px'
                            }}>
                            {name}
                        </Typography>
                    </div>
                );
            },
        },
        {
            title: 'Số điện thoại',
            dataIndex: 'phone',
            key: 'phone',
            render: (phone: string) => {
                return (
                    <div
                        style={{
                            minWidth: '90px',
                            maxWidth: '90px'
                        }}>
                        <Typography
                            style={{
                                fontFamily: 'Nunito-Regular',
                                fontSize: '14px'
                            }}>
                            {phone}
                        </Typography>
                    </div>
                );
            },
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
            render: (email: string) => {
                return (
                    <div
                        style={{
                            minWidth: '220px',
                            maxWidth: '220px'
                        }}>
                        <Typography
                            style={{
                                fontFamily: 'Nunito-Regular',
                                fontSize: '14px'
                            }}>
                            {email}
                        </Typography>
                    </div>
                );
            },
        },
        {
            title: 'Vai trò',
            dataIndex: 'role',
            key: 'role',
            render: (role: string) => {
                return (
                    <div
                        style={{
                            minWidth: '70px',
                            maxWidth: '70px'
                        }}>
                        <Typography
                            style={{
                                fontFamily: 'Nunito-Regular',
                                fontSize: '14px'
                            }}>
                            {role}
                        </Typography>
                    </div>
                );
            },
        },
        {
            title: 'Trạng thái hoạt động',
            dataIndex: 'active',
            key: 'active',
            render: (active: string) => {
                return (
                    <>
                        {active === 'Hoạt động'
                            ?
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'center'
                                }}>
                                <div
                                    style={{
                                        width: '6px',
                                        height: '6px',
                                        backgroundColor: '#34CD26',
                                        borderRadius: '100%',
                                        marginRight: '2px'
                                    }}></div>
                                <Typography
                                    style={{
                                        fontFamily: 'Nunito-Regular',
                                        fontSize: '14px'
                                    }}>
                                    Hoạt động
                                </Typography>
                            </div>
                            : ""
                        }
                        {
                            active === 'Ngưng hoạt động'
                                ?
                                <div
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center'
                                    }}>
                                    <div
                                        style={{
                                            width: '6px',
                                            height: '6px',
                                            backgroundColor: '#EC3740',
                                            borderRadius: '100%',
                                            marginRight: '2px'
                                        }}></div>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-Regular',
                                            fontSize: '14px'
                                        }}>
                                        Ngưng hoạt động
                                    </Typography>
                                </div>
                                : ""
                        }
                    </>
                );
            },
        },
        {
            title: ' ',
            dataIndex: 'update',
            key: 'update',
            render: (_: any, record: User) => {
                console.log(record.id)
                return (
                    <Link
                        to={`/setting/account-setting/update-account-setting/id=${record.id}`}>
                        <Typography.Link
                            key={record.id}
                            style={{
                                textDecoration: 'underline',
                                fontFamily: 'Nunito-Regular',
                                fontSize: '14px'
                            }}>
                            Cập nhật
                        </Typography.Link>
                    </Link>

                );
            },
        },
    ];
    return (
        <div
            style={{
                margin: '0 20px',
                width: '1112px',
            }}>
            <Typography
                style={{
                    fontSize: '24px',
                    fontFamily: 'Nunito-Bold',
                    color: '#FF7506',
                }}>
                Danh sách tài khoản
            </Typography>
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                }}>
                <div
                    style={{
                        display: 'flex',
                        width: '100%',
                        justifyContent: 'space-between',
                    }}>
                    <div>
                        <Typography
                            style={{
                                fontSize: '16px',
                                fontFamily: 'Nunito-SemiBold',
                            }}>
                            Tên vai trò
                        </Typography>
                        <Select
                            defaultValue="Tất cả"
                            onChange={handleRole}
                            suffixIcon={<CaretDownOutlined
                                style={{
                                    color: '#FF7506'
                                }}
                            />}
                            listHeight={180}
                            style={{
                                width: '300px',
                                height: '44px',
                                borderRadius: '8px',
                                display: 'flex',
                                justifyContent: 'space-between'
                            }}
                        >
                            <Select.Option
                                className='timeSelect'
                                key={1}
                                Title="Tất cả"
                                value="Tất cả"
                                style={{
                                    height: '44px',
                                    alignItems: 'center',
                                    fontSize: '16px',
                                    fontFamily: 'Nunito-Regular'
                                }}
                            >
                                Tất cả
                            </Select.Option>
                            {roles &&
                                roles.map((role) => (
                                    <Select.Option
                                        value={role.name}
                                        className='timeSelect'
                                        Title={role.name}
                                        key={role.id}
                                        style={{
                                            height: '44px',
                                            alignItems: 'center',
                                            fontSize: '16px',
                                            fontFamily: 'Nunito-Regular'
                                        }}
                                    >
                                        {role.name}
                                    </Select.Option>
                                ))}
                        </Select>
                    </div>
                    <div>
                        <Typography
                            style={{
                                fontSize: '16px',
                                fontFamily: 'Nunito-SemiBold',
                            }}>
                            Từ khoá
                        </Typography>
                        <Input
                            onChange={handleSearch}
                            placeholder="Nhập từ khóa"
                            style={{
                                width: '300px',
                                height: '42px'
                            }}
                            suffix={
                                <SearchOutlined
                                    style={{
                                        color: '#FF7506',
                                        fontSize: '16px'
                                    }} />
                            }
                        />
                    </div>
                </div>
            </div>
            <div
                style={{
                    height: '490px'
                }}
            >
                <Table
                    pagination={{ pageSize: 9 }}
                    dataSource={selectedRole === 'Tất cả'
                        ? users && users.filter(
                            user =>
                                user && user.userAccount && user.userAccount && user.userAccount.toLowerCase().includes(searchKeyword.toLowerCase())
                        )
                        : users.filter(
                            users =>
                                users &&
                                (users.role === selectedRole)

                        )}
                    columns={columns}
                    rowClassName={(record, index) =>
                        index % 2 === 0 ? "bg-white" : "bg-orange-50"
                    }
                    style={{
                        height: '490px',
                        marginTop: '4px'
                    }}
                />
            </div>
            <Link
                to={'/setting/account-setting/add-account-setting'}
                style={{
                    display: 'flex',
                    position: 'absolute',
                    top: '24.4%',
                    left: '94%',
                    backgroundColor: '#FFF2E7',
                    flexDirection: 'column',
                    padding: '12px',
                    width: '92px',
                    borderRadius: '8px',
                    boxShadow: '0px 0px 6px #E7E9F2',
                    cursor: 'pointer'
                }}>
                <PlusOutlined
                    style={{
                        color: '#ffffff',
                        width: '28px',
                        height: '28px',
                        backgroundColor: '#FF9138',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: '8px',
                        marginBottom: '4px',
                        alignSelf: 'center',
                    }} />
                <Typography
                    style={{
                        fontFamily: 'Nunito-SemiBold',
                        fontSize: '14px',
                        color: '#FF9138',
                        textAlign: 'center',
                    }}>
                    Thêm tài khoản
                </Typography>
            </Link>
        </div>
    )
}

export default AccountSettingContent