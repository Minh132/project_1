import {
    Button,
    Input,
    Typography,
    Checkbox
} from 'antd'
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import type { CheckboxChangeEvent } from 'antd/es/checkbox';
import { addRole } from '../../../features/Role/roleSlice';
function AddRoleSettingContent() {
    const onChange = (e: CheckboxChangeEvent) => {
        console.log(`checked = ${e.target.checked}`);
    };
    const { TextArea } = Input;
    const [name, setName] = useState("");
    const [number, setNumber] = useState("");
    const [describe, setDecribe] = useState("");
    const dispatch = useDispatch();
    const addNewRole = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        try {
            await dispatch(
                addRole({
                    name,
                    number,
                    describe,
                }) as any
            );
            console.log('successfully added a new role');
        } catch (error) {
            console.log('error:', error);
        }
        console.log("successfully added a new role");
        setNumber(number)
    };
    return (
        <div
            style={{
                margin: '0 20px'
            }}
        >
            <Typography
                style={{
                    color: '#FF7506',
                    fontFamily: 'Nunito-Bold',
                    fontSize: '24px'
                }}
            >
                Danh sách vai trò
            </Typography>
            <div
                style={{
                    width: '1178px',
                    height: '534px',
                    backgroundColor: '#ffffff',
                    boxShadow: '2px 2px 8px rgba(232, 239, 244, 0.8)',
                    borderRadius: '16px',
                    padding: '20px',
                    margin: '7px 0'
                }}>
                <div>
                    <Typography
                        style={{
                            color: '#FF9138',
                            fontFamily: 'Nunito-Bold',
                            fontSize: '20px'
                        }}>
                        Thông tin vai trò
                    </Typography>
                    <div
                        style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            marginTop: '6px',
                            marginBottom: '6px',
                        }}>
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'space-between',
                                height: '300px'
                            }}>
                            <div
                                style={{
                                    display: 'flex',
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-SemiBold',
                                    }}>
                                    Tên vai trò:
                                </Typography>
                                <div
                                    style={{
                                        width: '25px',
                                        height: '25px'
                                    }}
                                >
                                    <Typography
                                        style={{
                                            color: 'red',
                                            fontSize: '16px',
                                            alignItems: 'center',
                                            display: 'flex',
                                            height: '32px',
                                            marginLeft: '2px'
                                        }}>
                                        *
                                    </Typography>
                                </div>
                            </div>
                            <Input
                                onChange={(e) => setName(e.target.value)}
                                placeholder="Nhập tên vai trò"
                                style={{
                                    width: '553px',
                                    height: '44px',
                                    fontSize: '16px'
                                }}
                            />
                            <div
                                style={{
                                    display: 'flex',
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-SemiBold',
                                    }}>
                                    Mô tả:
                                </Typography>
                            </div>
                            <TextArea
                                onChange={(e) => setDecribe(e.target.value)}
                                placeholder="Nhập mô tả"
                                style={{
                                    width: '553px',
                                    height: '132px',
                                    fontSize: '16px',
                                    resize: 'none'
                                }}
                            />
                            <div
                                style={{
                                    display: 'flex',
                                }}>
                                <Typography
                                    style={{
                                        color: 'red',
                                        fontSize: '14px',
                                        alignItems: 'center',
                                        display: 'flex',
                                        height: '28px',
                                        marginRight: '2px'
                                    }}>
                                    *
                                </Typography>
                                <Typography
                                    style={{
                                        fontSize: '14px',
                                        fontFamily: 'Nunito-Regular',
                                        color: '#7E7D88'
                                    }}>
                                    Là trường thông tin bắt buộc
                                </Typography>
                            </div>
                        </div>

                        <div>
                            <div
                                style={{
                                    display: 'flex',
                                    marginBottom: '10px'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Nunito-SemiBold',
                                    }}>
                                    Phân quyền chức năng
                                </Typography>
                                <div
                                    style={{
                                        width: '25px',
                                        height: '25px'
                                    }}
                                >
                                    <Typography
                                        style={{
                                            color: 'red',
                                            fontSize: '16px',
                                            alignItems: 'center',
                                            display: 'flex',
                                            height: '32px',
                                            marginLeft: '2px'
                                        }}>
                                        *
                                    </Typography>
                                </div>
                            </div>
                            <div
                                className='scroll-bar'
                                style={{
                                    backgroundColor: '#FFF2E7',
                                    width: '560px',
                                    height: '420px',
                                    borderRadius: '8px',
                                    padding: '20px',
                                    display: 'flex',
                                    flexDirection: 'column',
                                    overflow: 'auto',
                                }}>
                                <div
                                    style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                        minHeight: '180px',
                                        justifyContent: 'space-between',
                                        marginBottom: '20px'
                                    }}>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-Bold',
                                            fontSize: '20px',
                                            color: '#FF7506'
                                        }}>
                                        Nhóm chức năng A
                                    </Typography>

                                    <div>
                                        <Checkbox
                                            onChange={onChange}
                                            style={{
                                            }}
                                        >
                                            <Typography
                                                style={{
                                                    fontFamily: 'Nunito-SemiBold',
                                                    fontSize: '16px',
                                                }}
                                            >
                                                Tất cả
                                            </Typography>
                                        </Checkbox>
                                    </div>
                                    <div>
                                        <Checkbox
                                            onChange={onChange}
                                            style={{
                                            }}
                                        >
                                            <Typography
                                                style={{
                                                    fontFamily: 'Nunito-SemiBold',
                                                    fontSize: '16px',
                                                }}
                                            >
                                                Chức năng x
                                            </Typography>
                                        </Checkbox>
                                    </div>
                                    <div>
                                        <Checkbox
                                            onChange={onChange}
                                            style={{
                                            }}
                                        >
                                            <Typography
                                                style={{
                                                    fontFamily: 'Nunito-SemiBold',
                                                    fontSize: '16px',
                                                }}
                                            >
                                                Chức năng y
                                            </Typography>
                                        </Checkbox>
                                    </div>
                                    <div>
                                        <Checkbox
                                            onChange={onChange}
                                            style={{
                                            }}
                                        >
                                            <Typography
                                                style={{
                                                    fontFamily: 'Nunito-SemiBold',
                                                    fontSize: '16px',
                                                }}
                                            >
                                                Chức năng z
                                            </Typography>
                                        </Checkbox>
                                    </div>
                                </div>
                                <div
                                    style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                        minHeight: '180px',
                                        justifyContent: 'space-between',
                                        marginBottom: '20px'
                                    }}>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-Bold',
                                            fontSize: '20px',
                                            color: '#FF7506'
                                        }}>
                                        Nhóm chức năng B
                                    </Typography>

                                    <div>
                                        <Checkbox
                                            onChange={onChange}
                                            style={{
                                            }}
                                        >
                                            <Typography
                                                style={{
                                                    fontFamily: 'Nunito-SemiBold',
                                                    fontSize: '16px',
                                                }}
                                            >
                                                Tất cả
                                            </Typography>
                                        </Checkbox>
                                    </div>
                                    <div>
                                        <Checkbox
                                            onChange={onChange}
                                            style={{
                                            }}
                                        >
                                            <Typography
                                                style={{
                                                    fontFamily: 'Nunito-SemiBold',
                                                    fontSize: '16px',
                                                }}
                                            >
                                                Chức năng x
                                            </Typography>
                                        </Checkbox>
                                    </div>
                                    <div>
                                        <Checkbox
                                            onChange={onChange}
                                            style={{
                                            }}
                                        >
                                            <Typography
                                                style={{
                                                    fontFamily: 'Nunito-SemiBold',
                                                    fontSize: '16px',
                                                }}
                                            >
                                                Chức năng y
                                            </Typography>
                                        </Checkbox>
                                    </div>
                                    <div>
                                        <Checkbox
                                            onChange={onChange}
                                            style={{
                                            }}
                                        >
                                            <Typography
                                                style={{
                                                    fontFamily: 'Nunito-SemiBold',
                                                    fontSize: '16px',
                                                }}
                                            >
                                                Chức năng z
                                            </Typography>
                                        </Checkbox>
                                    </div>
                                </div>
                                <div
                                    style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                        minHeight: '180px',
                                        justifyContent: 'space-between',
                                        marginBottom: '20px'
                                    }}>
                                    <Typography
                                        style={{
                                            fontFamily: 'Nunito-Bold',
                                            fontSize: '20px',
                                            color: '#FF7506'
                                        }}>
                                        Nhóm chức năng C
                                    </Typography>

                                    <div>
                                        <Checkbox
                                            onChange={onChange}
                                            style={{
                                            }}
                                        >
                                            <Typography
                                                style={{
                                                    fontFamily: 'Nunito-SemiBold',
                                                    fontSize: '16px',
                                                }}
                                            >
                                                Tất cả
                                            </Typography>
                                        </Checkbox>
                                    </div>
                                    <div>
                                        <Checkbox
                                            onChange={onChange}
                                            style={{
                                            }}
                                        >
                                            <Typography
                                                style={{
                                                    fontFamily: 'Nunito-SemiBold',
                                                    fontSize: '16px',
                                                }}
                                            >
                                                Chức năng x
                                            </Typography>
                                        </Checkbox>
                                    </div>
                                    <div>
                                        <Checkbox
                                            onChange={onChange}
                                            style={{
                                            }}
                                        >
                                            <Typography
                                                style={{
                                                    fontFamily: 'Nunito-SemiBold',
                                                    fontSize: '16px',
                                                }}
                                            >
                                                Chức năng y
                                            </Typography>
                                        </Checkbox>
                                    </div>
                                    <div>
                                        <Checkbox
                                            onChange={onChange}
                                            style={{
                                            }}
                                        >
                                            <Typography
                                                style={{
                                                    fontFamily: 'Nunito-SemiBold',
                                                    fontSize: '16px',
                                                }}
                                            >
                                                Chức năng z
                                            </Typography>
                                        </Checkbox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                <div
                    style={{
                        width: '320px',
                        display: 'flex',
                        justifyContent: 'space-between'
                    }}
                >

                    <Link
                        to={'/setting/role-setting'}>
                        <Button
                            style={{
                                width: '147px',
                                height: '48px',
                                borderRadius: '8px',
                                backgroundColor: '#FFF2E7',
                                borderWidth: '1.5px',
                                borderColor: '#FF9138',

                            }}>
                            <Typography
                                style={{
                                    fontFamily: 'Nunito-Bold',
                                    fontSize: '16px',
                                    color: '#FF9138',
                                }}>
                                Huỷ bỏ
                            </Typography>
                        </Button>
                    </Link>
                    <Button
                        onClick={(e: any) => addNewRole(e)}
                        style={{
                            width: '147px',
                            height: '48px',
                            borderRadius: '8px',
                            backgroundColor: '#FF9138'
                        }}>
                        <Typography
                            style={{
                                fontFamily: 'Nunito-Bold',
                                fontSize: '16px',
                                color: '#ffffff'
                            }}>
                            Thêm
                        </Typography>
                    </Button>
                </div>

            </div>
        </div >
    )
}

export default AddRoleSettingContent