import {
    Input,
    Table,
    Typography
} from 'antd'
import {
    SearchOutlined,
    PlusOutlined,
} from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { Role } from '../../../types/Role';
import React, {
    useState,
    useEffect
} from 'react'
import {
    DocumentData,
    onSnapshot,
    QuerySnapshot
} from "firebase/firestore";
import { rolesCollection } from '../../../features/Role/roleSlice';
function RoleSettingContent() {
    const [roles, setRoles] = useState<Role[]>([]);
    const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchKeyword(event.target.value);
    }; const [searchKeyword, setSearchKeyword] = useState<string>('');
    useEffect(() => {
        const unsubscribe = onSnapshot(rolesCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const roleData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setRoles(roleData);
        });
        return () => {
            unsubscribe();
        };
    }, []);
    const columns = [
        {
            title: 'Tên vai trò',
            dataIndex: 'name',
            key: 'name',
            render: (name: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {name}
                    </Typography>
                );
            },
        },
        {
            title: 'Số người dùng',
            dataIndex: 'number',
            key: 'number',
            render: (number: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {number}
                    </Typography>
                );
            },
        },
        {
            title: 'Mô tả',
            dataIndex: 'describe',
            key: 'describe',
            render: (describe: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Nunito-Regular',
                            fontSize: '14px'
                        }}>
                        {describe}
                    </Typography>
                );
            },
        },
        {
            title: ' ',
            dataIndex: 'update',
            key: 'update',
            render: (_: any, record: Role) => {
                return (
                    <Link
                        to={`/setting/role-setting/update-role-setting/id=${record.id}`}>
                        <Typography.Link
                            style={{
                                textDecoration: 'underline',
                                fontFamily: 'Nunito-Regular',
                                fontSize: '14px'
                            }}>
                            Cập nhật
                        </Typography.Link>
                    </Link>
                );
            },
        },
    ];
    return (
        <div
            style={{
                margin: '0 20px',
                width: '1112px'
            }}>
            <Typography
                style={{
                    fontSize: '24px',
                    fontFamily: 'Nunito-Bold',
                    color: '#FF7506',
                }}>
                Danh sách vai trò
            </Typography>
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'end',
                }}>
                <div>
                    <Typography
                        style={{
                            fontSize: '16px',
                            fontFamily: 'Nunito-SemiBold',
                        }}>
                        Từ khoá
                    </Typography>
                    <Input
                        onChange={handleSearch}
                        placeholder="Nhập từ khóa"
                        style={{
                            width: '300px',
                            height: '42px'
                        }}
                        suffix={
                            <SearchOutlined
                                style={{
                                    color: '#FF7506',
                                    fontSize: '16px'
                                }} />
                        }
                    />
                </div>
            </div>
            <div
                style={{
                    height: '490px'
                }}
            >
                <Table
                    pagination={{ pageSize: 9 }}
                    dataSource={roles && roles.filter(
                        role =>
                            role && role.name && role.name && role.name.toLowerCase().includes(searchKeyword.toLowerCase()))}
                    columns={columns}
                    rowClassName={(record, index) =>
                        index % 2 === 0 ? "bg-white" : "bg-orange-50"
                    }
                    style={{
                        height: '490px',
                        marginTop: '4px'
                    }}
                />
            </div>
            <Link
                to={'/setting/role-setting/add-role-setting'}
                style={{
                    display: 'flex',
                    position: 'absolute',
                    top: '24%',
                    left: '94%',
                    backgroundColor: '#FFF2E7',
                    flexDirection: 'column',
                    padding: '12px',
                    width: '92px',
                    borderRadius: '8px',
                    boxShadow: '0px 0px 6px #E7E9F2',
                    cursor: 'pointer'
                }}>
                <PlusOutlined
                    style={{
                        color: '#ffffff',
                        width: '28px',
                        height: '28px',
                        backgroundColor: '#FF9138',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: '8px',
                        marginBottom: '4px',
                        alignSelf: 'center',
                    }} />
                <Typography
                    style={{
                        fontFamily: 'Nunito-SemiBold',
                        fontSize: '14px',
                        color: '#FF9138',
                        textAlign: 'center',
                    }}>
                    Thêm vai trò
                </Typography>
            </Link>
        </div>
    )
}

export default RoleSettingContent