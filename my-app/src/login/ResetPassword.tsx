import {
    Button,
    Image,
    Input,
    Typography
} from 'antd'
import React, {
    useState,
    useRef,
    useEffect
} from 'react'
import {
    useNavigate,
    useLocation
} from 'react-router-dom'
import {
    confirmPasswordReset
} from "firebase/auth";
import { auth } from "../lib/firebase";
import {
    InfoCircleOutlined,
    CheckCircleOutlined
} from '@ant-design/icons';
function ResetPassword() {
    const navigate = useNavigate();
    const [checkSuccess, setCheckSuccess] = useState(false)
    const [checkInput, setCheckInput] = useState(true)
    const [newPassword, setNewPassword] = useState("")
    const isInputValid = newPassword.length >= 8;
    const [confirmPassword, setConfirmPassword] = useState("")
    const isInputConfirmValid = confirmPassword.length >= 8;
    const oobCode = useRef<string | null>(null);
    const location = useLocation();
    const handlePressEnter = () => {
        reset();
    }
    useEffect(() => {
        const queryParams = new URLSearchParams(location.search);
        oobCode.current = queryParams.get("oobCode");
    }, []);
    const reset = async () => {
        try {
            if (newPassword === confirmPassword && isInputConfirmValid && isInputValid) {
                if (oobCode.current === null) {
                    throw new Error("Invalid oobCode");
                }
                const user = await confirmPasswordReset(
                    auth,
                    oobCode.current,
                    newPassword,
                );
                console.log(user);
                setCheckInput(true);
                setCheckSuccess(true);
                setTimeout(
                    function () {
                        navigate("/")
                    }
                    , 3000);
            }
            else {
                setCheckInput(false);
                setNewPassword("");
                setConfirmPassword("");
            }
        } catch (error: any) {
            console.log(error.message);
        }
    };
    return (
        <div
            style={{
                position: 'relative',
                background: '#F7F7F7',
                borderRadius: '16px',
                display: 'flex'
            }}>

            <div style={{
                flex: 1,
            }}>
                <div style={{
                    margin: '60px',
                    justifyContent: 'center',
                    display: 'flex'
                }}>
                    <Image width={180} preview={false} src='https://media.discordapp.net/attachments/1029747761094070354/1099033961432367224/image.png?width=425&height=340'></Image>
                </div>
                <div style={{
                    justifyItems: 'center',
                    display: 'flex',
                    flexDirection: 'column',
                    padding: '30px 100px'
                }}>
                    <Typography style={{ fontFamily: 'Nunito-Bold', fontSize: '22px', textAlign: 'center' }}>Đặt lại mật khẩu mới</Typography>
                    {
                        checkInput
                            ?
                            <>
                                <div>
                                    <Typography
                                        style={{
                                            fontSize: '18px',
                                            margin: '10px 0 5px 0'
                                        }}>
                                        Mật khẩu
                                    </Typography>
                                    <Input.Password
                                        value={newPassword}
                                        placeholder="Input password"
                                        style={{ fontSize: '18px' }}
                                        onChange={(even: any) => {
                                            setNewPassword(even.target.value);
                                        }} />
                                </div>
                                <div>
                                    <Typography
                                        style={{
                                            fontSize: '18px',
                                            margin: '10px 0 5px 0'
                                        }}>
                                        Nhập lại mật khẩu
                                    </Typography>
                                    <Input.Password
                                        value={confirmPassword}
                                        placeholder="Input password"
                                        style={{ fontSize: '18px' }}
                                        onChange={(even: any) => {
                                            setConfirmPassword(even.target.value);
                                        }}
                                        onPressEnter={handlePressEnter} />
                                    {checkSuccess
                                        ?
                                        <Typography
                                            style={{
                                                color: '#34CD26',
                                                fontSize: '14px',
                                                margin: '10px 0 5px 0'
                                            }}>
                                            <CheckCircleOutlined /> Thay đổi mật khẩu thành công
                                        </Typography>
                                        :
                                        <div
                                            style={{
                                                height: '37px'
                                            }} />
                                    }
                                </div>
                            </>
                            :
                            <>
                                <div>
                                    <Typography
                                        style={{
                                            fontSize: '18px',
                                            margin: '10px 0 5px 0'
                                        }}>
                                        Mật khẩu
                                    </Typography>
                                    <Input.Password
                                        value={newPassword}
                                        placeholder="Input password"
                                        style={{
                                            fontSize: '18px',
                                            borderColor: '#E73F3F'
                                        }}
                                        onChange={(even: any) => {
                                            setNewPassword(even.target.value);
                                        }} />
                                </div>
                                <div>
                                    <Typography
                                        style={{
                                            fontSize: '18px',
                                            margin: '10px 0 5px 0'
                                        }}>
                                        Nhập lại mật khẩu
                                    </Typography>
                                    <Input.Password
                                        value={confirmPassword}
                                        placeholder="Input password"
                                        style={{
                                            fontSize: '18px',
                                            borderColor: '#E73F3F'
                                        }}
                                        onChange={(even: any) => {
                                            setConfirmPassword(even.target.value);
                                        }}
                                        onPressEnter={handlePressEnter} />
                                    <Typography
                                        style={{
                                            color: '#E73F3F',
                                            fontSize: '14px',
                                            margin: '10px 0 5px 0'
                                        }}>
                                        <InfoCircleOutlined /> Mật khẩu nhập lại khác với mật khẩu hoặc nhập thiếu 8 kí tự
                                    </Typography>
                                </div>
                            </>
                    }
                    <div style={{
                        margin: '15px auto 0',
                    }}>
                        <Button style={{ width: '162px', height: '40px', backgroundColor: '#FF9138', }} onClick={reset}>
                            <Typography style={{ color: '#ffffff', fontSize: '16px' }}>
                                Xác nhận
                            </Typography>
                        </Button>
                    </div>
                </div>
            </div>
            <div style={{
                backgroundColor: 'white',
                flex: 1.3,
                padding: '114px 0 60px 80px'
            }}>
                <Image width={690} preview={false} src='alta-2.png'></Image>
            </div>
        </div >
    )
}

export default ResetPassword