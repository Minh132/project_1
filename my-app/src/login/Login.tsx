import {
    Button,
    Image,
    Input,
    Typography
} from 'antd'
import React, {
    useState,
    useContext
} from 'react'
import {
    Link,
    useNavigate
} from 'react-router-dom'
import {
    InfoCircleOutlined
} from '@ant-design/icons';
import {
    signInWithEmailAndPassword,
} from "firebase/auth";
import { auth } from "../lib/firebase";
import { IdContext } from '../IdContext';
function Login() {
    const [login, setLogin] = useState(true)
    const [loginEmail, setLoginEmail] = useState("")
    const [loginPassword, setLoginPassword] = useState("")
    const isInputValid = loginPassword.length >= 8;
    const navigate = useNavigate();
    const handlePressEnter = () => {
        handleLogin();
    }
    const { setId } = useContext(IdContext);
    const handleLogin = async () => {
        try {
            if (isInputValid) {
                const user = await signInWithEmailAndPassword(
                    auth,
                    loginEmail,
                    loginPassword
                );
                console.log(user);
                auth.onAuthStateChanged(function (userData: any) {
                    if (userData) {
                        var uid = userData.uid;
                        console.log(uid);
                        setId(uid)
                        navigate(`/account-info/id=${uid}`)
                    }
                });
            } else {
                setLogin(false);
                setLoginPassword("");
            }
        } catch (error: any) {
            console.log(error.message);
            setLogin(false);
            setLoginPassword("");
        }
    };
    return (
        <div
            style={{
                position: 'relative',
                background: '#F7F7F7',
                borderRadius: '16px',
                display: 'flex'
            }}>
            <div style={{
                flex: 1,
            }}>
                <div style={{
                    margin: '60px',
                    justifyContent: 'center',
                    display: 'flex'
                }}>
                    <Image
                        width={180}
                        preview={false}
                        src='alta.png'
                    />
                </div>
                {
                    login
                        ?
                        <div style={{
                            justifyItems: 'center',
                            display: 'flex',
                            flexDirection: 'column',
                            padding: '0 100px'
                        }}>
                            <div>
                                <Typography
                                    style={{
                                        fontSize: '18px',
                                        margin: '10px 0 5px 0'
                                    }}>
                                    Tên đăng nhập *
                                </Typography>
                                <Input
                                    placeholder="Input account name"
                                    style={{ fontSize: '18px' }}
                                    onChange={(even: any) => {
                                        setLoginEmail(even.target.value);
                                    }} />
                            </div>
                            <div>
                                <Typography
                                    style={{
                                        fontSize: '18px',
                                        margin: '10px 0 5px 0'
                                    }} >
                                    Mật khẩu *
                                </Typography>
                                <Input.Password
                                    value={loginPassword}
                                    placeholder="Input password"
                                    style={{
                                        fontSize: '18px'
                                    }}
                                    onChange={(even: any) => {
                                        setLoginPassword(even.target.value);
                                    }}
                                    onPressEnter={handlePressEnter} />
                            </div>
                            <Link
                                to={`/forgot-password`}
                                style={{
                                    textDecoration: 'none',
                                    margin: '10px 0 5px 0'
                                }}>
                                <Typography
                                    style={{
                                        color: '#E73F3F',
                                        fontSize: '14px'
                                    }}>
                                    Quên mật khẩu?
                                </Typography>
                            </Link>
                            <div
                                style={{
                                    margin: '20px auto ',
                                }}>
                                <Button
                                    style={{
                                        width: '162px',
                                        height: '40px',
                                        backgroundColor: '#FF9138',
                                    }}
                                    onClick={handleLogin}>
                                    <Typography
                                        style={{
                                            color: '#ffffff',
                                            fontSize: '16px'
                                        }}>
                                        Đăng nhập
                                    </Typography>
                                </Button>
                            </div>
                        </div>
                        :
                        <div
                            style={{
                                justifyItems: 'center',
                                display: 'flex',
                                flexDirection: 'column',
                                padding: '0 100px'
                            }}>
                            <div>
                                <Typography
                                    style={{
                                        fontSize: '18px',
                                        margin: '10px 0 5px 0'
                                    }}>
                                    Tên đăng nhập *
                                </Typography>
                                <Input
                                    placeholder="Input account name"
                                    style={{
                                        fontSize: '18px',
                                        borderColor: '#E73F3F'
                                    }}
                                    onChange={(even: any) => {
                                        setLoginEmail(even.target.value);
                                    }} />
                            </div>
                            <div>
                                <Typography
                                    style={{
                                        fontSize: '18px',
                                        margin: '10px 0 5px 0'
                                    }}>
                                    Mật khẩu *
                                </Typography>
                                <Input.Password
                                    value={loginPassword}
                                    onPressEnter={handlePressEnter}
                                    placeholder="Input password"
                                    style={{
                                        fontSize: '18px',
                                        borderColor: '#E73F3F'
                                    }}
                                    onChange={(even: any) => {
                                        setLoginPassword(even.target.value);
                                    }} />
                            </div>
                            <Typography
                                style={{
                                    color: '#E73F3F',
                                    fontSize: '14px',
                                    margin: '10px 0 5px 0'
                                }}>
                                <InfoCircleOutlined /> Sai mật khẩu hoặc tên đăng nhập
                            </Typography>
                            <div style={{
                                margin: '20px auto ',
                            }}>
                                <Button
                                    style={{
                                        width: '162px',
                                        height: '40px',
                                        backgroundColor: '#FF9138',
                                    }}
                                    onClick={handleLogin}>
                                    <Typography
                                        style={{
                                            color: '#ffffff',
                                            fontSize: '16px'
                                        }}>
                                        Đăng nhập
                                    </Typography>
                                </Button>
                            </div>
                            <Link
                                to={`/forgot-password`}
                                style={{
                                    textDecoration: 'none',
                                    margin: '10px 0 5px 0'
                                }}>
                                <Typography
                                    style={{
                                        color: '#E73F3F',
                                        fontSize: '14px',
                                        textAlign: 'center'
                                    }}>
                                    Quên mật khẩu?
                                </Typography>
                            </Link>
                        </div>
                }
            </div>
            <div
                style={{
                    backgroundColor: 'white',
                    flex: 1.3,
                    padding: '114px 0 60px 80px'
                }}>
                <Image
                    width={540}
                    preview={false}
                    src='https://media.discordapp.net/attachments/1029747761094070354/1099035009408565359/image.png?width=580&height=588' />
                <Typography
                    style={{
                        fontFamily: "Nunito-Black",
                        position: 'absolute',
                        left: '1060px',
                        top: '400px',
                        fontSize: '36px',
                        lineHeight: '54px',
                        textAlign: 'center',
                        color: '#FF7506',
                    }}>
                    QUẢN LÝ XẾP HÀNG
                </Typography>
                <Typography
                    style={{
                        position: 'absolute',
                        left: '1060px',
                        top: '350px',
                        fontSize: '34px',
                        lineHeight: '51px',
                        textAlign: 'center',
                        color: '#FF7506',
                    }}>
                    Hệ thống
                </Typography>
            </div>
        </div >
    )
}
export default Login