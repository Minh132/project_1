import {
    Button,
    Image,
    Input,
    Typography
} from 'antd'
import React, { useState } from 'react'
import { Link, } from 'react-router-dom'
import {
    sendPasswordResetEmail,
} from "firebase/auth";
import { auth } from "../lib/firebase";
import {
    InfoCircleOutlined,
    CheckCircleOutlined
} from '@ant-design/icons';
function ForgotPassword() {
    const [success, setSuccess] = useState(false)
    const [email, setEmail] = useState(true)
    const [loginEmail, setLoginEmail] = useState("")
    const handleEmail = async () => {
        try {
            const user = await sendPasswordResetEmail(
                auth,
                loginEmail,
            );
            console.log(user);
            setSuccess(true);
        } catch (error: any) {
            setEmail(false);
            setLoginEmail("");
            console.log(error.message);
        }
    };
    const handlePressEnter = () => {
        handleEmail();
    }
    return (
        <div
            style={{
                position: 'relative',
                background: '#F7F7F7',
                borderRadius: '16px',
                display: 'flex'
            }}>
            <div
                style={{
                    flex: 1,
                }}>
                <div
                    style={{
                        margin: '60px',
                        justifyContent: 'center',
                        display: 'flex'
                    }}>
                    <Image
                        width={180}
                        preview={false}
                        src='https://media.discordapp.net/attachments/1029747761094070354/1099033961432367224/image.png?width=425&height=340' />
                </div>
                {email ?
                    <div
                        style={{
                            justifyItems: 'center',
                            display: 'flex',
                            flexDirection: 'column',
                            padding: '30px 100px'
                        }}>
                        <Typography
                            style={{
                                fontFamily: 'Nunito-Bold',
                                fontSize: '22px',
                                textAlign: 'center'
                            }}>
                            Đặt lại mật khẩu
                        </Typography>
                        <div>
                            <Typography
                                style={{
                                    fontSize: '18px',
                                    margin: '10px 0 5px 0'
                                }}>
                                Vui lòng nhập email để đặt lại mật khẩu của bạn *
                            </Typography>
                            <Input
                                value={loginEmail}
                                placeholder="Input email"
                                style={{
                                    fontSize: '18px'
                                }}
                                onChange={(even: any) => {
                                    setLoginEmail(even.target.value);
                                }}
                                onPressEnter={handlePressEnter} />
                        </div>
                        {
                            success
                                ?
                                <Typography
                                    style={{
                                        color: '#34CD26',
                                        fontSize: '14px',
                                        margin: '10px 0 5px 0'
                                    }}>
                                    <CheckCircleOutlined /> Tin nhắn đã được gửi qua mail. Vui lòng kiểm tra hộp mail
                                </Typography>
                                :
                                <div
                                    style={{
                                        height: '37px'
                                    }}
                                />
                        }
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'row',
                                marginTop: '15px'
                            }}>
                            <div
                                style={{
                                    margin: '0 auto'
                                }}>
                                <Link
                                    to={`/`}>
                                    <Button
                                        style={{
                                            width: '162px',
                                            height: '40px',
                                            backgroundColor: 'transparent',
                                            borderColor: '#FF9138'
                                        }}>
                                        <Typography
                                            style={{
                                                color: '#FF9138',
                                                fontSize: '16px'
                                            }}>
                                            Hủy
                                        </Typography>
                                    </Button>
                                </Link>
                            </div>
                            <div
                                style={{
                                    margin: '0 auto'
                                }}>
                                <Button
                                    onClick={handleEmail}
                                    style={{
                                        width: '162px',
                                        height: '40px',
                                        backgroundColor: '#FF9138'
                                    }}>
                                    <Typography
                                        style={{
                                            color: '#ffffff',
                                            fontSize: '16px'
                                        }}>
                                        Tiếp tục
                                    </Typography>
                                </Button>
                            </div>
                        </div>
                    </div>
                    :
                    <div
                        style={{
                            justifyItems: 'center',
                            display: 'flex',
                            flexDirection: 'column',
                            padding: '30px 100px'
                        }}>
                        <Typography
                            style={{
                                fontFamily: 'Nunito-Bold',
                                fontSize: '22px',
                                textAlign: 'center'
                            }}>
                            Đặt lại mật khẩu
                        </Typography>
                        <div>
                            <Typography
                                style={{
                                    fontSize: '18px',
                                    margin: '10px 0 5px 0'
                                }}>
                                Vui lòng nhập email để đặt lại mật khẩu của bạn *
                            </Typography>
                            {
                                success
                                    ?
                                    <Input
                                        value={loginEmail}
                                        placeholder="Input email"
                                        style={{
                                            fontSize: '18px',
                                        }}
                                        onChange={(even: any) => {
                                            setLoginEmail(even.target.value);
                                        }}
                                        onPressEnter={handlePressEnter} />
                                    :
                                    <Input
                                        value={loginEmail}
                                        placeholder="Input email"
                                        style={{
                                            fontSize: '18px',
                                            borderColor: '#E73F3F'
                                        }}
                                        onChange={(even: any) => {
                                            setLoginEmail(even.target.value);
                                        }}
                                        onPressEnter={handlePressEnter} />
                            }
                        </div>
                        {
                            success
                                ?
                                <Typography
                                    style={{
                                        color: '#34CD26',
                                        fontSize: '14px',
                                        margin: '10px 0 5px 0'
                                    }}>
                                    <CheckCircleOutlined /> Tin nhắn đã được gửi qua mail. Vui lòng kiểm tra hộp mail
                                </Typography>
                                :
                                <Typography
                                    style={{
                                        color: '#E73F3F',
                                        fontSize: '14px',
                                        margin: '10px 0 5px 0'
                                    }}>
                                    <InfoCircleOutlined /> Sai email vui lòng nhập lại
                                </Typography>
                        }
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'row',
                                marginTop: '15px'
                            }}>
                            <div
                                style={{
                                    margin: '0 auto'
                                }}>
                                <Link
                                    to={`/`}>
                                    <Button
                                        style={{
                                            width: '162px',
                                            height: '40px',
                                            backgroundColor: 'transparent',
                                            borderColor: '#FF9138'
                                        }}>
                                        <Typography
                                            style={{
                                                color: '#FF9138',
                                                fontSize: '16px'
                                            }}>
                                            Hủy
                                        </Typography>
                                    </Button>
                                </Link>
                            </div>
                            <div
                                style={{
                                    margin: '0 auto'
                                }}>
                                <Button
                                    onClick={handleEmail}
                                    style={{
                                        width: '162px',
                                        height: '40px',
                                        backgroundColor: '#FF9138'
                                    }}>
                                    <Typography
                                        style={{
                                            color: '#ffffff',
                                            fontSize: '16px'
                                        }}>
                                        Tiếp tục
                                    </Typography>
                                </Button>
                            </div>
                        </div>
                    </div>
                }
            </div>
            <div
                style={{
                    backgroundColor: 'white',
                    flex: 1.3,
                    padding: '114px 0 60px 80px'
                }}>
                <Image
                    width={690}
                    preview={false}
                    src='alta-2.png' />
            </div>
        </div >
    )
}

export default ForgotPassword