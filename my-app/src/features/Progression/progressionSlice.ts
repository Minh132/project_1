import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { collection, doc, setDoc, getFirestore } from 'firebase/firestore';
import { Progression } from '../../types/Progression';
interface ProgressionState {
    loading: boolean;
    error: string | null;
}

const initialState: ProgressionState = {
    loading: false,
    error: null,
};

export const firestore = getFirestore();

export const progressionCollection = collection(firestore, 'progressions');
export const addProgression = createAsyncThunk<void, Progression, { state: RootState }>(
    'progression/addProgression',
    async (progressionData, { rejectWithValue }) => {
        try {
            const newProgression = await setDoc(doc(progressionCollection), progressionData);
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);
const progressionSlice = createSlice({
    name: 'progression',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(addProgression.pending, (state) => {
                state.loading = true;
                state.error = null;
            })
            .addCase(addProgression.fulfilled, (state) => {
                state.loading = false;
                state.error = null;
            })
            .addCase(addProgression.rejected, (state, action) => {
                state.loading = false;
                state.error = action.payload as string;
            })
    },
});

export default progressionSlice.reducer;