import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { collection, doc, setDoc, getFirestore } from 'firebase/firestore';
import { Device } from '../../types/Device';

interface DeviceState {
    loading: boolean;
    error: string | null;
}

const initialState: DeviceState = {
    loading: false,
    error: null,
};

export const firestore = getFirestore();

export const devicesCollection = collection(firestore, 'devices');

export const addDevice = createAsyncThunk<void, Device, { state: RootState }>(
    'device/addDevice',
    async (deviceData, { rejectWithValue }) => {
        try {
            const newDevice = await setDoc(doc(devicesCollection), deviceData);
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

export const updateDevice = createAsyncThunk<void, { id: string | undefined, docData: Device }, { state: RootState }>(
    'device/updateDevice',
    async ({ id, docData }, { rejectWithValue }) => {
        try {
            const getDevice = doc(firestore, `devices/${id}`);
            await setDoc(getDevice, docData, { merge: true });
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

const deviceSlice = createSlice({
    name: 'device',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(addDevice.pending, (state) => {
                state.loading = true;
                state.error = null;
            })
            .addCase(addDevice.fulfilled, (state) => {
                state.loading = false;
                state.error = null;
            })
            .addCase(addDevice.rejected, (state, action) => {
                state.loading = false;
                state.error = action.payload as string;
            })
            .addCase(updateDevice.pending, (state) => {
                state.loading = true;
                state.error = null;
            })
            .addCase(updateDevice.fulfilled, (state) => {
                state.loading = false;
                state.error = null;
            })
            .addCase(updateDevice.rejected, (state, action) => {
                state.loading = false;
                state.error = action.payload as string;
            });
    },
});

export default deviceSlice.reducer;