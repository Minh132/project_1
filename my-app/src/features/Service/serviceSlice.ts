import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { collection, doc, setDoc, getFirestore } from 'firebase/firestore';
import { Service } from '../../types/Service';

interface ServiceState {
  loading: boolean;
  error: string | null;
}

const initialState: ServiceState = {
  loading: false,
  error: null,
};

export const firestore = getFirestore();

export const servicesCollection = collection(firestore, 'services');

export const addService = createAsyncThunk<void, Service, { state: RootState }>(
  'service/addService',
  async (serviceData, { rejectWithValue }) => {
    try {
      const newService = await setDoc(doc(servicesCollection), serviceData);
    } catch (error: any) {
      return rejectWithValue(error.message);
    }
  }
);

export const updateService = createAsyncThunk<void, { id: string | undefined, docData: Service }, { state: RootState }>(
  'service/updateService',
  async ({ id, docData }, { rejectWithValue }) => {
    try {
      const getService = doc(firestore, `services/${id}`);
      await setDoc(getService, docData, { merge: true });
    } catch (error: any) {
      return rejectWithValue(error.message);
    }
  }
);

const serviceSlice = createSlice({
  name: 'service',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(addService.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(addService.fulfilled, (state) => {
        state.loading = false;
        state.error = null;
      })
      .addCase(addService.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload as string;
      })
      .addCase(updateService.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(updateService.fulfilled, (state) => {
        state.loading = false;
        state.error = null;
      })
      .addCase(updateService.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload as string;
      });
  },
});

export default serviceSlice.reducer;