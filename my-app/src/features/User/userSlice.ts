import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
    createUserWithEmailAndPassword,
    getAuth,
    updateEmail,
    updatePassword
} from "firebase/auth";
import { RootState } from '../store';
import {
    collection,
    deleteDoc,
    doc,
    getFirestore,
    setDoc,
    getDocs,
    query,
    where,
    onSnapshot,
    QuerySnapshot,
    DocumentData
} from "firebase/firestore";
import {
    app,
    auth,
} from "../../lib/firebase";

import { User } from '../../types/User';

interface UserState {
    loading: boolean;
    error: string | null;
}

const initialState: UserState = {
    loading: false,
    error: null,
};
export const firestore = getFirestore(app);

export const usersCollection = collection(firestore, "users");

export const addUser = createAsyncThunk<void, User, { state: RootState }>(
    'user/addUser',
    async (userData, { rejectWithValue }) => {
        try {
            if (!userData.email || !userData.password) {
                throw new Error('Email or password is missing.');
            }

            const auth = getAuth();
            const res = await createUserWithEmailAndPassword(
                auth,
                userData.email,
                userData.password
            );

            const timeStart = new Date()
                .toLocaleTimeString()
                .replace(/([\d]?[\d]:[\d]+):[\d]+(\s[AP]M)/, '$1');
            const currentDate = new Date();
            const day = currentDate.getDate().toString().padStart(2, '0');
            const month = (currentDate.getMonth() + 1).toString().padStart(2, '0');
            const year = currentDate.getFullYear().toString();
            const dateStart = `${day}/${month}/${year}`;

            await setDoc(doc(usersCollection, res.user.uid), {
                ...userData,
                timeStart,
                dateStart,
            });
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);
export const updateUser = createAsyncThunk<void, { id: string | undefined, docData: User }, { state: RootState }>(
    'user/updateUser',
    async ({ id, docData }, { rejectWithValue }) => {
        try {
            const getUser = doc(firestore, `users/${id}`);
            await setDoc(getUser, docData, { merge: true });
            const user = auth.currentUser;

            if (user && docData.email && docData.password) {
                // Update email
                try {
                    await updateEmail(user, docData.email);
                    console.log("Cập nhật email thành công");
                } catch (error) {
                    console.error("Lỗi khi cập nhật email:", error);
                }

                // Update password
                try {
                    await updatePassword(user, docData.password);
                    console.log("Cập nhật mật khẩu thành công");
                } catch (error) {
                    console.error("Lỗi khi cập nhật mật khẩu:", error);
                }
            }
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);
const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(addUser.pending, (state) => {
                state.loading = true;
                state.error = null;
            })
            .addCase(addUser.fulfilled, (state) => {
                state.loading = false;
                state.error = null;
            })
            .addCase(addUser.rejected, (state, action) => {
                state.loading = false;
                state.error = action.payload as string;
            })
            .addCase(updateUser.pending, (state) => {
                state.loading = true;
                state.error = null;
            })
            .addCase(updateUser.fulfilled, (state) => {
                state.loading = false;
                state.error = null;
            })
            .addCase(updateUser.rejected, (state, action) => {
                state.loading = false;
                state.error = action.payload as string;
            });
    },
});

export default userSlice.reducer;