import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { collection, doc, setDoc, getFirestore } from 'firebase/firestore';
import { Role } from '../../types/Role';

interface RoleState {
    loading: boolean;
    error: string | null;
}

const initialState: RoleState = {
    loading: false,
    error: null,
};

export const firestore = getFirestore();

export const rolesCollection = collection(firestore, 'roles');

export const addRole = createAsyncThunk<void, Role, { state: RootState }>(
    'role/addRole',
    async (roleData, { rejectWithValue }) => {
        try {
            const newRole = await setDoc(doc(rolesCollection), roleData);
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

export const updateRole = createAsyncThunk<void, { id: string | undefined, docData: Role }, { state: RootState }>(
    'role/updateRole',
    async ({ id, docData }, { rejectWithValue }) => {
        try {
            const getRole = doc(firestore, `roles/${id}`);
            await setDoc(getRole, docData, { merge: true });
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

const roleSlice = createSlice({
    name: 'role',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(addRole.pending, (state) => {
                state.loading = true;
                state.error = null;
            })
            .addCase(addRole.fulfilled, (state) => {
                state.loading = false;
                state.error = null;
            })
            .addCase(addRole.rejected, (state, action) => {
                state.loading = false;
                state.error = action.payload as string;
            })
            .addCase(updateRole.pending, (state) => {
                state.loading = true;
                state.error = null;
            })
            .addCase(updateRole.fulfilled, (state) => {
                state.loading = false;
                state.error = null;
            })
            .addCase(updateRole.rejected, (state, action) => {
                state.loading = false;
                state.error = action.payload as string;
            });
    },
});

export default roleSlice.reducer;