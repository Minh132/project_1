import { Service } from './../types/Service';
import { configureStore } from '@reduxjs/toolkit';
import userReducer from './User/userSlice';
import serviceReducer from './Service/serviceSlice';
import roleReducer from './Role/roleSlice';
import deviceReducer from './Device/deviceSlice';
import progressionReducer from './Progression/progressionSlice';
const store = configureStore({
    reducer: {
        user: userReducer,
        service: serviceReducer,
        role: roleReducer,
        device: deviceReducer,
        progression: progressionReducer,
    },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;