import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
const firebaseConfig = {
    apiKey: "AIzaSyCVkPe4BK9EzrcrOTZahdhUKBCGfCtyKPQ",
    authDomain: "queuing-system-52b50.firebaseapp.com",
    projectId: "queuing-system-52b50",
    storageBucket: "queuing-system-52b50.appspot.com",
    messagingSenderId: "66449965805",
    appId: "1:66449965805:web:90b49c33dda07000d642e4",
    measurementId: "G-FXYNHXBK5X"
};

export const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const auth = getAuth(app);