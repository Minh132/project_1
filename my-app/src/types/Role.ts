export interface Role {
    id?: string;
    name?: string;
    number?: string;
    describe?: string;
}