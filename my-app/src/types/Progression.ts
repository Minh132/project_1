import { Device } from "./Device";
import { Service } from "./Service";
import { User } from "./User";

export interface Progression {
    id?: string;
    stt?: number;
    name?: string;
    service?: string;
    dayStart?: string;
    dayEnd?: string;
    status?: string;
    provide?: string;
    phone?: string;
    email?: string
}