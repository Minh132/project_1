import { Service } from './Service';
export interface Device {
    id?: string;
    idDevice?: string
    name?: string;
    ip?: string;
    active?: string;
    connect?: string;
    service?: string;
    type?: string;
    nameAccount?: string;
    password?: string;
}