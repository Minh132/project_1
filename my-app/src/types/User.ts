export interface User {
    id?: string;
    name?: string;
    userAccount?: string;
    phone?: string;
    password?: string;
    email?: string;
    role?: string;
    active?: string;
}