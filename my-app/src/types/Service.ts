export interface Service {
    id?: string;
    idService?: string;
    name?: string;
    describe?: string;
    active?: string;
}