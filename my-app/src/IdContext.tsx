import React, { createContext, useState, ReactNode } from 'react';

interface IdContextProps {
    id: string;
    setId: (id: string) => void;
}

export const IdContext = createContext<IdContextProps>({
    id: '',
    setId: () => { },
});

interface IdProviderProps {
    children: ReactNode;
}

export const IdProvider: React.FC<IdProviderProps> = ({ children }) => {
    const [id, setId] = useState<string>('');
    return (
        <IdContext.Provider value={{ id, setId }}>
            {children}
        </IdContext.Provider>
    );
};