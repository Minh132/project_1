import React, { useState } from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import reportWebVitals from './reportWebVitals';
import Login from '../src/login/Login';
import ForgotPassword from '../src/login/ForgotPassword';
import ResetPassword from '../src/login/ResetPassword';
import AccountInfo from './container/AccountInfo';
import DashBoard from './container/DashBoard';
import Device from './container/device/Device';
import ListDevice from './container/device/ListDevice';
import AddDevice from './container/device/AddDevice';
import Progression from './container/progression/Progression';
import Report from './container/report/Report';
import Service from './container/service/Service';
import Setting from './container/setting/Setting';
import DetailDevice from './container/device/DetailDevice';
import UpdateDevice from './container/device/UpdateDevice';
import ListService from './container/service/ListService';
import AddService from './container/service/AddService';
import DetailService from './container/service/DetailService';
import UpdateService from './container/service/UpdateService';
import ListProgression from './container/progression/ListProgression';
import NewProgression from './container/progression/NewProgression';
import DetailProgression from './container/progression/DetailProgression';
import ListReport from './container/report/ListReport';
import AccountSetting from './container/setting/account-setting/AccountSetting';
import RoleSetting from './container/setting/role-setting/RoleSetting';
import UserSetting from './container/setting/user-setting/UserSetting';
import AddAccountSetting from './container/setting/account-setting/AddAccountSetting';
import UpdateAccountSetting from './container/setting/account-setting/UpdateAccountSetting';
import AddRoleSetting from './container/setting/role-setting/AddRoleSetting';
import UpdateRoleSetting from './container/setting/role-setting/UpdateRoleSetting';
import { IdProvider } from './IdContext';
import { Provider } from 'react-redux';
import store from './features/store';
const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <Provider store={store}>
    <IdProvider >
      <Router>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/forgot-password" element={<ForgotPassword />} />
          <Route path="/reset-password" element={<ResetPassword />} />
          <Route path="/account-info/:id" element={<AccountInfo />} />
          <Route path="/dashboard" element={<DashBoard />} />
          <Route path="/device" element={<Device />} >
            <Route path="list-device" element={<ListDevice />} />
            <Route path="list-device/add-device" element={<AddDevice />} />
            <Route path="list-device/detail-device/:id" element={<DetailDevice />} />
            <Route path="list-device/update-device/:id" element={<UpdateDevice />} />
          </Route>
          <Route path="/progression" element={<Progression />} >
            <Route path="list-progression" element={<ListProgression />} />
            <Route path="list-progression/new-progression" element={<NewProgression />} />
            <Route path="list-progression/detail-progression/:id" element={<DetailProgression />} />
          </Route>
          <Route path="/report" element={<Report />} >
            <Route path="list-report" element={<ListReport />} />
          </Route>
          <Route path="/service" element={<Service />} >
            <Route path="list-service" element={<ListService />} />
            <Route path="list-service/add-service" element={<AddService />} />
            <Route path="list-service/detail-service/:id" element={<DetailService />} />
            <Route path="list-service/update-service/:id" element={<UpdateService />} />
          </Route>
          <Route path="/setting" element={<Setting />} >
            <Route path="account-setting" element={<AccountSetting />} />
            <Route path="account-setting/add-account-setting" element={<AddAccountSetting />} />
            <Route path="account-setting/update-account-setting/:id" element={<UpdateAccountSetting />} />
            <Route path="role-setting" element={<RoleSetting />} />
            <Route path="role-setting/add-role-setting" element={<AddRoleSetting />} />
            <Route path="role-setting/update-role-setting/:id" element={<UpdateRoleSetting />} />
            <Route path="user-setting" element={<UserSetting />} />
          </Route>
        </Routes>
      </Router></IdProvider>
  </Provider>
);
reportWebVitals();
