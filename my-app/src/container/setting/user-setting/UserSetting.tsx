import { Layout, Space } from 'antd'
import React from 'react'

import UserSettingContent from '../../../component/setting/user-setting/UserSettingContent'
import Header from '../../../component/Header'
import SideBar from '../../../component/SideBar'

function UserSetting() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <UserSettingContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default UserSetting