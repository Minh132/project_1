import { Layout, Space } from 'antd'
import React from 'react'

import AddAccountSettingContent from '../../../component/setting/account-setting/AddAccountSettingContent'
import Header from '../../../component/Header'
import SideBar from '../../../component/SideBar'

function AccountSetting() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <AddAccountSettingContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default AccountSetting