import { Layout, Space } from 'antd'
import React from 'react'

import UpdateAccountSettingContent from '../../../component/setting/account-setting/UpdateAccountSettingContent'
import Header from '../../../component/Header'
import SideBar from '../../../component/SideBar'

function AccountSetting() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <UpdateAccountSettingContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default AccountSetting