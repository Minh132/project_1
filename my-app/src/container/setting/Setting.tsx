import { useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";

const Setting = () => {
    const navigate = useNavigate();

    useEffect(() => {
        if (
            window.location.pathname === "/setting" ||
            window.location.pathname === "/setting/"
        ) {
            navigate("role-setting");
        }
    }, [navigate]);

    return <Outlet />;
};

export default Setting;
