import { Layout, Space } from 'antd'
import React from 'react'

import AddRoleSettingContent from '../../../component/setting/role-setting/AddRoleSettingContent'
import Header from '../../../component/Header'
import SideBar from '../../../component/SideBar'

function AddRoleSetting() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <AddRoleSettingContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default AddRoleSetting