import { Layout, Space } from 'antd'
import React from 'react'

import UpdateRoleSettingContent from '../../../component/setting/role-setting/UpdateRoleSettingContent'
import Header from '../../../component/Header'
import SideBar from '../../../component/SideBar'

function UpdateRoleSetting() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <UpdateRoleSettingContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default UpdateRoleSetting