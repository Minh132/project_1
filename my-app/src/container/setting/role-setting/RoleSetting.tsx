import { Layout, Space } from 'antd'
import React from 'react'

import RoleSettingContent from '../../../component/setting/role-setting/RoleSettingContent'
import Header from '../../../component/Header'
import SideBar from '../../../component/SideBar'

function RoleSetting() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <RoleSettingContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default RoleSetting