import { Layout, Space } from 'antd'
import React from 'react'

import AddServiceContent from '../../component/service/AddServiceContent'
import Header from '../../component/Header'
import SideBar from '../../component/SideBar'

function AddService() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <AddServiceContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default AddService