import { Layout, Space } from 'antd'
import React from 'react'

import UpdateServiceContent from '../../component/service/UpdateServiceContent'
import Header from '../../component/Header'
import SideBar from '../../component/SideBar'

function UpdateService() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <UpdateServiceContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default UpdateService