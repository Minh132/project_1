import { Layout, Space } from 'antd'
import React from 'react'

import ListServiceContent from '../../component/service/ListServiceContent'
import Header from '../../component/Header'
import SideBar from '../../component/SideBar'

function ListService() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <ListServiceContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default ListService