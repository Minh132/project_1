import { useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";

const Service = () => {
    const navigate = useNavigate();

    useEffect(() => {
        if (
            window.location.pathname === "/service" ||
            window.location.pathname === "/service/"
        ) {
            navigate("list-service");
        }
    }, [navigate]);

    return <Outlet />;
};

export default Service;
