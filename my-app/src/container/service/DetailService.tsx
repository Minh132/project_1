import { Layout, Space } from 'antd'
import React from 'react'

import DetailServiceContent from '../../component/service/DetailServiceContent'
import Header from '../../component/Header'
import SideBar from '../../component/SideBar'

function DetailService() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <DetailServiceContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default DetailService