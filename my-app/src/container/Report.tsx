import { Layout, Space } from 'antd'
import React from 'react'
import SideBar from '../component/SideBar'
import Header from '../component/Header'
import ListReportContent from '../component/report/ListReportContent'
function Report() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <ListReportContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default Report