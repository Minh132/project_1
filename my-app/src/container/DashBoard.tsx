import { Layout, Space } from 'antd'
import React from 'react'
import SideBar from '../component/SideBar'
import Header from '../component/Header'
import DashBoardContent from '../component/DashBoardContent'
function DashBoard() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <DashBoardContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default DashBoard