import { useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";

const Report = () => {
    const navigate = useNavigate();

    useEffect(() => {
        if (
            window.location.pathname === "/report" ||
            window.location.pathname === "/report/"
        ) {
            navigate("list-report");
        }
    }, [navigate]);

    return <Outlet />;
};

export default Report;
