import { Layout, Space } from 'antd'
import React from 'react'

import ListReportContent from '../../component/report/ListReportContent'
import Header from '../../component/Header'
import SideBar from '../../component/SideBar'

function ListReport() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <ListReportContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default ListReport