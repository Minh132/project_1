import { Layout, Space } from 'antd'
import React from 'react'

import AddDeviceContent from '../../component/device/AddDeviceContent'
import Header from '../../component/Header'
import SideBar from '../../component/SideBar'

function AddDevice() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <AddDeviceContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default AddDevice