import { useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";

const Device = () => {
    const navigate = useNavigate();

    useEffect(() => {
        if (
            window.location.pathname === "/device" ||
            window.location.pathname === "/device/"
        ) {
            navigate("list-device");
        }
    }, [navigate]);

    return <Outlet />;
};

export default Device;
