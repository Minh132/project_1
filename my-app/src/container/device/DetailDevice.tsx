import { Layout, Space } from 'antd'
import React from 'react'

import DetailDeviceContent from '../../component/device/DetailDeviceContent'
import Header from '../../component/Header'
import SideBar from '../../component/SideBar'

function DetailDevice() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <DetailDeviceContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default DetailDevice