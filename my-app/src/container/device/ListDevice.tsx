import { Layout, Space } from 'antd'
import React from 'react'

import ListDeviceContent from '../../component/device/ListDeviceContent'
import Header from '../../component/Header'
import SideBar from '../../component/SideBar'

function ListDevice() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <ListDeviceContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default ListDevice