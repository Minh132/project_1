import { Layout, Space } from 'antd'
import React from 'react'

import UpdateDeviceContent from '../../component/device/UpdateDeviceContent'
import Header from '../../component/Header'
import SideBar from '../../component/SideBar'

function UpdateDevice() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <UpdateDeviceContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default UpdateDevice