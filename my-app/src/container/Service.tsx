import { Layout, Space } from 'antd'
import React from 'react'
import SideBar from '../component/SideBar'
import Header from '../component/Header'
import ServiceContent from '../component/ServiceContent'
function Service() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <ServiceContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default Service