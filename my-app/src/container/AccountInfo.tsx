import { Layout, Space } from 'antd'
import React from 'react'
import SideBar from '../component/SideBar'
import
AccountInfoContent from '../component/AccountInfoContent'
import Header from '../component/Header'

function AccountInfo() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <AccountInfoContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default AccountInfo