import { Layout, Space } from 'antd'
import React from 'react'

import ListProgressionContent from '../../component/progression/ListProgressionContent'
import Header from '../../component/Header'
import SideBar from '../../component/SideBar'

function ListProgression() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <ListProgressionContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default ListProgression