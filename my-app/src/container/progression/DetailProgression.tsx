import { Layout, Space } from 'antd'
import React from 'react'

import DetailProgressionContent from '../../component/progression/DetailProgressionContent'
import Header from '../../component/Header'
import SideBar from '../../component/SideBar'

function DetailProgression() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <DetailProgressionContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default DetailProgression