import { Layout, Space } from 'antd'
import React from 'react'

import NewProgressionContent from '../../component/progression/NewProgressionContent'
import Header from '../../component/Header'
import SideBar from '../../component/SideBar'

function NewProgression() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <NewProgressionContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default NewProgression