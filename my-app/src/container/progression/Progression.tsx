import { useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";

const Progression = () => {
    const navigate = useNavigate();

    useEffect(() => {
        if (
            window.location.pathname === "/progression" ||
            window.location.pathname === "/progression/"
        ) {
            navigate("list-progression");
        }
    }, [navigate]);

    return <Outlet />;
};

export default Progression;
